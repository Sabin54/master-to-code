package com.kozu.mastertocode;

public class CourseListDataModel {
        String courseName;
        int photo;
        public CourseListDataModel(String courseName,int photo)
        {
            this.courseName = courseName;
            this.photo=photo;
        }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }
}
