package com.kozu.mastertocode;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    CourseListDataModel  courseListDataModel;
    RecyclerView reView;
     List<CourseListDataModel> CourseList;
     CourseAdapter adapter;
     AdView adView;
     boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        reView = findViewById(R.id.reView);
        CourseList = new ArrayList();
        adView = findViewById(R.id.adView);


        //adView.setAdUnitId("ca-app-pub-7031830486098035/4079395019");

        adView.setVisibility(View.GONE);
        if (flag)
        {
            adView.setVisibility(View.VISIBLE);
            MobileAds.initialize(this, new OnInitializationCompleteListener() {
                @Override
                public void onInitializationComplete(InitializationStatus initializationStatus) {
                    Log.d("TAG", "onInitializationComplete: "+ initializationStatus.toString());
                }
            });
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }


        Courses();

        adapter = new CourseAdapter(this,CourseList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        reView.setLayoutManager(mLayoutManager);
        reView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        reView.setItemAnimator(new DefaultItemAnimator());
        reView.setAdapter(adapter);
    }


    public void Courses() {
        int[] images = new int[]
                {
                        R.drawable.ic_java,
                        R.drawable.ic_cplus,
                        R.drawable.c1,
                        R.drawable.ic_php,
                      /*  R.drawable.kkotlin,
                        R.drawable.ic_python,
                        R.drawable.ic_csharp,
                        R.drawable.ic_javascript*/
                };

        courseListDataModel = new CourseListDataModel("JAVA",images[0]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("C++",images[1]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("C Programming",images[2]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("PHP",images[3]);
        CourseList.add(courseListDataModel);

       /* courseListDataModel = new CourseListDataModel("Kotlin",images[4]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("Python",images[5]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("C#",images[6]);
        CourseList.add(courseListDataModel);

        courseListDataModel = new CourseListDataModel("Java Script",images[7]);
        CourseList.add(courseListDataModel);
*/


    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
