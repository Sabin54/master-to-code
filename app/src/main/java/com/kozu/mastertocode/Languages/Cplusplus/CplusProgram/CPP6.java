package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP6 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    int n, t1=0, t2=1, nextTerm=0;\n" +
                "\n" +
                "    cout << \"Enter the number of terms: \";\n" +
                "    cin >> n;\n" +
                "    cout << \"Fibonacci Series: \";\n" +
                "\n" +
                "    for (int i=1; i <= n; ++i)\n" +
                "    {\n" +
                "        if(i == 1)\n" +
                "        {\n" +
                "            cout << \" \" << t1;\n" +
                "            continue;\n" +
                "        }\n" +
                "        if(i == 2)\n" +
                "        {\n" +
                "            cout << t2 << \" \";\n" +
                "            continue;\n" +
                "        }\n" +
                "        nextTerm = t1 + t2;\n" +
                "        t1 = t2;\n" +
                "        t2 = nextTerm;\n" +
                "        \n" +
                "        cout << nextTerm << \" \";\n" +
                "    }\n" +
                "    getch();\n" +
                "    return 0 ;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter the number of terms: 10\n" +
                "Fibonacci Series: 0 1 1 2 3 5 8 13 21 34");

    }
}
