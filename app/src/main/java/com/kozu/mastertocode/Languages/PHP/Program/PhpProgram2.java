package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram2 extends AppCompatActivity {

    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<?php\n" +
                "$txt1 = \"Learn PHP\";\n" +
                "$txt2 = \"W3Schools.com\";\n" +
                "$x = 5;\n" +
                "$y = 4;\n" +
                "\n" +
                "echo \"<h2>\" . $txt1 . \"</h2>\";\n" +
                "echo \"Study PHP at \" . $txt2 . \"<br>\";\n" +
                "echo $x + $y;\n" +
                "?>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "Study PHP at W3Schools.com\n" +
                "9");

    }

}
