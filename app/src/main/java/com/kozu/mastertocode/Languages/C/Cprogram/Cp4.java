package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp4 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {  \n" +
                "    char c;\n" +
                "    printf(\"Enter a character: \");\n" +
                "    scanf(\"%c\", &c);  \n" +
                "    \n" +
                "    // %d displays the integer value of a character\n" +
                "    // %c displays the actual character\n" +
                "    printf(\"ASCII value of %c = %d\", c, c);\n" +
                "    \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a character: G\n" +
                "ASCII value of G = 71");

    }
}
