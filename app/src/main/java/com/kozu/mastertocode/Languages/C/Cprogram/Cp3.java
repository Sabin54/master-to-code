package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp3 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    double a, b, product;\n" +
                "    printf(\"Enter two numbers: \");\n" +
                "    scanf(\"%lf %lf\", &a, &b);  \n" +
                " \n" +
                "    // Calculating product\n" +
                "    product = a * b;\n" +
                "\n" +
                "    // Result up to 2 decimal point is displayed using %.2lf\n" +
                "    printf(\"Product = %.2lf\", product);\n" +
                "    \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter two numbers: 2.4\n" +
                "1.12\n" +
                "Product = 2.69");

    }
}
