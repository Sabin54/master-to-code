package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession5 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    TextView title1,title2,title3,title4;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Types of Array");
        title2.setText("2. Single Dimensional Array");
        title3.setText("3. Multi Dimensional Array");
        title4.setText("4. Strings");

        content1.setText("1. Single Dimensional Array\n" +
                "2. Multi Dimensional Array");

        content2.setText("One-dimensional array is a structured collection of array element that can be accessed individually by specifying the position with a single indexed value.\n" +
                "\n" +
                "Declaring Single Dimensional Array:\n" +
                "<data-type> <array-name> [size];\n" +
                "\n" +
                "Example:\n" +
                "int a[3] = [10,20,30];");

        content3.setText("Multi-dimensional array is nothing just array of arrays. we can declear multi-dimensional array as below:\n" +
                "\n" +
                "Declaring Multi Dimensional Array:\n" +
                "<data-type> <array-name> [size][size1][size2];\n" +
                "\n");

        content4.setText("Strings are actually one-dimensional array of characters terminated by a null character '\\0'. Thus a null-terminated string contains the characters that comprise the string followed by a null.\n" +
                "\n" +
                "C supports a wide range of functions that manipulate null-terminated strings :\n" +
                "\n" +
                "1. strcpy(s1, s2);\n" +
                "\tCopies string s2 into string s1.\n" +
                "\n" +
                "2. strcat(s1, s2);\n" +
                "\tConcatenates string s2 onto the end of string s1.\n" +
                "\n" +
                "3. strlen(s1);\n" +
                "\tReturns the length of string s1.\n" +
                "\n" +
                "4. strcmp(s1, s2);\n" +
                "\tReturns 0 if s1 and s2 are the same; less than 0 if s1<s2; greater than 0 if s1>s2.\n" +
                "\n" +
                "5. \tstrchr(s1, ch);\n" +
                "\tReturns a pointer to the first occurrence of character ch in string s1.\n" +
                "\n" +
                "6. strstr(s1, s2);\n" +
                "\tReturns a pointer to the first occurrence of string s2 in string s1.");
    }


}
