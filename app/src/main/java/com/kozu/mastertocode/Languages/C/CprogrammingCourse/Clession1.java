package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession1 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("C programming is a general-purpose, procedural, imperative computer programming language developed in 1972 by " +
                "Dennis M. Ritchie at the Bell Telephone Laboratories to develop the UNIX operating system. " +
                "C is the most widely used computer language. It keeps fluctuating at number one scale of popularity along with Java programming language," +
                " which is also equally popular and most widely used among modern software programmers.");

        content2.setText("C programming language is a MUST for students and working professionals to become a great Software Engineer specially when they are working in Software Development Domain. I will list down some of the key advantages of learning C Programming:\n" +
                "\n" +
                "\t1. Easy to learn\n" +
                "\n" +
                "\t2. Structured language\n" +
                "\n" +
                "\t3. It produces efficient programs\n" +
                "\n" +
                "\t4. It can handle low-level activities\n" +
                "\n" +
                "\t5. It can be compiled on a variety of computer platforms");

        content3.setText("- C was invented to write an operating system called UNIX.\n" +
                "\n" +
                "- C is a successor of B language which was introduced around the early 1970s.\n" +
                "\n" +
                "- The language was formalized in 1988 by the American National Standard Institute (ANSI).\n" +
                "\n" +
                "- The UNIX OS was totally written in C.\n" +
                "\n" +
                "- Today C is the most widely used and popular System Programming Language.\n" +
                "\n" +
                "- Most of the state-of-the-art software have been implemented using C.\n" +
                "\n" +
                "- Today's most popular Linux OS and RDBMS MySQL have been written in C.");

        content4.setText("#include <stdio.h>\n" +
                "\n" +
                "int main() {\n" +
                "   /* my first program in C */\n" +
                "   printf(\"Hello, World! \\n\");\n" +
                "   \n" +
                "   return 0;\n" +
                "}");


    }
}