package com.kozu.mastertocode.Languages.C;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.navigation.NavigationView;
import com.kozu.mastertocode.Languages.C.Cprogram.CprogramList;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession1;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession2;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession3;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession4;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession5;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession6;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession7;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession8;
import com.kozu.mastertocode.Languages.C.CprogrammingCourse.Clession9;
import com.kozu.mastertocode.Languages.C.Cquiz.CQuiz;
import com.kozu.mastertocode.Languages.JAVA.CourseContent.Fragment3;
import com.kozu.mastertocode.R;

public class CprogramCourseContent extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    CardView title1, title2, title3, title4, title5, title6, title7, title8, title9, title10, title13;
    InterstitialAd mInterstitialAd;

    private DrawerLayout drawer_layout;
    private ActionBarDrawerToggle aToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c__programming_course_content);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);
        title6 = findViewById(R.id.title6);
        title7 = findViewById(R.id.title7);
        title8 = findViewById(R.id.title8);
        title9 = findViewById(R.id.title9);
        //title10 = findViewById(R.id.title10);
        drawer_layout = findViewById(R.id.drawer_layout);


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7031830486098035/9463900861");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        drawer_layout = findViewById(R.id.drawer_layout);
        aToggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.open, R.string.close);
        drawer_layout.addDrawerListener(aToggle);
        aToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

        title1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession1.class);
                startActivity(intent);
            }
        });
        title2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession2.class);
                startActivity(intent);
            }
        });

        title3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession3.class);
                startActivity(intent);
            }
        });

        title4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession4.class);
                startActivity(intent);
            }
        });
        title5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession5.class);
                startActivity(intent);
            }
        });
        title6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession6.class);
                startActivity(intent);
            }
        });

        title7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession7.class);
                startActivity(intent);
            }
        });

        title8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession8.class);
                startActivity(intent);
            }
        });

        title9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Clession9.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (aToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id=menuItem.getItemId();
        if(id==R.id.Tutorial)
        {
            drawer_layout.close();
        }
        if(id==R.id.Program)

        {
            mInterstitialAd.show();
            Intent intent = new Intent(this, CprogramList.class);
            startActivity(intent);
        }
        if(id==R.id.Quiz)
        {
            mInterstitialAd.show();
            Intent intent = new Intent(this, CQuiz.class);
            startActivity(intent);

        }

        return true;
    }
}



