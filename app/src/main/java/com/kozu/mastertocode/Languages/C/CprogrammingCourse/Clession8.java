package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession8 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    TextView title1, title2, title3, title4;
    AdView adView, adView1;

    CardView cv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        cv4 = findViewById(R.id.cv4);

        cv4.setVisibility(View.GONE);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. C Functions");
        title2.setText("2. Standard library functions");
        title3.setText("3. User-defined functions");


        content1.setText("A function is a block of code that performs a specific task.\n" +
                "\n" +
                "Suppose, you need to create a program to create a circle and color it. You can create two functions to solve this problem:\n" +
                "\n" +
                "\t1. create a circle function\n" +
                "\t2. create a color function\n" +
                "\n" +
                "Dividing a complex problem into smaller chunks makes our program easy to understand and reuse.\n" +
                "\n" +
                "\n" +
                "Types of function:\n" +
                "\n" +
                "\t1. Standard library functions\n" +
                "\t2. User-defined functions\n" +
                "\n");

        content2.setText("C Standard library functions or simply C Library functions are inbuilt functions in C programming.\n" +
                "\n" +
                "The prototype and data definitions of these functions are present in their respective header files. To use these functions we need to include the header file in our program. For example,\n" +
                "\n" +
                "If you want to use the printf() function, the header file <stdio.h> should be included.\n" +
                "\n" +
                "Library Functions in Different Header Files:\n" +
                "\n" +
                "1. <assert.h>\n" +
                "\tProgram assertion functions\n" +
                "\n" +
                "2. <ctype.h>\n" +
                "\tCharacter type functions\n" +
                "\n" +
                "3. <locale.h>\n" +
                "\tLocalization functions\n" +
                "\n" +
                "4. <math.h>\n" +
                "\tMathematics functions\n" +
                "\n" +
                "5. <setjmp.h>\n" +
                "\tJump functions\n" +
                "\n" +
                "6. <signal.h>\n" +
                "\tSignal handling functions\n" +
                "\n" +
                "7. <stdarg.h>\n" +
                "\tVariable arguments handling functions\n" +
                "\n" +
                "8. <stdio.h>\n" +
                "\tStandard Input/Output functions\n" +
                "\n" +
                "9. <stdlib.h>\n" +
                "\tStandard Utility functions\n" +
                "\n" +
                "10. <string.h>\n" +
                "\tString handling functions\n" +
                "\n" +
                "11. <time.h>\n" +
                "\tDate time functions");

        content3.setText("A function is a block of code that performs a specific task.\n" +
                "\n" +
                "C allows you to define functions according to your need. These functions are known as user-defined functions. For example:\n" +
                "\n" +
                "Suppose, you need to create a circle and color it depending upon the radius and color. You can create two functions to solve this problem:\n" +
                "\n" +
                "\t1. createCircle() function\n" +
                "\t2. color() function");
    }
}
