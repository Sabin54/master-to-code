package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession9 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    CardView cv4, cv3;

    TextView title1, title2, title3, title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        //cv3.setVisibility(View.GONE);
        cv4.setVisibility(View.GONE);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Introduction");
        title2.setText("2. Function Templates");
        title3.setText("3. Class Templates");

        content1.setText("Templates are powerful features of C++ which allows you to write generic programs. In simple terms, you can create a single function or a class to work with different data types using templates.\n" +
                "\n" +
                "Templates are often used in larger codebase for the purpose of code reusability and flexibility of the programs.\n" +
                "\n" +
                "The concept of templates can be used in two different ways:\n" +
                "\n" +
                "1. Function Templates\n" +
                "2. Class Templates\n");

        content2.setText("A function template works in a similar to a normal function, with one key difference.\n" +
                "\n" +
                "A single function template can work with different data types at once but, a single normal function can only work with one set of data types.\n" +
                "\n" +
                "Normally, if you need to perform identical operations on two or more types of data, you use function overloading to create two functions with the required function declaration.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "   #include <iostream>\n" +
                "   using namespace std;\n" +
                "\n" +
                "   // template function\n" +
                "   template <class T>\n" +
                "   T Large(T n1, T n2)\n" +
                "   {\n" +
                "      return (n1 > n2) ? n1 : n2;\n" +
                "   }\n" +
                "\n" +
                "   int main()\n" +
                "   {\n" +
                "      int i1, i2;\n" +
                "      cout << \"Enter two integers:\\n\";\n" +
                "      cin >> i1 >> i2;\n" +
                "      cout << Large(i1, i2) <<\" is larger.\" << endl;\n" +
                "      return 0;\n" +
                "   }\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter two integers:\n" +
                "5\n" +
                "10\n" +
                "10 is larger.\n");

        content3.setText("Like function templates, you can also create class templates for generic class operations.\n" +
                "\n" +
                "Sometimes, you need a class implementation that is same for all classes, only the data types used are different.\n" +
                "\n" +
                "Normally, you would need to create a different class for each data type OR create different member variables and functions within a single class.\n" +
                "\n" +
                "This will unnecessarily bloat your code base and will be hard to maintain, as a change is one class/function should be performed on all classes/functions.\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "   template <class T>\n" +
                "   class className\n" +
                "   {\n" +
                "      ... .. ...\n" +
                "   public:\n" +
                "      T var;\n" +
                "      T someOperation(T arg);\n" +
                "      ... .. ...\n" +
                "   };\n");

    }
}
