package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program6 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "class PrimeCheck\n" +
                "{\n" +
                "   public static void main(String args[])\n" +
                "   {        \n" +
                "    int temp;\n" +
                "    boolean isPrime=true;\n" +
                "    Scanner scan= new Scanner(System.in);\n" +
                "    System.out.println(\"Enter any number:\");\n" +
                "    //capture the input in an integer\n" +
                "    int num=scan.nextInt();\n" +
                "        scan.close();\n" +
                "    for(int i=2;i<=num/2;i++)\n" +
                "    {\n" +
                "           temp=num%i;\n" +
                "       if(temp==0)\n" +
                "       {\n" +
                "          isPrime=false;\n" +
                "          break;\n" +
                "       }\n" +
                "    }\n" +
                "    //If isPrime is true then the number is prime else not\n" +
                "    if(isPrime)\n" +
                "       System.out.println(num + \" is a Prime Number\");\n" +
                "    else\n" +
                "       System.out.println(num + \" is not a Prime Number\");\n" +
                "   }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter any number:\n" +
                "19\n" +
                "19 is a Prime Number\n" +
                "Output 2:\n" +
                "\n" +
                "\n" +
                "Enter any number:\n" +
                "6\n" +
                "6 is not a Prime Number");
    }
}
