package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kozu.mastertocode.R;

public class CplusPrograms extends AppCompatActivity {

    CardView t1 , t2, t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13
            ,t14 , t15, t16,t17,t18,t19,t20,t21,t22,t23;
    TextView title1, title2, title3, title4,title5, title6, title7, title8,title9, title10, title11, title12,title13, title14, title15, title16;
    AdView adView, adView1;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_programs);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7031830486098035/9463900861");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        t3 = findViewById(R.id.t3);
        t4 = findViewById(R.id.t4);
        t5 = findViewById(R.id.t5);
        t6 = findViewById(R.id.t6);
        t7 = findViewById(R.id.t7);
        t8 = findViewById(R.id.t8);
        t9 = findViewById(R.id.t9);
        t10 = findViewById(R.id.t10);
        t11 = findViewById(R.id.t11);
        t12 = findViewById(R.id.t12);
        t13 = findViewById(R.id.t13);
        t14 = findViewById(R.id.t14);
        t15 = findViewById(R.id.t15);
        t16 = findViewById(R.id.t16);
        t17 = findViewById(R.id.t17);
        t18 = findViewById(R.id.t18);
        t19 = findViewById(R.id.t19);
        t20 = findViewById(R.id.t20);
        t21 = findViewById(R.id.t21);
        t22 = findViewById(R.id.t22);
        t23 = findViewById(R.id.t23);

        t9.setVisibility(View.GONE);
        t13.setVisibility(View.GONE);
        t17.setVisibility(View.GONE);
        t18.setVisibility(View.GONE);
        t19.setVisibility(View.GONE);
        t20.setVisibility(View.GONE);
        t21.setVisibility(View.GONE);
        t22.setVisibility(View.GONE);
        t23.setVisibility(View.GONE);



        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);
        title6 = findViewById(R.id.title6);
        title7 = findViewById(R.id.title7);
        title8 = findViewById(R.id.title8);
        title10 = findViewById(R.id.title10);
        title11 = findViewById(R.id.title11);
        title12 = findViewById(R.id.title12);
        title13 = findViewById(R.id.title13);
        title14 = findViewById(R.id.title14);
        title15 = findViewById(R.id.title15);
        title16 = findViewById(R.id.title16);

        title1.setText("Program to find Greatest of Three Numbers");
        title2.setText("Program to find Divisor of a Number");
        title3.setText("Program to find Prime Number");
        title4.setText("Program for Checking Armstrong Number");
        title5.setText("Program to find Factorial of a Number");
        title6.setText("Program to find Fibonacci Series upto n terms");
        title7.setText("Program for Simple Calculator Program");
        title8.setText("Program to print Half Pyramid using *");
        title10.setText("Program to print Pascal Triangle");
        title11.setText("Program to Swap Two Numbers");
        title12.setText("Program to print Palindrome Program");
        //title13.setText("Matrix Operation on 2D Array");
        title14.setText("Program to perform  Transpose of Matrix");
        title15.setText("Program to perform Call by Value ");
        title16.setText("Program to perform Inheritance");


        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP1.class);
                startActivity(intent);
            }
        });
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP2.class);
                startActivity(intent);
            }
        });
        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP3.class);
                startActivity(intent);
            }
        });
        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP4.class);
                startActivity(intent);
            }
        });
        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP5.class);
                startActivity(intent);
            }
        });
        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP6.class);
                startActivity(intent);
            }
        });
        t7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP7.class);
                startActivity(intent);
            }
        });
        t8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP8.class);
                startActivity(intent);
            }
        });
        t10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP10.class);
                startActivity(intent);
            }
        });
        t11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP11.class);
                startActivity(intent);
            }
        });
        t12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP12.class);
                startActivity(intent);
            }
        });
        t14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP14.class);
                startActivity(intent);
            }
        });
        t15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP15.class);
                startActivity(intent);
            }
        });
        t16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),CPP16.class);
                startActivity(intent);
            }
        });






    }
}
