package com.kozu.mastertocode.Languages.C.Cquiz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.kozu.mastertocode.R;

import java.util.Random;

public class CQuiz extends AppCompatActivity {
    Button ans1,ans2,ans3,ans4,next;
    TextView questions, ans;
    LinearLayout lay1;

    Random randomGenerator = new Random();

    int quesNum;
    private Cquestions mQuestion = new Cquestions();
    private String mAnswer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz);

        ans1 = findViewById(R.id.ans1);
        ans2 = findViewById(R.id.ans2);
        ans3 = findViewById(R.id.ans3);
        ans4 = findViewById(R.id.ans4);
        next = findViewById(R.id.next);
        ans = findViewById(R.id.ans);
        lay1 = findViewById(R.id.lay1);

        lay1.setVisibility(View.INVISIBLE);

        questions = findViewById(R.id.questions);
        updateQuestion();

        ans1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ans1.getText().toString().equals(mAnswer))
                {
                    ans1.setBackgroundColor(getResources().getColor(R.color.green));
                }
                else
                {
                    ans1.setBackgroundColor(getResources().getColor(R.color.red));
                    lay1.setVisibility(View.VISIBLE);
                    ans.setText(mAnswer);
                }
            }
        });
        ans2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ans2.getText().toString().equals(mAnswer))
                {
                    ans2.setBackgroundColor(getResources().getColor(R.color.green));
                }
                else
                {
                    ans2.setBackgroundColor(getResources().getColor(R.color.red));
                    lay1.setVisibility(View.VISIBLE);
                    ans.setText(mAnswer);
                }
            }
        });
        ans3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ans3.getText().toString().equals(mAnswer))
                {
                    ans3.setBackgroundColor(getResources().getColor(R.color.green));
                }
                else
                {
                    ans3.setBackgroundColor(getResources().getColor(R.color.red));
                    lay1.setVisibility(View.VISIBLE);
                    ans.setText(mAnswer);
                }
            }
        });
        ans4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ans4.getText().toString().equals(mAnswer))
                {
                    ans4.setBackgroundColor(getResources().getColor(R.color.green));
                }
                else
                {
                    ans4.setBackgroundColor(getResources().getColor(R.color.red));
                    lay1.setVisibility(View.VISIBLE);
                    ans.setText(mAnswer);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateQuestion();
            }
        });
    }

    private void updateQuestion() {

        load();
        lay1.setVisibility(View.INVISIBLE);
        quesNum = randomGenerator.nextInt(20);
        questions.setText(mQuestion.getQuestion(quesNum));
        ans1.setText(mQuestion.getAns1(quesNum));
        ans2.setText(mQuestion.getAns2(quesNum));
        ans3.setText(mQuestion.getAns3(quesNum));
        ans4.setText(mQuestion.getAns4(quesNum));

        mAnswer = mQuestion.getCorrectAnswer(quesNum);
        quesNum++;
    }

    public void load()
    {
        ans1.setBackgroundColor(getResources().getColor(R.color.black));
        ans2.setBackgroundColor(getResources().getColor(R.color.black));
        ans3.setBackgroundColor(getResources().getColor(R.color.black));
        ans4.setBackgroundColor(getResources().getColor(R.color.black));

    }
}
