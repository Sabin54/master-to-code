package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program1 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("class pattern\n" +
                "{\n" +
                "    public static void main(String args[])\n" +
                "    {\n" +
                "        int i,j,k;\n" +
                "        for(i=0;i<=5;i++)\n" +
                "        {\n" +
                "            for(j=1;j<=i;j++)\n" +
                "            {\n" +
                "                System.out.print(\" \");\n" +
                "            }\n" +
                "            for(k=5-i;k>=0;k--)\n" +
                "            {\n" +
                "                System.out.print(\"* \");\n" +
                "            }\n" +
                "            System.out.println(\" \");\n" +
                "        }\n" +
                "    }\n" +
                "}\n" +
                "Output\n" +
                "\n" +
                "*   *    *    * \n" +
                "   *   *    *\n" +
                "     *    *\n" +
                "        *");

    }
}
