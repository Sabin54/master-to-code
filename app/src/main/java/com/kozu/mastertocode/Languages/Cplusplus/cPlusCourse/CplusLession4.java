package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession4 extends AppCompatActivity {

    TextView content1, content2, content3, content4, content5, content6, content7;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag3);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("The general form of a simple if statement is,\n" +
                "\n" +
                "if(expression)\n" +
                "{\n" +
                "    statement-inside;\n" +
                "}\n" +
                "    statement-outside;\n" +
                "\n" +
                "    \n" +
                "If the expression is true, then 'statement-inside' will be executed, otherwise 'statement-inside' is skipped and only 'statement-outside' will be executed.");

        content2.setText("If statement block with else statement is known as as if...else statement. \n" +
                "Else portion is non-compulsory.\n" +
                "Example IF, ELSE IF and Else Statement: \n" +
                "\tif ( condition_one ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t } \n" +
                "\telse if ( condition_two ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t }\n" +
                "\t else \n" +
                "\t{\n" +
                "\t //statements\n" +
                "\t }\n" +
                "\n" +
                "If first condition is true, then compiler will execute the if block, And if First condition is false it will check the second condition available in else if block of statements, if it is true else if block will execute and  if false then else block of statements will be executed.\n" +
                "\n");

        content3.setText("when a series of decisions are involved, we may have to use more than one if...else statement in nested form as follows:\n" +
                "if (test condition1)\n" +
                " { \n" +
                "\tIf (test condition2) \n" +
                "\t{ \n" +
                "\t//statement1; \n" +
                "\t}\n" +
                "\t Else\n" +
                "\t {\n" +
                "\t //statement2; \n" +
                "\t} \n" +
                "} \n" +
                "else \n" +
                "{ \n" +
                "\t//statement3; \n" +
                "}\n" +
                "\n");

        content4.setText("A switch statement is used instead of nested if...else statements. \n" +
                "It is multiple branch decision statement. A switch statement tests a variable with list of values for equivalence. Each value is called a case. The case value must be a constant i.\n" +
                "\n" +
                "SYNTAX \n" +
                "switch(expression) {\n" +
                "  \tcase x:\n" +
                "   \t\t// code block\n" +
                "    \t\tbreak;\n" +
                "  \tcase y:\n" +
                "   \t\t // code block\n" +
                "    \t\tbreak;\n" +
                "  \tdefault:\n" +
                "    \t\t// code block\n" +
                "}\n" +
                "\n");

        content5.setText("while loop can be address as an entry control loop. It is completed in 3 steps.\n" +
                "\n" +
                "Variable initialization.(e.g int x=0;)\n" +
                "condition(e.g while( x<=10))\n" +
                "Variable increment or decrement (x++ or x-- or x=x+2)\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "variable initialization;\n" +
                "while (condition)\n" +
                "{\n" +
                "    statements;\n" +
                "    variable increment or decrement; \n" +
                "}");

        content6.setText("In some situations it is necessary to execute body of the loop before testing the condition. Such situations can be handled with the help of do-while loop. do statement evaluates the body of the loop first and at the end, the condition is checked using while statement. General format of do-while loop is,\n" +
                "\n" +
                "do\n" +
                "{\n" +
                "    // a couple of statements\n" +
                "}\n" +
                "while(condition);");

        content7.setText("for loop is used to execute a set of statement repeatedly until a particular condition is satisfied. we can say it an open ended loop. General format is,\n" +
                "\n" +
                "for(initialization; condition; increment/decrement)\n" +
                "{\n" +
                "    statement-block;\n" +
                "}\n" +
                "In for loop we have exactly two semicolons, one after initialization and second after condition. In this loop we can have more than one initialization or increment/decrement, separated using comma operator. for loop can have only one condition.");

    }
}
