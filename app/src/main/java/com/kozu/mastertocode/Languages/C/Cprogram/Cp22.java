package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp22 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int n, i;\n" +
                "    printf(\"Enter an integer: \");\n" +
                "    scanf(\"%d\", &n);\n" +
                "    for (i = 1; i <= 10; ++i) {\n" +
                "        printf(\"%d * %d = %d \\n\", n, i, n * i);\n" +
                "    }\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter an integer: 9\n" +
                "9 * 1 = 9\n" +
                "9 * 2 = 18\n" +
                "9 * 3 = 27\n" +
                "9 * 4 = 36\n" +
                "9 * 5 = 45\n" +
                "9 * 6 = 54\n" +
                "9 * 7 = 63\n" +
                "9 * 8 = 72\n" +
                "9 * 9 = 81\n" +
                "9 * 10 = 90");

    }
}
