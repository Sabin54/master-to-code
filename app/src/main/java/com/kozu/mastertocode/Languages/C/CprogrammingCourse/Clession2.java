package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession2 extends AppCompatActivity {

    TextView content1, content2, content3, content4, content5, content6, content7, content8;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag2);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("Every variable in C has a data type. Data types specify the size and type of values that can be stored.\n" +
                "\n" +
                "Integer: This group includes byte, short, int and long, which are whole signed numbers. \n" +
                "\n" +
                "Floating-point Numbers: This group includes float and double, which represent number with fraction precision. \n" +
                "\n" +
                "Characters: This group includes char, which represents character set like letters and number \n" +
                "\n" +
                "Boolean: This group includes Boolean, which is special type of representation of true or false value.\n" +
                "\n" +
                "Some data types with their range and size: byte: -128 to 127 (1 byte)\n" +
                "\n" +
                "\n\tshort: -32,768 to +32,767 (2 bytes)\n" +
                "\tint: -2,147,483,648 to +2,147,483,647 (4 bytes)\n" +
                "\tfloat: 3.4e-038 to 1.7e+0.38 (4 bytes)\n" +
                "\tdouble: 3.4e-038 to 1.7e+308 (8 bytes)\n" +
                "\tchar:  Holds single character only (2 bytes)\n" +
                "\tBoolean:  Holds Single Character Only (2 bytes)\n");


        content3.setText("Increment and decrement operators are used to add or subtract 1 from the current value of oprand. ++ increment -- decrement\n" +
                "Increment and Decrement operators can be prefix or postfix. In the prefix style the value of oprand is changed before the result of expression and in the postfix style the variable is modified after result.\n" +
                "For Example:\n" +
                " \n" +
                "\ta = 9; \n" +
                "\tb = a++ + 5; \n" +
                "\t/* Output:  a=10, b=14 */\n" +
                "\n" +
                "\ta = 9;\n" +
                " \tb = ++a + 5; \n" +
                "\t/* Output:   a=10 ,b=15 */\n");

        content4.setText("C defines several bitwise operators, which can be applied to the integer types, long, int, short, char, and byte. \n" +
                "Bitwise operator works on bits and performs bit-by-bit operation. \n" +
                "\n" +
                "\t~ Unary bitwise complement \n" +
                "\t<< Signed left shift \n" +
                "\t> Signed right shift \n" +
                "\t>> Unsigned right shift \n" +
                "\t& Bitwise AND\n" +
                "\t^ Bitwise exclusive OR \n" +
                "\t| Bitwise inclusive OR\n" +
                "\n");

        content5.setText("The following table lists the logical operators:\n" +
                "\t&& Conditional-AND \n" +
                "\t|| Conditional-OR \n" +
                "\t?: Ternary (shorthand for if-then-else statement)\n");

        content6.setText("There are following relational operators supported by C language:\n" +
                "\t>Greater than\n" +
                "\t< Less than \n" +
                "\t== Equal to \n" +
                "\t!= Not equal to \n" +
                "\t>= Greater than or equal to\n" +
                "\t<= Less than or equal to\n");

        content7.setText("int a = 20; \n" +
                "int b = 10; \n" +
                "int result; \n" +
                "result = a + b; // 30 \n" +
                "result = a - b; // 10 \n" +
                "result = a * b; // 200 \n" +
                "result = a / b; // 2 \n" +
                "result = a % b; // 0\n" +
                "\nBasic math operations can be applied to int, double and float data types:\n" +
                "•\t+ addition\n" +
                "•\t- subtraction\n" +
                "•\t* multiplication\n" +
                "•\t/ division\n" +
                "•\t% modulo (yields the remainder)\n" +
                "\n\nThese operations are not supported for other data types.\n");

        content8.setText("int a = 5; \n" +
                "int b = 3;\n" +
                "boolean result = a > b; // result now holds the boolean value true\n" +
                "Comparison operators can be used to compare two values:\n" +
                "•\t> greater than\n" +
                "•\t< less than\n" +
                "•\t>= greater than or equal to\n" +
                "•\t<= less than or equal to\n" +
                "•\t== equal to\n" +
                "•\t!= not equal to\n" +
                "\n\nThey are supported for primitive data types and the result of a comparison is a boolean value true or false.\n");


    }

}
