package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment5 extends AppCompatActivity {
    TextView content1, content2;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag5);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("Array is an object the contains elements of similar data type. It is a data structure where we store similar elements. We can store only fixed set of elements in a java array. Array in java is index based, first element of the array is stored at 0 index.\n" +
                "Types of Array in java\n" +
                "\n1. Single Dimensional Array \n" +
                "2. Multidimensional Array\n" +
                "\n" +
                "1.\tSingle Dimensional Array \n" +
                "Declaring Array Variables: \n" +
                "\nTo use an array in a program, you must declare a variable to reference the array, and you must specify the type of array the variable can reference. Here is the syntax for declaring an array variable:\n" +
                "\tdataType[] arrayName; \n" +
                "\tor \n" +
                "\tdataType arrayName[];\n" +
                "\n" +
                "Instantiating Arrays: \n" +
                "You can instantiate an array by using the new operator with the following syntax:\n" +
                "\n" +
                "\t\tarrayName = new dataType[arraySize];\n" +
                "\n" +
                "The above statement does two things: \n" +
                "It creates an array using new datatype[arraySize]; It assigns the reference of the newly created array to the variable array Name.\n" +
                "\n" +
                "2.\tMultidimensional Array \n" +
                "\n" +
                "Syntax to Declare Multidimensional Array in java\n" +
                "\n" +
                "\tdataType[][] arrayName;\n" +
                " \tor\n" +
                "\tdataType arrayName[][];\n" +
                "\n" +
                "Example to instantiate Multidimensional Array\n" +
                " int[][] arr=new int[2][3]; //2 row and 3 column \n" +
                "\n" +
                "Example to initialize Multidimensional Array in java \n" +
                "\tarr[0][0]=1; \n" +
                "\tarr[0][1]=2; \n" +
                "\tarr[0][2]=3; \n" +
                "\tarr[1][0]=4; \n" +
                "\tarr[1][1]=5; \n" +
                "\tarr[1][2]=6; \n" +
                "\nPassing Arrays to Methods: \n" +
                "Just as you can pass primitive type values to methods, you can also pass arrays to methods. For example, the following method displays the elements in an int array: \n" +
                "\npublic static void display(int[] arr) \n" +
                "{ \n" +
                "\tfor (int i = 0; i < arr.length; i++)\n" +
                " \t{ \n" +
                "\t\tSystem.out.print(arr[i] + \" \"); \n" +
                "\t}\n" +
                " }\n" +
                "\n" +
                "Returning an Array from a Method: \n" +
                "A method may also return an array. For example, the method shown below returns an array that is the copy of another array: \n" +
                "\n" +
                "public static int[] copyarray(int[] list) \n" +
                "{ int[] result = new int[list.length];\n" +
                "\n" +
                "\tfor (int i = 0; i < list.length - 1 ; i++) \n" +
                "\t{ \n" +
                "\t\tresult[i] = list[i];\n" +
                " \t}\n" +
                " \t\treturn result; \n" +
                "}\n" +
                "\n" +
                "Arrays Methods : \n" +
                "\n" +
                "Arrays.binarySearch(Object[] a, Object key)\n" +
                "\n\tSearches the specified array of Object ( Byte, Int , double, etc.) for the specified value using the binary search algorithm. The array must be sorted prior to making this call. This returns index of the search key.\n" +
                "\n" +
                "Arrays.equals(long[] a, long[] a2) \n" +
                "\n\tReturns true if the two specified arrays of longs are equal to one another. Two arrays are considered equal if both arrays contain the same number of elements, and all corresponding pairs of elements in the two arrays are equal.\n" +
                "\n" +
                "Arrays.fill(int[] a, int val) \n" +
                "\n\tAssigns the specified int value to each element of the specified array of ints. Same method could be used by all other primitive data types (Byte, short, Int etc.)\n" +
                "\n" +
                "Arrays.sort(Object[] a) \n" +
                "\n\tSorts the specified array of objects into ascending order, according to the natural ordering of its elements. Same method could be used by all other primitive data types ( Byte, short, Int, etc.\n" +
                "\n");


        content2.setText("string is basically an object that represents sequence of char values. The Java platform provides the String class to create and manipulate strings.\n" +
                "\n" +
                "Creating Strings: \n" +
                "\n" +
                "1) The most direct way to create a string is to write: \n" +
                "String str1 = \"Hello Java!\";\n" +
                "\n" +
                "2) Using another String object \n" +
                "String str2 = new String(str1);\n" +
                "\n" +
                "3) Using new Keyword \n" +
                "String str3 = new String(\"Java\");\n" +
                "\n" +
                "4) Using + operator (Concatenation) \n" +
                "String str4 = str1 + str2;\n" +
                " or, \n" +
                "String str5 = \"hello\"+\"Java\";\n" +
                "\n" +
                "String Length: length() method \n" +
                "returns the number of characters contained in the string object. \n" +
                "\n" +
                "String str1 = \"Hello Java\"; \n" +
                "int len = str1.length(); \n" +
                "System.out.println( \"String Length is : \" + len );\n" +
                "\n" +
                "Concatenating Strings: \n" +
                "The String class includes a method for concatenating two strings: string1.concat(string2); This returns a new string that is string1 with string2 added to it at the end. You can also use the concat() method with string literals, as in: \n" +
                "\"Hello \".concat(\"Java\"); \n" +
                "Strings are more commonly concatenated with the + operator, as in:\n" +
                " \"Hello \" + \" Java\" + \"!\" \n" +
                "which results in: \n" +
                "\"Hello Java!\"\n" +
                "\n" +
                "String Methods : \n" +
                "\n" +
                "1 char charAt(int index) \n" +
                "returns char value for the particular index\n" +
                "\n" +
                "2 int length() \n" +
                "returns string length\n" +
                "\n" +
                "3 static String format(String format, Object... args)\n" +
                " returns formatted string\n" +
                "\n" +
                "4 static String format(Locale l, String format, Object... args) \n" +
                "returns formatted string with given locale\n" +
                "\n" +
                "5 String substring(int beginIndex) \n" +
                "returns substring for given begin index\n" +
                "\n" +
                "6 String substring(int beginIndex, int endIndex) \n" +
                "returns substring for given begin index and end index\n" +
                "\n" +
                "7 boolean contains(CharSequence s) \n" +
                "returns true or false after matching the sequence of char value\n" +
                "\n" +
                "8 static String join(CharSequence delimiter, CharSequence... elements) \n" +
                "returns a joined string\n" +
                "\n" +
                "9 static String join(CharSequence delimiter, Iterable<? extends CharSequence> elements) returns a joined string\n" +
                "\n" +
                "10 boolean equals(Object another) \n" +
                "checks the equality of string with object\n" +
                "\n" +
                "11 boolean isEmpty()\n" +
                " checks if string is empty\n" +
                "\n" +
                "12 String concat(String str) \n" +
                "concatinates specified string\n" +
                "\n" +
                "13 String replace(char old, char new) \n" +
                "replaces all occurrences of specified char value\n" +
                "\n" +
                "14 String replace(CharSequence old, CharSequence new) \n" +
                "replaces all occurrences of specified CharSequence\n" +
                "\n" +
                "15 String trim() \n" +
                "returns trimmed string omitting leading and trailing spaces\n" +
                "\n" +
                "16 String split(String regex) \n" +
                "returns splitted string matching regex\n" +
                "\n" +
                "17 String split(String regex, int limit)\n" +
                " returns splitted string matching regex and limit\n" +
                "\n" +
                "18 String intern() \n" +
                "returns interned string\n" +
                "\n" +
                "19 int indexOf(int ch)\n" +
                " returns specified char value index\n" +
                "\n" +
                "20 int indexOf(int ch, int fromIndex) \n" +
                "returns specified char value index starting with given index\n" +
                "\n" +
                "21 int indexOf(String substring) \n" +
                "returns specified substring index\n" +
                "\n" +
                "22 int indexOf(String substring, int fromIndex)\n" +
                " returns specified substring index starting with given index\n" +
                "\n" +
                "23 String toLowerCase() \n" +
                "returns string in lowercase.\n" +
                "\n" +
                "24 String toLowerCase(Locale l) \n" +
                "returns string in lowercase using specified locale.\n" +
                "\n" +
                "25 String toUpperCase() \n" +
                "returns string in uppercase.\n" +
                "\n");
    }
}
