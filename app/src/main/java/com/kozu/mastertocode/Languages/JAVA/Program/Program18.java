package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program18 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "\n" +
                "public class JavaExample{\n" +
                "   public static void main(String args[]){\n" +
                "      int temp1, temp2, num1, num2, temp, hcf;\n" +
                "      Scanner scanner = new Scanner(System.in);\n" +
                "\n" +
                "      System.out.print(\"Enter First Number: \");\n" +
                "      num1 = scanner.nextInt();\n" +
                "      System.out.print(\"Enter Second Number: \");\n" +
                "      num2 = scanner.nextInt();\n" +
                "      scanner.close();\n" +
                "\n" +
                "      temp1 = num1;\n" +
                "      temp2 = num2;\n" +
                "\n" +
                "      while(temp2 != 0){\n" +
                "         temp = temp2;\n" +
                "         temp2 = temp1%temp2;\n" +
                "         temp1 = temp;\n" +
                "      }\n" +
                "\n" +
                "      hcf = temp1;\n" +
                "\n" +
                "      System.out.println(\"HCF of input numbers: \"+hcf);\n" +
                "    \n" +
                "   }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter First Number:10\n" +
                "Enter Second Number:35\n" +
                "HCF of input numbers:5\n");
    }
}
