package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession7 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    TextView title1, title2, title3, title4;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. C structure");
        title2.setText("2. Accessing Structure Members");
        title3.setText("3. C Unions");
        title4.setText("4. Access members of a union");

        content1.setText("In C programming language we can use arrays when we want to hold multiple element to the homogeneous data type in single variable, but a struct (or structure) is a collection of variables (can be of different types) under a single name.\n" +
                "\n" +
                "Syntax: \n" +
                "\n" +
                "\tstruct struct_name(members);\n" +
                "\n" +
                "\n" +
                "Structure with Variable Syntax:\n" +
                "\n" +
                "\tstruct struct_name\n" +
                "\t{\n" +
                "\t\tStructure_member;\n" +
                "\t}instance1,instance2,instance_n;\n" +
                "\n");

        content2.setText("Accessing Structure Members\n" +
                "\n" +
                "Member operator '.' is used to access the individual structure members. It is also known as dot operator or period operator.\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "\tstructure_var.member;");

        content3.setText("A union is a user-defined type similar to structs in C except for one key difference. Structs allocate enough space to store all its members wheres unions allocate the space to store only the largest member.\n" +
                "\n" +
                "Syntax:\n" +
                "\tunion car\n" +
                "\t{\n" +
                "\t  char name[50];\n" +
                "\t  int price;\n" +
                "\t};\n" +
                "\n" +
                "Creating union variables:\n" +
                "\n" +
                "\tunion car\n" +
                "\t{\n" +
                "\t  char name[50];\n" +
                "\t  int price;\n" +
                "\t} car1, car2, *car3;");

        content4.setText("We use the . operator to access members of a union. To access pointer variables, we use also use the -> operator.\n" +
                "\n" +
                "In the above example,\n" +
                "\n" +
                "\t1. To access price for car1, car1.price is used.\n" +
                "\t2. To access price using car3, either (*car3).price or car3->price can be used.");
    }

}