package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp7 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    char c;\n" +
                "    int lowercase_vowel, uppercase_vowel;\n" +
                "    printf(\"Enter an alphabet: \");\n" +
                "    scanf(\"%c\", &c);\n" +
                "\n" +
                "    // evaluates to 1 if variable c is a lowercase vowel\n" +
                "    lowercase_vowel = (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');\n" +
                "\n" +
                "    // evaluates to 1 if variable c is a uppercase vowel\n" +
                "    uppercase_vowel = (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U');\n" +
                "\n" +
                "    // evaluates to 1 (true) if c is a vowel\n" +
                "    if (lowercase_vowel || uppercase_vowel)\n" +
                "        printf(\"%c is a vowel.\", c);\n" +
                "    else\n" +
                "        printf(\"%c is a consonant.\", c);\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter an alphabet: G\n" +
                "G is a consonant.");

    }
}
