package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program22 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.applet.*;\n" +
                "import java.awt.*;\n" +
                "public class input_user extends Applet\n" +
                "{\n" +
                "    TextField t1,t2;\n" +
                "    public void init()\n" +
                "    {\n" +
                "        Label l1=new Label(\"No1\");\n" +
                "        add(l1);\n" +
                "        t1=new TextField(10);\n" +
                "        add(t1);\n" +
                "        t1.setText(\"\");\n" +
                "        Label l2=new Label(\"No2\");\n" +
                "        add(l2);\n" +
                "        t2=new TextField(10);\n" +
                "        add(t2);\n" +
                "        t2.setText(\"\");\n" +
                "    }\n" +
                "    public void paint(Graphics g)\n" +
                "    {\n" +
                "        int x=0,y=0,a,b,c,d;\n" +
                "        String s,s1,s2,s3,s4,s5;\n" +
                "        g.drawString(\"Input no. in textbox\",10,20);\n" +
                "        s1=t1.getText();\n" +
                "        x=Integer.parseInt(s1);\n" +
                "        s2=t2.getText();\n" +
                "        y=Integer.parseInt(s2);\n" +
                "        a=x+y;\n" +
                "        b=x-y;\n" +
                "        c=x*y;\n" +
                "        d=x/y;\n" +
                "        s=String.valueOf(a);\n" +
                "        s3=String.valueOf(b);\n" +
                "        s4=String.valueOf(c);\n" +
                "        s5=String.valueOf(d);\n" +
                "        g.drawString(\"The sum is: \",10,75); \n" +
                "        g.drawString(s,80,75);\n" +
                "        g.drawString(\"The difference is: \",10,95);  \n" +
                "        g.drawString(s3,110,95);\n" +
                "        g.drawString(\"The mutiplication is: \",10,115);  \n" +
                "        g.drawString(s4,140,115);\n" +
                "        g.drawString(\"The division is: \",10,135);   \n" +
                "        g.drawString(s5,170,135);       \n" +
                "    }\n" +
                "    public boolean action(Event event,Object object)\n" +
                "    {\n" +
                "        repaint();\n" +
                "        return true;\n" +
                "    }\n" +
                "}\n" +
                "Html file\n" +
                "<HTML>\n" +
                "    <HEAD>\n" +
                "        <title>welcome to Applet</title>\n" +
                "    </HEAD>\n" +
                "    <BODY>\n" +
                "        <Applet\n" +
                "            code=\"input_user.class\"\n" +
                "            width=600\n" +
                "            height=500>\n" +
                "        </Applet>\n" +
                "    </BODY>\n" +
                "</HTML>");
    }
}
