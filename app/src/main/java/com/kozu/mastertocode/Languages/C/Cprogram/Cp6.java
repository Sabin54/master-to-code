package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp6 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int num;\n" +
                "    printf(\"Enter an integer: \");\n" +
                "    scanf(\"%d\", &num);\n" +
                "\n" +
                "    // True if num is perfectly divisible by 2\n" +
                "    if(num % 2 == 0)\n" +
                "        printf(\"%d is even.\", num);\n" +
                "    else\n" +
                "        printf(\"%d is odd.\", num);\n" +
                "    \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter an integer: -7\n" +
                "-7 is odd.");

    }
}
