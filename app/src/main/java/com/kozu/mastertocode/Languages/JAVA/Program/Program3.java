package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program3 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("public class AddTwoNumbers {\n" +
                "\n" +
                "   public static void main(String[] args) {\n" +
                "        \n" +
                "      int num1 = 5, num2 = 15, sum;\n" +
                "      sum = num1 + num2;\n" +
                "\n" +
                "      System.out.println(\"Sum of these numbers: \"+sum);\n" +
                "   }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Sum of these numbers: 20");
    }
}
