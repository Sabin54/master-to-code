package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession3 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5, content6, content7, content8, content9, content10;
    AdView adView, adView1;
    TextView title1, title2, title3, title4,title5, title6, title7, title8,title9, title10;

    CardView cv10, cv9, cv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag4);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);
        content9 = findViewById(R.id.content9);
        content10 = findViewById(R.id.content10);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);
        title6 = findViewById(R.id.title6);
        title7 = findViewById(R.id.title7);
        title8 = findViewById(R.id.title8);
        title9 = findViewById(R.id.title9);
        title10 = findViewById(R.id.title10);

        cv10 = findViewById(R.id.cv10);
        cv9 = findViewById(R.id.cv9);
        cv10.setVisibility(View.GONE);
        cv9.setVisibility(View.GONE);

        title1.setText("1. Assignment Operator");
        title2.setText("2. Arthemetic Operators");
        title3.setText("3. Increment/Decrement Operator");
        title4.setText("4. Relational Operators");
        title5.setText("5. Logical Operators");
        title6.setText("6. Ternary Operator");
        title7.setText("7. Comma Operator");
        title8.setText("8. Bitwise Operators");



        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("Operates '=' is used for assignment, it takes the right-hand side (called rvalue) and copy it into the left-hand side (called lvalue). Assignment operator is the only operator which can be overloaded but cannot be inherited.");

        content2.setText("int a = 20; \n" +
                "int b = 10; \n" +
                "int result; \n" +
                "result = a + b; // 30 \n" +
                "result = a - b; // 10 \n" +
                "result = a * b; // 200 \n" +
                "result = a / b; // 2 \n" +
                "result = a % b; // 0\n" +
                "\nBasic math operations can be applied to int, double and float data types:\n" +
                "•\t+ addition\n" +
                "•\t- subtraction\n" +
                "•\t* multiplication\n" +
                "•\t/ division\n" +
                "•\t% modulo (yields the remainder)\n" +
                "\n\nThese operations are not supported for other data types.\n");

        content3.setText("Increment and decrement operators are used to add or subtract 1 from the current value of oprand. ++ increment -- decrement\n" +
                "Increment and Decrement operators can be prefix or postfix. In the prefix style the value of oprand is changed before the result of expression and in the postfix style the variable is modified after result.\n" +
                "For Example:\n" +
                " \n" +
                "\ta = 9; \n" +
                "\tb = a++ + 5; \n" +
                "\t/* Output:  a=10, b=14 */\n" +
                "\n" +
                "\ta = 9;\n" +
                " \tb = ++a + 5; \n" +
                "\t/* Output:   a=10 ,b=15 */\n");

        content4.setText("These operators establish a relationship between operands. \n" +
                "The relational operators are : \n" +
                "\n" +
                "1. less than (<)\n" +
                "2. grater thatn (>)\n" +
                "3. less than or equal to (<=) \n" +
                "4. greater than equal to (>=)\n" +
                "5. equivalent (==) \n" +
                "6. not equivalent (!=)\n" +
                "\n" +
                "You must notice that assignment operator is (=) and there is a relational operator, for equivalent (==). These two are different from each other, the assignment operator assigns the value to any variable, whereas equivalent operator is used to compare values, like in if-else conditions, Example\n" +
                "\n" +
                "int x = 10;  //assignment operator\n" +
                "x=5;         // again assignment operator \n" +
                "if(x == 5)   // used equivalent relational operator\n" +
                "{\n" +
                "    cout <<\"Successfully compared\";\n" +
                "}");

        content5.setText("The logical operators are:\n" +
                "\n" +
                "1. AND (&&) \n" +
                "2. OR (||). \n" +
                "\n" +
                "They are used to combine two different expressions together.\n" +
                "\n" +
                "If two statement are connected using AND operator, the validity of both statements will be considered, but if they are connected using OR operator, then either one of them must be valid. These operators are mostly used in loops (especially while loop) and in Decision making.");

        content6.setText("The ternary if-else ? : is an operator which has three operands.\n" +
                "\n" +
                "int a = 10;\n" +
                "a > 5 ? cout << \"true\" : cout << \"false\"\n" +
                "\n" +
                "Output:\n" +
                "true");

        content7.setText("This is used to separate variable names and to separate expressions. In case of expressions, the value of last expression is produced and used.\n" +
                "\n" +
                "Example :\n" +
                "\n" +
                "int a,b,c; // variables declaration using comma operator\n" +
                "a=b++, c++; // a = c++ will be done.");

        content8.setText("There are used to change individual bits into a number. They work with only integral data types like char, int and long and not with floating point values.\n" +
                "\n" +
                "1. Bitwise AND operators &\n" +
                "2. Bitwise OR operator |\n" +
                "3. Bitwise XOR operator ^\n" +
                "4. Bitwise NOT operator ~\n" +
                "\n" +
                "They can be used as shorthand notation too, & = , |= , ^= , ~= etc.");
    }
}
