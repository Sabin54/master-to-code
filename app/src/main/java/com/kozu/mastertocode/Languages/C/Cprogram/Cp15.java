package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp15 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int n, reversedN = 0, remainder, originalN;\n" +
                "    printf(\"Enter an integer: \");\n" +
                "    scanf(\"%d\", &n);\n" +
                "    originalN = n;\n" +
                "\n" +
                "    // reversed integer is stored in reversedN\n" +
                "    while (n != 0) {\n" +
                "        remainder = n % 10;\n" +
                "        reversedN = reversedN * 10 + remainder;\n" +
                "        n /= 10;\n" +
                "    }\n" +
                "\n" +
                "    // palindrome if orignalN and reversedN are equal\n" +
                "    if (originalN == reversedN)\n" +
                "        printf(\"%d is a palindrome.\", originalN);\n" +
                "    else\n" +
                "        printf(\"%d is not a palindrome.\", originalN);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter an integer: 1001\n" +
                "1001 is a palindrome.");

    }
}
