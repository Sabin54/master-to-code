package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment8 extends AppCompatActivity {
    TextView content1, content2, content3;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag8);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("Inheritance is one of the key features of Object Oriented Programming. \n" +
                "Inheritance provided mechanism that allowed a class to inherit property of another class. When a Class extends another class it inherits all non-private members including fields and methods.\n" +
                "\n" +
                "The class which inherits the properties of other is known as subclass (derived class, child class) and the class whose properties are inherited is known as superclass (base class, parent class). \n" +
                "\n" +
                "Inheritance defines is-a relationship between a Super class and its Sub class. extends and implements keywords are used to describe inheritance in Java.\n" +
                "\n" +
                "Why inheritance in java:\n" +
                "\n" +
                "-For Method Overriding (so runtime polymorphism can be achieved).\n" +
                "-For Code Reusability.\n" +
                "\n" +
                "Syntax of Java Inheritance\n" +
                "\tclass Subclass-name extends Superclass-name\n" +
                "\t{ \n" +
                "\t\t//methods and fields\n" +
                "\t}\n" +
                "extends is the keyword used to inherit the properties of a class.\n" +
                "\n" +
                "super keyword\n" +
                "In Java, super keyword is used to refer to immediate parent class of a class. In other words super keyword is used by a subclass whenever it need to refer to its immediate super class.");

        content2.setText("Syntax of Java Inheritance:\n" +
                "\n" +
                "Single inheritance is easy to understand. When a class extends another one class then we call it a single inheritance. \n" +
                "\n" +
                "Example:\n" +
                "Here A is a parent class of B and B is a a child class of A.\n" +
                "\t\n" +
                "\tclass A{}\n" +
                "\tclass B extends A{}\n" +
                "\n" +
                "2) Multiple Inheritance\n" +
                "\n" +
                "''Multiple Inheritance'' refers to the concept of one class extending (Or inherits) more than one base class. The inheritance we learnt earlier had the concept of one base class or parent. The problem with ''multiple inheritance'' is that the derived class will have to manage the dependency on two base classes.\n" +
                "Most of the new OO languages like Small Talk, Java, C# do not support Multiple inheritance. Multiple Inheritance is supported in C++.\n" +
                "\t\n" +
                "\tclass A{}\n" +
                "\tclass B{}\n" +
                "\tclass C extends A,B{}\n" +
                "\n" +
                "3) Multilevel Inheritance\n" +
                "\n" +
                "Multilevel inheritance refers to a mechanism in OO technology where one can inherit from a derived class, thereby making this derived class the base class for the new class. As you can see in below example C is subclass or child class of B and B is a child class of A.\n" +
                "\t\n" +
                "\tclass A{}\n" +
                "\tclass B extends A{}\n" +
                "\tclass C extends B{}\n" +
                "\n" +
                "4) Hierarchical Inheritance\n" +
                "\n" +
                "In such kind of inheritance one class is inherited by many sub classes. In below example class B,C and D inherits the same class A. A is parent class (or base class) of B,C & D.\n" +
                "\t\n" +
                "\tclass A{}\n" +
                "\tclass B extends A{}\n" +
                "\tclass C extends A{}\n" +
                "\tclass D extends A{}\n" +
                "\n" +
                "5) Hybrid Inheritance\n" +
                "\n" +
                "In simple terms you can say that Hybrid inheritance is a combination of Single and Multiple inheritance. \n" +
                "A hybrid inheritance can be achieved in the java in a same way as multiple inheritance can be!! Using interfaces. yes you heard it right. By using interfaces you can have multiple as well as hybrid inheritance in Java.");


        content3.setText("Abstraction is a process of hiding the implementation details and showing only functionality to the user.\n" +
                "Another way, it shows only important things to the user and hides the internal details for example sending sms, you just type the text and send the message. You don't know the internal processing about the message delivery.\n" +
                "Abstraction lets you focus on what the object does instead of how it does it.\n" +
                "\n" +
                "There are two ways to achieve abstraction in java\n" +
                "- Abstract class (0 to 100%)\n" +
                "- Interface (100%)\n" +
                "\n" +
                "Abstract class\n" +
                "\n" +
                "A class that is declared as abstract is known as abstract class. It needs to be extended and its method implemented. It cannot be instantiated.\n" +
                "\n" +
                "Example:\n" +
                "\tabstract class A{\n" +
                "\t} \n" +
                "\n" +
                "Abstract method\n" +
                "\n" +
                "A method that is declared as abstract and does not have implementation is known as abstract method.\n" +
                "Example:\n" +
                "\tabstract void printStatus();\n" +
                "\t//no body and abstract");
    }
}
