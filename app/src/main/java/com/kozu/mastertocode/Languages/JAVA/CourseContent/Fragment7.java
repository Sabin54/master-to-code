package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment7 extends AppCompatActivity {
    TextView content1, content2, content3;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag7);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("A package is a group of similar types of classes, interfaces and sub-packages.\n" +
                "Package in java can be categorized in two types, built-in package and user-defined package.\n" +
                "\nThere are many built-in packages such as: \n " +
                "\t\tjava, lang, awt, javax, swing, net, io, util, sql etc.\n" +
                "Advantage of Java Package\n" +
                "\n" +
                "1) Java package is used to categorize the classes and interfaces so that they can be easily maintained.\n" +
                "2) Java package provides access protection.\n" +
                "3) Java package removes naming collision.\n" +
                "\n" +
                "Example:\n" +
                "The package keyword is used to create a package in java.\n" +
                "\n" +
                "package mypack; \n" +
                "\n" +
                "public class Simple{ \n" +
                "\tpublic static void main(String args[]){ \n" +
                "\t\tSystem.out.println(\"Welcome to package\"); \n" +
                "\t} \n" +
                "}");

        content2.setText("An Application Programming Interface (API), in the context of Java, is a collection of prewritten packages, classes, and interfaces with their respective methods, fields and constructors. \n" +
                "Similar to a user interface, which facilitates interaction between humans and computers, an API serves as a software program interface facilitating interaction.\n" +
                "In Java, most basic programming tasks are performed by the API's classes and packages, which are helpful in minimizing the code.\n" +
                "\n" +
                "\n" +
                "Java system packages and their classes:\n" +
                "\n" +
                "java.lang\n" +
                "\tlanguage support classes.\n" +
                "\tThey include classes for primitive types, strings,math functions,threads and exceptions.\n" +
                "\n" +
                "java.util\n" +
                "\tLanguage utility classes such as vectors, hash tables, random numbers, date etc.\n" +
                "\n" +
                "java.io\n" +
                "\tInput/Output support classes. They provides facilities for the input and output of data.\n" +
                "\n" +
                "java.awt\n" +
                "\tSet of classes for implementing graphical user interface.They include classes for windows, buttons, lists, menus and so on.\n" +
                "\n" +
                "java.net\n" +
                "\tclasses for networking. They include classes for communicating with local computers as well as with internet servers.\n" +
                "\n" +
                "java.applet\n" +
                "\tClasses for creating and implementing applets.");


        content3.setText("The package named java contains the package awt,which in turn contains various classes required for implementing graphical user interface.\n" +
                "There are two ways of accessing the classes stored in a package. \n" +
                "The first approach is to use the fully qualified class name of the class that we want to use.\n" +
                "\n" +
                "for example, \n" +
                "if we want to refer to the class Color in the awt package, then we may do so as follows:\n" +
                "\n" +
                "\tjava.awt.Colour\n" +
                "\n" +
                "Notice that awt is a package within the package java and the hierarchy is represented by separating the levels with dots.\n" +
                "\n" +
                "This approach is perhaps the best and easiest one if we need to access the class only once or when we need not have to access any other classes of the package. \n" +
                "\n" +
                "But, in many situations, we might want to use a class in a number of places in the program or we may like to use many of the classes contained in a package.\n" +
                "We may achieve this easily as follows: \n" +
                "\n" +
                "\timport packagename.classname;\n" +
                "\tor\n" +
                "\timport packagename.* \n" +
                "\n" +
                "These are known as import statements and must appear at the top of the file, before any class declarations, import is a keyword.");

    }
}
