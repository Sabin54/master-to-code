package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP3 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "    int no, i;\n" +
                "    cout<<\"Enter a number to be checked (greater than 2) : \";\n" +
                "    cin>>no;\n" +
                "    for(i=2;i<no;i++)\n" +
                "    {\n" +
                "        if(no%i==0)\n" +
                "        {\n" +
                "            break;\n" +
                "        }\n" +
                "    }\n" +
                "    if(no==i)\n" +
                "    {\n" +
                "        cout<<\"\\n\"<<no<<\" is a prime number\";\n" +
                "    }\n" +
                "    else\n" +
                "    {\n" +
                "        cout<<\"\\n\"<<no<<\" is not a prime number\";\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter a number to be checked (greater than 2) : 5\n" +
                "5 is a prime number");

    }
}
