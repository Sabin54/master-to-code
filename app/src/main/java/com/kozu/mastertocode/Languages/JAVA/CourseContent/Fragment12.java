package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment12 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5, content6;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag12);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("The java.io package contains Stream classes which provide capabilities for processing all types of data.\n" +
                "Java performs I/O through Streams. A Stream is linked to a physical layer by java I/O system to make input and output operation in java.\n" +
                "In general, a stream means continuous flow of data. Streams are clean way to deal with input/output without having every part of your code understand the physical. All these streams represent an input source and an output destination. The stream in the java.io package supports many data such as primitives, Object, localized characters, etc.\n" +
                "\n" +
                "These classes may be categorized into two groups based on their data type handling capabilities:-\n" +
                "\t1. Byte Stream\n" +
                "\t2. Charater Stream\n" +
                "\n" +
                "The InputStream & OutputStream class providing support for handling I/O operations on bytes are type of byte stream.\n" +
                "\n" +
                "The Reader & Writer class providing support for handling I/O operations on characters are type of char stream. Character stream uses Unicode and therefore can be internationalized.");

        content2.setText("ByteStream classes are to provide an surrounding to handle byte-oriented data or I/O.\n" +
                "ByteStream classes are having 2 abstract class InputStream & OutputStream.\n" +
                "\n" +
                "These two abstract classes have several concrete classes that handle various devices such as disk files, network connection etc.\n" +
                "\n" +
                "InputStream\n" +
                "\t- It is an abstract class in which defines an interface known as Closable interface.\n" +
                "\t- Methods in this class will throw an IOException.\n" +
                "\n" +
                "OutputStream\n" +
                "\t- It is an abstract class in which defines an interface known as Closable & Flushable interface.\n" +
                "\t- Methods in this class returns void and throws IOException.\n" +
                "\n" +
                "methods by InputStream abstract class\n" +
                "int read()\n" +
                "it returns integer of next available input of bye\n" +
                "\n" +
                "int read(byte buffer[ ])\n" +
                "it read up to the length of byte in buffer\n" +
                "\n" +
                "int read(byte buffer[ ], int offset, int numBytes)\n" +
                "it read up to the length of byte in buffer starting form offset\n" +
                "\n" +
                "int available()\n" +
                "It gets the no.of bytes of input available.\n" +
                "\n" +
                "void reset()\n" +
                "it reads the input pointer.\n" +
                "\n" +
                "long skip(long numBytes)\n" +
                "it returns the bytes ignored.\n" +
                "\n" +
                "void close()\n" +
                "close the source of input.\n" +
                "\n" +
                "methods by OutputStream abstract class\n" +
                "void write(int b)\n" +
                "it writes a single byte in output.\n" +
                "\n" +
                "void write(byte buffer[ ])\n" +
                "it write a full array of bytes to an output stream\n" +
                "\n" +
                "void write(byte buffer[ ], int offset, int numBytes)\n" +
                "it writes a range of numBytes from array buffer starting at buffer offset.\n" +
                "\n" +
                "void close()\n" +
                "close the source of output\n" +
                "\n" +
                "void flush()\n" +
                "it marks a point to input stream that will stay valid until numBytes are read.");

        content3.setText("Java Character streams are used to perform input and output for 16-bit unicode. Though there are many classes related to character streams but the most frequently used classes are , Reader and Writer.\n" +
                "Though internally FileReader uses FileInputStream and FileWriter uses FileOutputStream but here major difference is that FileReader reads two bytes at a time and FileWriter writes two bytes at a time.\n" +
                "\n" +
                "Reader\n" +
                "\t- It is an abstract class used to input character.\n" +
                "\t- It implements 2 interfaces Closable & Readable.\n" +
                "\t- Methods in this class throws IO Exception(Except markSupported()).\n" +
                "\n" +
                "Writer\n" +
                "\t- Writer is an abstract class used to output character.\n" +
                "\t- It implements 3 interfaces Closable, Flushable & Appendable.\n" +
                "\n" +
                "methods of Reader abstract class:\n" +
                "\n" +
                "\tabstract void close()\n" +
                "\tclose the source of input.\n" +
                "\n" +
                "\tvoid mark(int numChars)\n" +
                "\tit marks a point to input stream that will stay valid until numChars are read.\n" +
                "\n" +
                "\tboolean markSupported()\n" +
                "\tit returns true if mark() support by stream.\n" +
                "\n" +
                "\tint read()\n" +
                "\tit returns integer of next available character from input\n" +
                "\n" +
                "\tint read(char buffer[])\n" +
                "\tit read up to the length of byte in buffer\n" +
                "\n" +
                "\tabstract read(byte buffer[], int offset, int numBytes)\n" +
                "\tit read up to the length of character in buffer starting form offset\n" +
                "\n" +
                "\tvoid reset()\n" +
                "\tit reads the input pointer.\n" +
                "\n" +
                "\tlong skip(long numBytes)\n" +
                "\tit returns the character ignored.\n" +
                "\n" +
                "\tboolean ready()\n" +
                "\tif input request will not wait returns true, otherwise false\n" +
                "\n" +
                "methods of Writer abstract class:\n" +
                "\n" +
                "\twrite append(char ch) \n" +
                "\tIt append the 'ch' character at last of output stream\n" +
                "\n" +
                "\twrite append(charSequence chars)\n" +
                "\tappend the chars at end of output stream.\n" +
                "\n" +
                "\twrite append(charSequence chars, int begin, int end)\n" +
                "\tappend the characters specified in the range.\n" +
                "\n" +
                "\tabstract void close()\n" +
                "\tclose the output stream\n" +
                "\n" +
                "\tabstract void flush()\n" +
                "\tit flush the output buffer.\n" +
                "\n" +
                "\tvoid write(char buffer[])\n" +
                "\twrites array of character to output stream.\n" +
                "\n" +
                "\tabstract void write(char buffer[], int offset, int numChars)\n" +
                "\tit writes a range of characters from buffer starting form offset\n" +
                "\n" +
                "\tvoid write(String str)\n" +
                "\tit writes the str to output stream\n" +
                "\n" +
                "\tvoid write(String str, int offset, int numChars)\n" +
                "\tit writes a range of character from the string str starting at offset.");

        content4.setText("Java provides following three standard streams\n" +
                "\n" +
                "Standard Input:\n" +
                "This is used to feed the data to user's program and usually a keyboard is used as standard input stream and represented as System.in.\n" +
                "\n" +
                "Standard Output:\n" +
                "This is used to output the data produced by the user's program and usually a computer screen is used to standard output stream and represented as System.out.\n" +
                "\n" +
                "Standard Error:\n" +
                "This is used to output the error data produced by the user's program and usually a computer screen is used to standard error stream and represented as System.err.");

        content5.setText("- File class is used to support with Files and File systems.\n" +
                "- File class describes properties of itself.\n" +
                "- File class has its objects which is used to handle or get data stored in disk.\n" +
                "- File class has some constructors which is used to describe the file path.\n" +
                "\n" +
                "Constructors : \n" +
                "\tFile(String directoryPath)\n" +
                "\tFile(String directoryPath, String filename)\n" +
                "\tFile(File dirObj, String filename)\n" +
                "\tFile(URl uriObj)\n" +
                "\n" +
                "FileInputStream:\n" +
                "This stream is used for reading data from the files. Objects can be created using the keyword new and there are several types of constructors available.\n" +
                "\n" +
                "Following constructor takes a file name as a string to create an input stream object to read the file.:\n" +
                "InputStream f = new FileInputStream(\"C:/java/hello\");\n" +
                "\n" +
                "Following constructor takes a file object to create an input stream object to read the file. First we create a file object using File() method as follows:\n" +
                "File f = new File(\"C:/java/hello\");\n" +
                "InputStream f = new FileInputStream(f);\n" +
                "\n" +
                "FileOutputStream:\n" +
                "FileOutputStream is used to create a file and write data into it. The stream would create a file, if it doesn't already exist, before opening it for output.\n" +
                "\n" +
                "Here are two constructors which can be used to create a FileOutputStream object.\n" +
                "Following constructor takes a file name as a string to create an input stream object to write the file:\n" +
                "OutputStream f = new FileOutputStream(\"C:/java/hello\") \n" +
                "\n" +
                "Following constructor takes a file object to create an output stream object to write the file. First, we create a file object using File() method as follows:\n" +
                "File f = new File(\"C:/java/hello\");\n" +
                "OutputStream f = new FileOutputStream(f);\n" +
                "\n" +
                "I/O Exceptions\n" +
                "EOFException\n" +
                "It indicates the signal are reached to end of file during input.\n" +
                "\n" +
                "FileNotFoundException\n" +
                "It indicates that the file not found at specified path.\n" +
                "\n" +
                "InterruptedIOException\n" +
                "It indicates that an I/O Exception has occurred\n" +
                "\n" +
                "I/O Exception\n" +
                "It indicates I/O Exception some sort has occurred.\n");


        content6.setText("A directory is a File which can contains a list of other files and directories. \n" +
                "You use File object to create directories, to list down files available in a directory. \n" +
                "For complete detail check a list of all the methods which you can call on File object and what are related to directories.\n" +
                "\n" +
                "Creating Directories:\n" +
                "There are two useful File utility methods, which can be used to create directories:\n" +
                "\n" +
                "\tThe mkdir( ) method creates a directory, returning true on success and false on failure. Failure indicates that the path specified in the File object already exists, or that the directory cannot be created because the entire path does not exist yet.\n" +
                "\n" +
                "\tThe mkdirs() method creates both a directory and all the parents of the directory.\n" +
                "\n" +
                "Listing Directories:\n" +
                "You can use list( ) method provided by File object to list down all the files and directories available in a directory\n" +
                "\n" +
                "Deleting Directories:\n" +
                "To delete a directory, you can simply use the File.delete(), but the directory must be empty in order to delete it.\n" +
                "Often times, you may require to perform recursive delete in a directory, which means all it's sub-directories and files should be delete as well.");
    }

}
