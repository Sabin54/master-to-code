package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP8 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    int rows;\n" +
                "\n" +
                "    cout << \"Enter number of rows: \";\n" +
                "    cin >> rows;\n" +
                "\n" +
                "    for(int i = 1; i <= rows; ++i)\n" +
                "    {\n" +
                "        for(int j = 1; j <= i; ++j)\n" +
                "        {\n" +
                "            cout << \"*\"<< \" \";\n" +
                "        }\n" +
                "        cout << \"\\n\";\n" +
                "    }\n" +
                "getch();\n" +
                "return 0 ;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter number of rows: 5\n" +
                "*\n" +
                "* *\n" +
                "* * *\n" +
                "* * * *\n" +
                "* * * * *");

    }
}
