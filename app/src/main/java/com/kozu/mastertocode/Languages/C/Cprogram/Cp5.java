package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp5 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<stdio.h>\n" +
                "int main() {\n" +
                "    int intType;\n" +
                "    float floatType;\n" +
                "    double doubleType;\n" +
                "    char charType;\n" +
                "\n" +
                "    // sizeof evaluates the size of a variable\n" +
                "    printf(\"Size of int: %ld bytes\\n\", sizeof(intType));\n" +
                "    printf(\"Size of float: %ld bytes\\n\", sizeof(floatType));\n" +
                "    printf(\"Size of double: %ld bytes\\n\", sizeof(doubleType));\n" +
                "    printf(\"Size of char: %ld byte\\n\", sizeof(charType));\n" +
                "    \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Size of int: 4 bytes\n" +
                "Size of float: 4 bytes\n" +
                "Size of double: 8 bytes\n" +
                "Size of char: 1 byte");

    }
}
