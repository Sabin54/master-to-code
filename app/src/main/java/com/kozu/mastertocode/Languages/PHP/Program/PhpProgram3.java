package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram3 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<?php\n" +
                "$cars = array(\"Volvo\", \"BMW\", \"Toyota\"); \n" +
                "echo \"I like \" . $cars[0] . \", \" . $cars[1] . \" and \" . $cars[2] . \".\";\n" +
                "?>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "I like Volvo, BMW and Toyota.");
    }
}
