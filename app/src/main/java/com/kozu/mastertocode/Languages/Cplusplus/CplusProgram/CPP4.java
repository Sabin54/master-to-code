package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP4 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "    int no, ld, nn=0, cn;\n" +
                "    cout<<\"Enter a number to be checked : \"\n" +
                "    cin>>no;\n" +
                "    cn==no;\n" +
                "    while(no > 0)\n" +
                "    {\n" +
                "        ld=no%10;\n" +
                "        nn=nn+ld*ld*ld;\n" +
                "        no=n/10;\n" +
                "    }\n" +
                "    if(nn==cn)\n" +
                "    {\n" +
                "        cout<<\"\\n\"<<cn<<\" is an armstrong number\";\n" +
                "    }\n" +
                "    else\n" +
                "    {\n" +
                "        cout<<\"\\n\"<<cn<<\" is not an armstrong number\";\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter a number to be checked : 153\n" +
                "153 is an armstrong number");

    }
}
