package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram1 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<h1>My first PHP page</h1>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\techo \"Hello World!\";\n" +
                "\t\t?> \n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output: \n" +
                "\n" +
                "My first PHP page\n" +
                "Hello World!\n");

    }
}
