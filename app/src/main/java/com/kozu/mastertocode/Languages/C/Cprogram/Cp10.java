package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp10 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "   int year;\n" +
                "   printf(\"Enter a year: \");\n" +
                "   scanf(\"%d\", &year);\n" +
                "\n" +
                "   // leap year if perfectly visible by 400\n" +
                "   if (year % 400 == 0) {\n" +
                "      printf(\"%d is a leap year.\", year);\n" +
                "   }\n" +
                "   // not a leap year if visible by 100\n" +
                "   // but not divisible by 400\n" +
                "   else if (year % 100 == 0) {\n" +
                "      printf(\"%d is not a leap year.\", year);\n" +
                "   }\n" +
                "   // leap year if not divisible by 100\n" +
                "   // but divisible by 4\n" +
                "   else if (year % 4 == 0) {\n" +
                "      printf(\"%d is a leap year.\", year);\n" +
                "   }\n" +
                "   // all other years are not leap year\n" +
                "   else {\n" +
                "      printf(\"%d is not a leap year.\", year);\n" +
                "   }\n" +
                "\n" +
                "   return 0;\n" +
                "}\n" +
                "\n" +
                "Output 1:\n" +
                "\n" +
                "Enter a year: 1900\n" +
                "1900 is not a leap year.\n" +
                "\n" +
                "Output 2:\n" +
                "\n" +
                "Enter a year: 2012\n" +
                "2012 is a leap year.");

    }
}
