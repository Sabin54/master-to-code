package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP16 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);
        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream>\n" +
                "#include <iostream.h>\n" +
                "#include<stdio.h>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "class Shape \n" +
                "{\n" +
                "    public:\n" +
                "    void setWidth(int w) \n" +
                "    {\n" +
                "       width = w;\n" +
                "    }\n" +
                "\n" +
                "    void setHeight(int h) \n" +
                "    {\n" +
                "       height = h;\n" +
                "    }\n" +
                "\n" +
                "    protected:\n" +
                "    int width;\n" +
                "    int height;\n" +
                "};\n" +
                "\n" +
                "class PaintCost  \n" +
                "{\n" +
                "    public:\n" +
                "    int getCost(int area) \n" +
                "    {\n" +
                "       return area * 70;\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "class Rectangle: public Shape, public PaintCost \n" +
                "{\n" +
                "    public:\n" +
                "    int getArea() \n" +
                "    {\n" +
                "       return (width * height);\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "int main(void) \n" +
                "{\n" +
                "    Rectangle Rect;\n" +
                "    int area;\n" +
                "    Rect.setWidth(5);\n" +
                "    Rect.setHeight(7);\n" +
                "    \n" +
                "    area = Rect.getArea();\n" +
                "    cout << \"Total area: \" << Rect.getArea() << endl;\n" +
                "    \n" +
                "    cout << \"Total paint cost: $\" << Rect.getCost(area) << endl;\n" +
                "    getch();\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Total area: 35\n" +
                "Total paint cost: $2450\n" +
                "\n");

    }
}

