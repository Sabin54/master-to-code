package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp20 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "   int i, space, rows, k = 0;\n" +
                "   printf(\"Enter the number of rows: \");\n" +
                "   scanf(\"%d\", &rows);\n" +
                "   for (i = 1; i <= rows; ++i, k = 0) {\n" +
                "      for (space = 1; space <= rows - i; ++space) {\n" +
                "         printf(\"  \");\n" +
                "      }\n" +
                "      while (k != 2 * i - 1) {\n" +
                "         printf(\"* \");\n" +
                "         ++k;\n" +
                "      }\n" +
                "      printf(\"\\n\");\n" +
                "   }\n" +
                "   return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "        *\n" +
                "      * * *\n" +
                "    * * * * *\n" +
                "  * * * * * * *\n" +
                "* * * * * * * * *");

    }
}

