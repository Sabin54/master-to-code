package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program11 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("package beginnersbook.com;\n" +
                "import java.util.Scanner;\n" +
                "class PalindromeCheck\n" +
                "{\n" +
                "    //My Method to check\n" +
                "    public static boolean isPal(String s)\n" +
                "    {   // if length is 0 or 1 then String is palindrome\n" +
                "        if(s.length() == 0 || s.length() == 1)\n" +
                "            return true; \n" +
                "        if(s.charAt(0) == s.charAt(s.length()-1))\n" +
                "        \n" +
                "            /* check for first and last char of String:\n" +
                "             * if they are same then do the same thing for a substring\n" +
                "             * with first and last char removed. and carry on this\n" +
                "             * until you string completes or condition fails\n" +
                "             * Function calling itself: Recursion\n" +
                "             */\n" +
                "\n" +
                "        return isPal(s.substring(1, s.length()-1));\n" +
                "\n" +
                "        /* If program control reaches to this statement it means\n" +
                "         * the String is not palindrome hence return false.\n" +
                "         */\n" +
                "\n" +
                "        return false;\n" +
                "    }\n" +
                "\n" +
                "    public static void main(String[]args)\n" +
                "    {\n" +
                "      //For capturing user input\n" +
                "        Scanner scanner = new Scanner(System.in);\n" +
                "        System.out.println(\"Enter the String for check:\");\n" +
                "        String string = scanner.nextLine();\n" +
                "\n" +
                "            /* If function returns true then the string is\n" +
                "             * palindrome else not\n" +
                "             */\n" +
                "\n" +
                "        if(isPal(string))\n" +
                "            System.out.println(string + \" is a palindrome\");\n" +
                "        else\n" +
                "            System.out.println(string + \" is not a palindrome\");\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Enter the String for check:\n" +
                "qqaabb\n" +
                "qqaabb is not a palindrome\n" +
                "Output 2:\n" +
                "\n" +
                "Enter the String for check:\n" +
                "cocoococ\n" +
                "cocoococ is a palindrome");
    }
}
