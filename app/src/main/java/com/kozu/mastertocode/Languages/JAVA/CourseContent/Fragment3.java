package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment3 extends AppCompatActivity {

    TextView content1, content2, content3, content4, content5, content6, content7;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag3);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("An if statement contains a Boolean expression and block of statements enclosed within braces.\n" +
                "\n\tif(conditional expression)\n" +
                "\t{\n" +
                "\t//Statement\n" +
                "\t}\n" +
                "\nIf the Boolean expression is true then IF block is executed otherwise program directly goes to Else statement without executing IF block.\n");

        content2.setText("If statement block with else statement is known as as if...else statement. \n" +
                "Else portion is non-compulsory.\n" +
                "Example IF, ELSE IF and Else Statement: \n" +
                "\tif ( condition_one ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t } \n" +
                "\telse if ( condition_two ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t }\n" +
                "\t else \n" +
                "\t{\n" +
                "\t //statements\n" +
                "\t }\n" +
                "\n" +
                "If first condition is true, then compiler will execute the if block, And if First condition is false it will check the second condition available in else if block of statements, if it is true else if block will execute and  if false then else block of statements will be executed.\n" +
                "\n");

        content3.setText("when a series of decisions are involved, we may have to use more than one if...else statement in nested form as follows:\n" +
                "if (test condition1)\n" +
                " { \n" +
                "\tIf (test condition2) \n" +
                "\t{ \n" +
                "\t//statement1; \n" +
                "\t}\n" +
                "\t Else\n" +
                "\t {\n" +
                "\t //statement2; \n" +
                "\t} \n" +
                "} \n" +
                "else \n" +
                "{ \n" +
                "\t//statement3; \n" +
                "}\n" +
                "\n");

        content4.setText("A switch statement is used instead of nested if...else statements. \n" +
                "It is multiple branch decision statement. A switch statement tests a variable with list of values for equivalence. Each value is called a case. The case value must be a constant i.\n" +
                "\n" +
                "SYNTAX \n" +
                "switch(expression) {\n" +
                "  \tcase x:\n" +
                "   \t\t// code block\n" +
                "    \t\tbreak;\n" +
                "  \tcase y:\n" +
                "   \t\t // code block\n" +
                "    \t\tbreak;\n" +
                "  \tdefault:\n" +
                "    \t\t// code block\n" +
                "}\n" +
                "\n");

        content5.setText("while loop is used to execute statement(s) until a condition holds true. \n" +
                "SYNTAX \n" +
                "while (condition(s))\n" +
                " \t{ \n" +
                "\t\t// statements \n" +
                "\t}\n" +
                "\n" +
                "If the condition holds true then the body of loop is executed, after execution of loop body condition is tested again and if the condition is true then body of loop is executed again and the process repeats until condition becomes false. \n" +
                "\n" +
                "1. Condition is always evaluated to true or false and if it is a constant, \n" +
                "For example \n" +
                "while (c) \n" +
                "\t{ \n" +
                "\t\t//Statements\n" +
                "\t}\n" +
                " where c is a constant then any non-zero value of c is considered true and zero is considered false.\n" +
                "\n" +
                "2. You can test multiple conditions such as\n" +
                "while ( a > b && c != 0)\n" +
                " \t{\n" +
                " \t\t// statements\n" +
                " \t}\n" +
                "Loop body is executed till value of a is greater than value of b and c is not equal to zero.\n" +
                "\n" +
                "3. Body of loop can contain more than one statement. For multiple statements you need to place them in a block using {} and if body of loop contain only single statement you can optionally use {}.\n" +
                "\n");

        content6.setText("The while loop makes a test condition before the loop is executed. Therefore, the body of the loop may not be executed at all if the condition is not satisfied at the very first attempt. \n" +
                "On some occasions it might be necessary to execute the body of the loop before the test is performed. Such situations can be handled with the help of the do statement.\n" +
                "SYNTAX:\n" +
                " \n" +
                "\tdo \n" +
                "\t\t{\n" +
                " \t\t\t//statement \n" +
                "\t\t} \n" +
                "\twhile (condition);\n" +
                "\n" +
                "On reaching the do statement, program evaluate the body of loop first. At the end of the loop, the condition in the while statement is evaluated. If the condition is true, the program continues to evaluate the body of loop again and again till condition becomes false.\n" +
                "\n" +
                "Example:\n" +
                "\tint i =0; \n" +
                "\tdo { \n" +
                "\t\tSystem.out.println(\"i = \" + i); \n" +
                "\t\ti++;\n" +
                "\t }\n" +
                "\tWhile (i < 5);\n" +
                "\t\tOutput:\n" +
                "\t\ti = 0\n" +
                "\t\ti = 1 \n" +
                "\t\ti = 2 \n" +
                "\t\ti = 3\n" +
                "\t\ti = 4\n" +
                "\n" +
                "\n");

        content7.setText("For loop are used to repeat execution of statement(s) until a certain condition holds true. \n" +
                "Syntax:\n" +
                "\tfor (initialization; termination; increment) \n" +
                "\t{ \n" +
                "\t\tstatement; \n" +
                "\t}\n" +
                "\n" +
                "You can initialize multiple variables, test many conditions and perform increments or decrements on many variables according to requirement. All three components of for loop are optional. \n" +
                "\n" +
                "Example: \n" +
                "\n" +
                "\tfor (i = 0; i < 5; i++) \n" +
                "\t{\n" +
                "\t\tStatement;\n" +
                "\t}\n" +
                "\n");
    }
}
