package com.kozu.mastertocode.Languages.JAVA.JAVAQuiz;

public class Questions {

    private String mQuestions[] = {
            " What is the range of short data type in Java?",
            "An expression involving byte, int, and literal numbers is promoted to which of these?",
            "class increment {\n" +
                    "        public static void main(String args[]) \n" +
                    "        {        \n" +
                    "             int g = 3;\n" +
                    "             System.out.print(++g * 8);\n" +
                    "        } \n" +
                    "    }",
            "   class area {\n" +
                    "        public static void main(String args[]) \n" +
                    "        {    \n" +
                    "             double r, pi, a;\n" +
                    "             r = 9.8;\n" +
                    "             pi = 3.14;\n" +
                    "             a = pi * r * r;\n" +
                    "             System.out.println(a);\n" +
                    "        } \n" +
                    "    }",
            " Which of these is an incorrect array declaration?",
            " Which of these is necessary to specify at time of array initialization?",
            "class array_output \n" +
                    "    {\n" +
                    "        public static void main(String args[]) \n" +
                    "        {\n" +
                    "            char array_variable [] = new char[10];\n" +
                    "\t    for (int i = 0; i < 10; ++i) \n" +
                    "            {\n" +
                    "                array_variable[i] = 'i';\n" +
                    "                System.out.print(array_variable[i] + \"\");\n" +
                    "            }\n" +
                    "        } \n" +
                    "    }",
            "Decrement operator, −−, decreases the value of variable by what number?",
            "When does method overloading is determined?",
            "Which concept of Java is a way of converting real world objects in terms of class?",
            "Which component is used to compile, debug and execute java program?",
            " Which component is responsible to run java program?",
            "Which statement is true about java?",
            "What is the extension of java code files?",
            "Which of the following is a valid declaration of an object of class Box?",
            "  class Output \n" +
                    "    {\n" +
                    "        public static void main(String args[])\n" +
                    "        {\n" +
                    "            int x = 9;\n" +
                    "            if (x == 9) \n" +
                    "            { \n" +
                    "                int x = 8;\n" +
                    "                System.out.println(x);\n" +
                    "            }\n" +
                    "        } \n" +
                    "    }",
            "Which method can be defined only once in a program?",
            " class equality \n" +
                    "    {\n" +
                    "        int x;\n" +
                    "        int y;\n" +
                    "        boolean isequal()\n" +
                    "        {\n" +
                    "            return(x == y);  \n" +
                    "        } \n" +
                    "    }    \n" +
                    "    class Output \n" +
                    "    {\n" +
                    "        public static void main(String args[])\n" +
                    "        {\n" +
                    "            equality obj = new equality();\n" +
                    "            obj.x = 5;\n" +
                    "            obj.y = 5;\n" +
                    "            System.out.println(obj.isequal());\n" +
                    "        } \n" +
                    "    }",
            "What would be behaviour if the constructor has a return type?",
            "Which of these class is superclass of every class in Java?",
            "    abstract class A \n" +
                    "    {\n" +
                    "        int i;\n" +
                    "        abstract void display();\n" +
                    "    }    \n" +
                    "    class B extends A \n" +
                    "    {\n" +
                    "        int j;\n" +
                    "        void display() \n" +
                    "        {\n" +
                    "            System.out.println(j);\n" +
                    "        }\n" +
                    "    }    \n" +
                    "    class Abstract_demo \n" +
                    "    {\n" +
                    "        public static void main(String args[])\n" +
                    "        {\n" +
                    "            B obj = new B();\n" +
                    "            obj.j=2;\n" +
                    "            obj.display();    \n" +
                    "        }\n" +
                    "    }",
            "Which of these is not abstract?",
            "If a class inheriting an abstract class does not define all of its function then it will be known as?",
            "Which of these is correct way of calling a constructor having no parameters, of superclass A by subclass B?",
            "class Output \n" +
                    " {\n" +
                    "  public static void main(String[] args) \n" +
                    "   {\n" +
                    "   int []x[] = {{1,2}, {3,4,5}, {6,7,8,9}};\n" +
                    "   int [][]y = x;\n" +
                    "   System.out.println(y[2][1]);\n" +
                    "   }\n" +
                    " }",
            "class A \n" +
                    "    {\n" +
                    "        int i;\n" +
                    "        public void display() \n" +
                    "        {\n" +
                    "            System.out.println(i);\n" +
                    "        }    \n" +
                    "    }    \n" +
                    "    class B extends A \n" +
                    "   {\n" +
                    "        int j;\n" +
                    "        public void display() \n" +
                    "        {\n" +
                    "            System.out.println(j);\n" +
                    "        } \n" +
                    "    }    \n" +
                    "    class Dynamic_dispatch \n" +
                    "   {\n" +
                    "        public static void main(String args[])\n" +
                    "        {\n" +
                    "            B obj2 = new B();\n" +
                    "            obj2.i = 1;\n" +
                    "            obj2.j = 2;\n" +
                    "            A r;\n" +
                    "            r = obj2;\n" +
                    "            r.display();     \n" +
                    "        }\n" +
                    "   }",
            "When does Exceptions in Java arises in code sequence?",
            "Which of these keywords is not a part of exception handling?",
            "Which of these keywords must be used to monitor for exceptions?",
            "class exception_handling_Output \n" +
                    "    {\n" +
                    "        public static void main(String args[]) \n" +
                    "        {\n" +
                    "            try \n" +
                    "            {\n" +
                    "                System.out.print(\"Hello\" + \" \" + 1 / 0);\n" +
                    "            }\n" +
                    "            catch(ArithmeticException e) \n" +
                    "            {\n" +
                    "        \tSystem.out.print(\"World\");        \t\n" +
                    "            }\n" +
                    "        }\n" +
                    "    }",
            "Which of these keywords are used for generating an exception manually?",
            "class Output \n" +
                    "    {\n" +
                    "        public static void main(String args[]) \n" +
                    "        {\n" +
                    "           try \n" +
                    "           {\n" +
                    "               int a = 0;\n" +
                    "               int b = 5;\n" +
                    "               int c = a / b;\n" +
                    "               System.out.print(\"Hello\");\n" +
                    "           }\n" +
                    "           catch(Exception e) \n" +
                    "           {\n" +
                    "               System.out.print(\"World\");\n" +
                    "           } \n" +
                    "        }\n" +
                    "    }",
            "Which of these is used to perform all input & output operations in Java?",
            "Which of these method of Thread class is used to Suspend a thread for a period of time?"
    };


    private String mChoices[][] = {
            {"-128 to 127", "-32768 to 32767", "-2147483648 to 2147483647", "None of the mentioned"},
            {"int", "byte", "long", "float"},
            {"25", "24", "32", "33"},
            {"301.5656", "301", "301.56", "301.56560000"},
            {"int arr[] = new int[5]", "int [] arr = new int[5]", "int arr[] = new int[5]", "int arr[] = int [5] new"},
            {"Row", "Column", "Both Row and Column", " None of the mentioned",},
            {"1 2 3 4 5 6 7 8 9 10", "0 1 2 3 4 5 6 7 8 9 10", "i j k l m n o p q r", "i i i i i i i i i i"},
            {"1", "2", "3", "4"},
            {"At run time", " At compile time", "At coding time", "At execution time"},
            {"Polymorphism", "Encapsulation", "Abstraction", "Inheritance"},
            {"JVM","JDK","JIT","JRE"},
            {"JVM","JDK","JIT","JRE"},
            {"Platform independent","Platform dependent","Code dependent ","Sequence dependent"},
            {".class",".java",".txt",".js"},
            {"Box obj = new Box();","Box obj = new Box;","obj = new Box();","new Box obj;"},
            {"8","9"," Compilation error"," Runtime error"},
            {" main method","finalize method"," static method","private method"},
            {"false","true","0","1"},
            {"Compilation error","Runtime error","Compilation and runs successfully","Only String return type is allowed"},
            {" String class","Object class","Abstract class","ArrayList class"},
            {"0","2","Runtime Error","Compilation Error"},
            {"Thread"," AbstractList","List"," None of the Mentioned"},
            {"Abstract","A simple class","Static class","None of the mentioned"},
            {"super(void);","superclass.();"," super.A();","super();"},
            {"2","3","7","Compilation Error"},
            {"1","2","3","4"},
            {"Run Time"," Compilation Time","Can Occur Any Time","None of the mentioned"},
            {"try","finally","catch","throw"},
            {"try","finally","catch","throw"},
            {"hello","world","HelloWorld","Hello World"},
            {"try","check","catch","throw"},
            {"hello","world","HelloWorld"," Compilation Error"},
            {"streams","Variables","classes","Methods"},
            {"sleep()"," terminate()","suspend()","stop()"}
    };

    private String mCorrectAnswers[] = {"-128 to 127", "int", "32", "301.5656", "int arr[] = int [5] new",
            "Row", "i i i i i i i i i i", "1", " At compile time", "Abstraction"
    ,"JDK","JRE","Platform independent",".java","Box obj = new Box();"," Compilation error"," main method",
    "true","Compilation error","Object class","2","Thread","Abstract","super();","7","2","Run Time","throw","try",
            "world","throw","hello","streams","sleep()"};


    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getAns1(int a) {
        String Ans1 = mChoices[a][0];
        return Ans1;
    }

    public String getAns2(int a) {
        String Ans2 = mChoices[a][1];
        return Ans2;
    }

    public String getAns3(int a) {
        String Ans3 = mChoices[a][2];
        return Ans3;
    }

    public String getAns4(int a) {
        String Ans4 = mChoices[a][3];
        return Ans4;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }

}
