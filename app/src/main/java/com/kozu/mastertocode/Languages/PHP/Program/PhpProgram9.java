package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram9 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<?php\n" +
                "$cookie_name = \"user\";\n" +
                "$cookie_value = \"Kozu\";\n" +
                "setcookie($cookie_name, $cookie_value, time() + (86400 * 30), \"/\"); // 86400 = 1 day\n" +
                "?>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t<?php\n" +
                "\tif(!isset($_COOKIE[$cookie_name])) {\n" +
                "\t     echo \"Cookie named '\" . $cookie_name . \"' is not set!\";\n" +
                "\t} else {\n" +
                "\t     echo \"Cookie '\" . $cookie_name . \"' is set!<br>\";\n" +
                "\t     echo \"Value is: \" . $_COOKIE[$cookie_name];\n" +
                "\t}\n" +
                "\t?>\n" +
                "\n" +
                "\t<p><strong>Note:</strong> You might have to reload the page to see the value of the cookie.</p>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "Cookie 'user' is set!\n" +
                "Value is: Kozu\n" +
                "Note: You might have to reload the page to see the value of the cookie.");

    }
}
