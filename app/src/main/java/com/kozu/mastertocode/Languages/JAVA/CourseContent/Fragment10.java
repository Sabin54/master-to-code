package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment10 extends AppCompatActivity {
    TextView content1, content2;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag10);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("An Exception can be anything which interrupts the normal execution of the program. When an exception occurs program processing terminates abnormally. In such cases we get a system generated error message. The good thing about exceptions is that they can be handled.\n" +
                "An exception can occur for many different reasons like Opening a non-existing file, Network connection problem, invalid data entered, class file missing which was supposed to be loaded and so on.\n" +
                "\n" +
                "Types of exceptions:\n" +
                "\n" +
                "Checked exceptions\n" +
                "A checked exception is an exception that occurs at the compile time, these are also called as compile time exceptions.\n" +
                "If these exceptions are not handled/declared in the program, it will give compilation error.\n" +
                "\n" +
                "Examples of Checked Exceptions :-\n" +
                "\tClassNotFoundException\n" +
                "\tIllegalAccessException\n" +
                "\tNoSuchFieldException\n" +
                "\tEOFException etc.\n" +
                "\n" +
                "Unchecked Exceptions\n" +
                "Runtime Exceptions are also known as Unchecked Exceptions as the compiler do not check whether the programmer has handled them or not but it's the duty of the programmer to handle these exceptions and provide a safe exit.\n" +
                "These exceptions need not be included in any method's throws list because compiler does not check to see if a method handles or throws these exceptions.\n" +
                "\n" +
                "Examples of Unchecked Exceptions:-\n" +
                "\tArithmeticException\n" +
                "\tArrayIndexOutOfBoundsException\n" +
                "\tNullPointerException\n" +
                "\tNegativeArraySizeException etc.\n" +
                "\n" +
                "Advantages of Exception Handling\n" +
                "\t\n" +
                "\t1. Exception handling allows us to control the normal flow of the program by using exception handling in program.\n" +
                "\n\t2. It throws an exception whenever a calling method encounters an error providing that the calling method takes care of that error.\n" +
                "\n\t3. It also gives us the scope of organizing and differentiating between different error types using a separate block of codes. This is done with the help of try-catch blocks.");

        content2.setText("A method catches an exception using a combination of the try and catch keywords.\n" +
                "A try/catch block is placed around the code that might generate an exception. \n" +
                "Code within a try/catch block is referred to as protected code, and the syntax for using try/catch looks like the following:\n" +
                "\t\t\n" +
                "\t\ttry\n" +
                "\t\t{\n" +
                "\t\t//Protected code\n" +
                "\t\t}catch(ExceptionName e1)\n" +
                "\t\t{\n" +
                "\t\t//Catch block\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "The code which is prone to exceptions is placed in the try block, when an exception occurs, that exception occurred is handled by catch block associated with it.\n" +
                "Every try block should be immediately followed either by a class block or finally block.\n" +
                "\n" +
                "\n" +
                "Multiple catch Blocks\n" +
                "A try block can be followed by multiple catch blocks. The syntax for multiple catch blocks looks like the following:\n" +
                "\t\ttry\n" +
                "\t\t{\n" +
                "\t\t//Protected code\n" +
                "\t\t}catch(ExceptionType1 e1)\n" +
                "\t\t{\n" +
                "\t\t//Catch block\n" +
                "\t\t}catch(ExceptionType2 e2)\n" +
                "\t\t{\n" +
                "\t\t//Catch block\n" +
                "\t\t}catch(ExceptionType3 e3)\n" +
                "\t\t{\n" +
                "\t\t//Catch block\n" +
                "\t\t}\n" +
                "\n" +
                "this is just an example of 3 catch statements you can have any number of them If an exception occurs in the protected code, the exception is thrown to the first catch block in the list. If the data type of the exception thrown matches ExceptionType1, it gets caught there. If not, the exception passes down to the second catch statement. This continues until the exception either is caught or falls through all catches, in which case the current method stops execution and the exception is thrown down to the previous method on the call stack.\n" +
                "\n" +
                "Finally block\n" +
                "Java finally block is a block that is used to execute important code such as closing connection, stream etc.\n" +
                "Java finally block is always executed whether exception is handled or not.\n" +
                "Java finally block must be followed by try or catch block.\n" +
                "\n" +
                "\tSyntax:\n" +
                "\t\ttry\n" +
                "\t\t{\n" +
                "\t\t//statements that may cause an exception\n" +
                "\t\t}\n" +
                "\t\tfinally\n" +
                "\t\t{\n" +
                "\t\t//statements to be executed\n" +
                "\t\t}");
    }
}
