package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program5 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("import java.util.Scanner;\n" +
                "public class Demo {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "\n" +
                "        int year;\n" +
                "        Scanner scan = new Scanner(System.in);\n" +
                "        System.out.println(\"Enter any Year:\");\n" +
                "        year = scan.nextInt();\n" +
                "        scan.close();\n" +
                "        boolean isLeap = false;\n" +
                "\n" +
                "        if(year % 4 == 0)\n" +
                "        {\n" +
                "            if( year % 100 == 0)\n" +
                "            {\n" +
                "                if ( year % 400 == 0)\n" +
                "                    isLeap = true;\n" +
                "                else\n" +
                "                    isLeap = false;\n" +
                "            }\n" +
                "            else\n" +
                "                isLeap = true;\n" +
                "        }\n" +
                "        else {\n" +
                "            isLeap = false;\n" +
                "        }\n" +
                "\n" +
                "        if(isLeap==true)\n" +
                "            System.out.println(year + \" is a Leap Year.\");\n" +
                "        else\n" +
                "            System.out.println(year + \" is not a Leap Year.\");\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Enter any Year: \n" +
                "2001\n" +
                "2001 is not a Leap Year.");
    }
}
