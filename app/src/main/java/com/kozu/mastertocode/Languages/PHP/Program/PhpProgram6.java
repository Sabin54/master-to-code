package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram6 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<?php\n" +
                "echo(sqrt(64) . \"<br>\");\n" +
                "echo(sqrt(0) . \"<br>\");\n" +
                "echo(sqrt(1) . \"<br>\");\n" +
                "echo(sqrt(9));\n" +
                "?>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "8\n" +
                "0\n" +
                "1\n" +
                "3");

    }
}
