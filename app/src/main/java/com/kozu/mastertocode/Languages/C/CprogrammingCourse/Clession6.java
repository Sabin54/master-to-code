package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession6 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    TextView title1, title2, title3, title4;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Indroduction to Pointer");
        title2.setText("2. What are Pointers?");
        title3.setText("3. Example of Pointer");
        title4.setText("4. NULL Pointers");

        content1.setText("Pointers in C are easy and fun to learn. Some C programming tasks are performed more easily with pointers, and other tasks, such as dynamic memory allocation, cannot be performed without using pointers. So it becomes necessary to learn pointers to become a perfect C programmer. Let's start learning them in simple and easy steps.");

        content2.setText("A pointer is a variable whose value is the address of another variable, i.e., direct address of the memory location. Like any variable or constant, you must declare a pointer before using it to store any variable address. The general form of a pointer variable declaration is −\n" +
                "\n" +
                "\ttype *var-name;\n" +
                "\n" +
                "Take a look at some of the valid pointer declarations:\n" +
                "\n" +
                "int    *ip;    /* pointer to an integer */\n" +
                "double *dp;    /* pointer to a double */\n" +
                "float  *fp;    /* pointer to a float */\n" +
                "char   *ch     /* pointer to a character */");

        content3.setText(
                "#include <stdio.h>\n" +
                "\n" +
                "int main () {\n" +
                "\n" +
                "   int  var = 20;   /* actual variable declaration */\n" +
                "   int  *ip;        /* pointer variable declaration */\n" +
                "\n" +
                "   ip = &var;  /* store address of var in pointer variable*/\n" +
                "\n" +
                "   printf(\"Address of var variable: %x\\n\", &var  );\n" +
                "\n" +
                "   /* address stored in pointer variable */\n" +
                "   printf(\"Address stored in ip variable: %x\\n\", ip );\n" +
                "\n" +
                "   /* access the value using the pointer */\n" +
                "   printf(\"Value of *ip variable: %d\\n\", *ip );\n" +
                "\n" +
                "   return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Address of var variable: bffd8b3c\n" +
                "Address stored in ip variable: bffd8b3c\n" +
                "Value of *ip variable: 20");

        content4.setText(
                "#include <stdio.h>\n" +
                "\n" +
                "int main () {\n" +
                "\n" +
                "   int  *ptr = NULL;\n" +
                "\n" +
                "   printf(\"The value of ptr is : %x\\n\", ptr  );\n" +
                " \n" +
                "   return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "The value of ptr is 0\n" +
                "\n" +
                "\n" +
                "It is always a good practice to assign a NULL value to a pointer variable in case you do not have an exact address to be assigned. This is done at the time of variable declaration. A pointer that is assigned NULL is called a null pointer.");

    }
}