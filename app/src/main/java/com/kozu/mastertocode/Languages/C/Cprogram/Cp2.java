package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp2  extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {    \n" +
                "\n" +
                "    int number1, number2, sum;\n" +
                "    \n" +
                "    printf(\"Enter two integers: \");\n" +
                "    scanf(\"%d %d\", &number1, &number2);\n" +
                "\n" +
                "    // calculating sum\n" +
                "    sum = number1 + number2;      \n" +
                "    \n" +
                "    printf(\"%d + %d = %d\", number1, number2, sum);\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter two integers: 12\n" +
                "11\n" +
                "12 + 11 = 23");

    }
}
