package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP7 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    char op;\n" +
                "    float num1, num2;\n" +
                "    cout << \"Enter operator either + or - or * or /: \";\n" +
                "    cin >> op;\n" +
                "    cout << \"\\nEnter two operands: \";\n" +
                "    cin >> num1 >> num2;\n" +
                "    switch(op)\n" +
                "    {\n" +
                "        case '+':\n" +
                "            cout <<”\\nResult is: ”<< num1+num2;\n" +
                "            break;\n" +
                "\n" +
                "        case '-':\n" +
                "            cout <<”\\nResult is: ”<< num1-num2;\n" +
                "            break;\n" +
                "\n" +
                "        case '*':\n" +
                "            cout <<”\\nResult is: ”<<num1*num2;\n" +
                "            break;\n" +
                "\n" +
                "        case '/':\n" +
                "            cout <<”\\nResult is: ”<<num1/num2;\n" +
                "            break;\n" +
                "\n" +
                "        default:\n" +
                "            // If the operator is other than +, -, * or /, error message is shown\n" +
                "            cout<<\"Error! operator is not correct\";\n" +
                "            break;\n" +
                "    }\n" +
                "getch();\n" +
                "return 0 ;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter operator either + or - or * or / : -\n" +
                "Enter two operands: 3.4\n" +
                "8.4\n" +
                "Result is: -5.0");

    }
}
