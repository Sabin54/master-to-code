package com.kozu.mastertocode.Languages.C.Cprogram;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kozu.mastertocode.Languages.PHP.Program.PhpProgram1;
import com.kozu.mastertocode.R;

public class CprogramList extends AppCompatActivity {

    CardView t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17, t18, t19, t20, t21, t22, t23;
    TextView title1,title2,title3,title4,title5,title6,title7,title8,title9,title10,title11,title12,title13,title14,title15,title16,title17,title18,title19,title20,title21,title22,title23;
    AdView adView, adView1;
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_programs);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7031830486098035/9463900861");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        t3 = findViewById(R.id.t3);
        t4 = findViewById(R.id.t4);
        t5 = findViewById(R.id.t5);
        t6 = findViewById(R.id.t6);
        t7 = findViewById(R.id.t7);
        t8 = findViewById(R.id.t8);
        t9 = findViewById(R.id.t9);
        t10 = findViewById(R.id.t10);
        t11 = findViewById(R.id.t11);
        t12 = findViewById(R.id.t12);
        t13 = findViewById(R.id.t13);
        t14 = findViewById(R.id.t14);
        t15 = findViewById(R.id.t15);
        t16 = findViewById(R.id.t16);
        t17 = findViewById(R.id.t17);
        t18 = findViewById(R.id.t18);
        t19 = findViewById(R.id.t19);
        t20 = findViewById(R.id.t20);
        t21 = findViewById(R.id.t21);
        t22 = findViewById(R.id.t22);
        t23 = findViewById(R.id.t23);


        title1= findViewById(R.id.title1);
        title2= findViewById(R.id.title2);
        title3= findViewById(R.id.title3);
        title4= findViewById(R.id.title4);
        title5= findViewById(R.id.title5);
        title6= findViewById(R.id.title6);
        title7= findViewById(R.id.title7);
        title8= findViewById(R.id.title8);
        title9= findViewById(R.id.title9);
        title10= findViewById(R.id.title10);
        title11= findViewById(R.id.title11);
        title12= findViewById(R.id.title12);
        title13= findViewById(R.id.title13);
        title14= findViewById(R.id.title14);
        title15= findViewById(R.id.title15);
        title16= findViewById(R.id.title16);
        title17= findViewById(R.id.title17);
        title18= findViewById(R.id.title18);
        title19= findViewById(R.id.title19);
        title20= findViewById(R.id.title20);
        title21= findViewById(R.id.title21);
        title22= findViewById(R.id.title22);
        title23= findViewById(R.id.title23);

        title1.setText("Program to Print an Integer");
        title2.setText("Program to Add Two Integers");
        title3.setText("Program to Multiply Two Floating-Point Numbers");
        title4.setText("Program to Find ASCII Value of a Character");
        title5.setText("Program to Find the Size of int, float, double and char");
        title6.setText("Program to Check Whether a Number is Even or Odd");
        title7.setText("Check Whether a Character is a Vowel or Consonant");
        title8.setText("Program to Swap Two Numbers");
        title9.setText("Program to Find the Largest Number Among Three Numbers");
        title10.setText("Program to Check Leap Year");
        title11.setText("Program to Calculate the Sum of Natural Numbers");
        title12.setText("Program to Display Fibonacci Sequence");
        title13.setText("Program to Count Number of Digits in an Integer");
        title14.setText("Program to Calculate the Power of a Number");
        title15.setText("Program to Check Whether a Number is Palindrome or Not");
        title16.setText("Program to Display Factors of a Number");
        title17.setText("Check Prime or Armstrong Number Using User-defined Function");
        title18.setText("Program to Find G.C.D Using Recursion");
        title19.setText("Reverse a sentence using recursion");
        title20.setText("Program to Print Pyramid");
        title21.setText("Program to Check Whether a Number is Positive or Negative");
        title22.setText("Program to Generate Multiplication Table");
        title23.setText("Program to Add Two Matrices Using Multi-dimensional Arrays");

        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp1.class);
                startActivity(intent);
            }
        });
        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp2.class);
                startActivity(intent);
            }
        });
        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp3.class);
                startActivity(intent);
            }
        });
        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp4.class);
                startActivity(intent);
            }
        });

        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp5.class);
                startActivity(intent);
            }
        });
        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp6.class);
                startActivity(intent);
            }
        });
        t7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp7.class);
                startActivity(intent);
            }
        });
        t8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp8.class);
                startActivity(intent);
            }
        });
        t9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp9.class);
                startActivity(intent);
            }
        });
        t10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp10.class);
                startActivity(intent);
            }
        });
        t11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp11.class);
                startActivity(intent);
            }
        });
        t12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp12.class);
                startActivity(intent);
            }
        });
        t13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp13.class);
                startActivity(intent);
            }
        });
        t14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp14.class);
                startActivity(intent);
            }
        });
        t15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp15.class);
                startActivity(intent);
            }
        });
        t16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp16.class);
                startActivity(intent);
            }
        });
        t17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp17.class);
                startActivity(intent);
            }
        });
        t18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp18.class);
                startActivity(intent);
            }
        });
        t19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp19.class);
                startActivity(intent);
            }
        });
        t20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp20.class);
                startActivity(intent);
            }
        });
        t21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp21.class);
                startActivity(intent);
            }
        });
        t22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp22.class);
                startActivity(intent);
            }
        });
        t23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Cp23.class);
                startActivity(intent);
            }
        });


    }
}