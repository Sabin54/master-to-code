package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program15 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "public class JavaExample {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "\n" +
                "        int num, number, temp, total = 0;\n" +
                "        System.out.println(\"Ënter 3 Digit Number\");\n" +
                "        Scanner scanner = new Scanner(System.in);\n" +
                "        num = scanner.nextInt();\n" +
                "        scanner.close();\n" +
                "        number = num;\n" +
                "\n" +
                "        for( ;number!=0;number /= 10)\n" +
                "        {\n" +
                "            temp = number % 10;\n" +
                "            total = total + temp*temp*temp;\n" +
                "        }\n" +
                "\n" +
                "        if(total == num)\n" +
                "            System.out.println(num + \" is an Armstrong number\");\n" +
                "        else\n" +
                "            System.out.println(num + \" is not an Armstrong number\");\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Ënter 3 Digit Number\n" +
                "371\n" +
                "371 is an Armstrong number");
    }
}
