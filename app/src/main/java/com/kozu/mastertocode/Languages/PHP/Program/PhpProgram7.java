package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram7 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "\t<?php\n" +
                "\t$txt1 = \"Hello\";\n" +
                "\t$txt2 = \" world!\";\n" +
                "\techo $txt1 . $txt2;\n" +
                "\t?>  \n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "Hello world!");

    }
}
