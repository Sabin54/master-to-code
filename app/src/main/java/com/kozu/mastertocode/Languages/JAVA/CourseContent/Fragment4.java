package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment4 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5, content6, content7, content8, content9, content10;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag4);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);
        content9 = findViewById(R.id.content9);
        content10 = findViewById(R.id.content10);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("public class Person { \n" +
                "// state of an object\n" +
                "\tint age; \n" +
                "\tString name; \n" +
                "\n" +
                "// behavior of an object \n" +
                "public void set_value() { \n" +
                "\tage = 20; \n" +
                "\tname = \"Robin\"; \n" +
                "} \n" +
                "public void get_value() { \n" +
                "\tSystem.out.println(\"Age is \" + age); \n" +
                "\tSystem.out.println(\"Name is \" + name); \n" +
                "}\n" +
                " // main method \n" +
                "public static void main(String [] args) { \n" +
                "// creates a new Person object \n" +
                "\tPerson p = new Person(); \n" +
                "\n" +
                "// changes state through behavior\n" +
                " \tp.set_value(); \n" +
                "} \n" +
                "}\n" +
                "\n\nIn Java, instances of a class are known as objects. Every object has state and behavior in the form of instance fields and methods respectively.\n");


        content2.setText("public class Maths { \n" +
                "\tpublic Maths() { \n" +
                "\t\tSystem.out.println(\"I am constructor\"); \n" +
                "\t} \n" +
                "public static void main(String [] args) \n" +
                "{ \n" +
                "\tSystem.out.println(\"I am main\");\n" +
                " \tMaths obj1 = new Maths(); \n" +
                "\t} \n" +
                "}\n" +
                "\n" +
                "Java classes contain a constructor method which is used to create instances of the class.\n" +
                "The constructor is named after the class. If no constructor is defined, a default empty constructor is used.\n");


        content3.setText("public class Person { \n" +
                "\tint age; String name; \n" +
                "\n" +
                "// Constructor method \n" +
                "public Person(int age, String name) \n" +
                "\t{ \n" +
                "\tthis.age = age; \n" +
                "\tthis.name = name; \n" +
                "\t} \n" +
                "public static void main(String[] args){\n" +
                "\t Person Bob = new Person(31, \"Bob\"); \n" +
                "\tPerson Alice = new Person(27, \"Alice\"); \n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "Java instances are objects that are based on classes. For example, Bob may be an instance of the class Person.\n" +
                "Every instance has access to its own set of variables which are known as instance fields, which are variables declared within the scope of the instance. Values for instance fields are assigned within the constructor method.\n" +
                "\n");

        content4.setText("public class Person { \n" +
                "\tint age; \n" +
                "public Person(int a) \n" +
                "{ \n" +
                "\tage = a; \n" +
                "} \n" +
                "public static void main(String [] args) { \n" +
                "// Here, we create a new instance of the Person class \n" +
                "\tPerson p = new Person(20); \n" +
                "\tSystem.out.println(\"Age is \" + p.age); \n" +
                "// Output: Age is 20 \n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "In Java, we use the new keyword followed by a call to the class constructor in order to create a new instance of a class.\n" +
                "The constructor can be used to provide initial values to instance fields.\n");

        content5.setText("public class Person { \n" +
                "int age; \n" +
                "public static void main(String [] args) \n" +
                "{\n" +
                " \tPerson p = new Person();\n" +
                " // here we use dot notation to set age \n" +
                " \tp.age = 20; \n" +
                "// here we use dot notation to access age and print\n" +
                "\tSystem.out.println(\"Age is \" + p.age); \n" +
                "// Output: Age is 20 \n" +
                "\t} \n" +
                "}\n" +
                "\n" +
                "In Java programming language, we use . to access the variables and methods of an object or a Class.\n" +
                "This is known as dot notation and the structure looks like this-\n" +
                "instanceOrClassName.fieldOrMethodName\n" +
                "\n");

        content6.setText("// Here is a public method named sum whose return type is int and has two parameters a and b \n" +
                "public int sum(int a, int b){\n" +
                " \treturn(a + b); \n" +
                "} \n" +
                "\n" +
                "In Java, methods are defined with a method signature, which specifies the scope (private or public), return type, name of the method, and any parameters it receives.\n");

        content7.setText("public class Maths { \n" +
                "public static void sum(int a, int b) {\n" +
                " // Start of sum \n" +
                "\tint result = a + b; \n" +
                "\tSystem.out.println(\"Sum is \" + result); \n" +
                "} \n" +
                "// End of sum \n" +
                "public static void main(String [] args) { \n" +
                "// Here, we call the sum method\n" +
                " \tsum(10, 20); \n" +
                "// Output: Sum is 30 \n" +
                "\t} \n" +
                "}\n" +
                "\n" +
                "In Java, we use curly brackets {} to enclose the body of a method.\n" +
                "The statements written inside the {} are executed when a method is called.\n");

        content8.setText("public class Maths { \n" +
                "\tpublic static void main(String [] args) \n" +
                "{ \n" +
                "\tint i, j;\n" +
                " \tSystem.out.println(\"These two variables are available in main method only\"); \n" +
                "\t} \n" +
                "} \n" +
                "\nJava variables defined inside a method cannot be used outside the scope of that method.\n");

        content9.setText("public class Maths { \n" +
                "// return type is int \n" +
                "public int sum(int a, int b) \n" +
                "{ \n" +
                "\tint k; \n" +
                "\tk = a + b; \n" +
                "// sum is returned using the return keyword \n" +
                "\treturn k; \n" +
                "} \n" +
                "\tpublic static void main(String [] args) \n" +
                "{ \n" +
                "\tMaths m = new Maths(); \n" +
                "\tint result; \n" +
                "\tresult = m.sum(10, 20); \n" +
                "\tSystem.out.println(\"Sum is \" + result); \n" +
                "// Output: Sum is 30 \n" +
                "\t} \n" +
                "}\n" +
                "\n" +
                "A Java method can return any value that can be saved in a variable. The value returned must match with the return type specified in the method signature.\n" +
                "The value is returned using the return keyword.\n" +
                "\n");


        content10.setText("public class Maths {\n" +
                " public int sum(int a, int b) \n" +
                "\t{ \n" +
                "\tint k = a + b; \n" +
                "\treturn k; \n" +
                "} \n" +
                "public static void main(String [] args){\n" +
                " \tMaths m = new Maths(); \n" +
                "\tint result = m.sum(10, 20); \n" +
                "\tSystem.out.println(\"sum is \" + result); // prints - sum is 30\t } \n" +
                "}\n" +
                "\nIn java, parameters are declared in a method definition. The parameters act as variables inside the method and hold the value that was passed in. They can be used inside a method for printing or calculation purposes.\n" +
                "In the example, a and b are two parameters which, when the method is called, hold the value 10 and 20 respectively.\n");

    }

}
