package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession9 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    TextView title1, title2, title3, title4;
    AdView adView, adView1;

    CardView cv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        cv4 = findViewById(R.id.cv4);

        cv4.setVisibility(View.GONE);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. C File Handling");
        title2.setText("2. Basic File Handling Operations");
        title3.setText("3. File Opening Mode");

        content1.setText("A file is a collection of streamed bytes stored on a secondary storage device. These streamed byte can be interpreted as characters, words, lines, paragraphs and pages of textual document; fields and records belonging to the database; or pixel from graphical image. Through the file handling function we can create, modify, move or delete files on system. In every programming lanuage it id important to implement file handling function. In C programming Language a special set of functions have been designed for handling file operations.");

        content2.setText("Some of the basic file handling operations are:-\n" +
                "1. Open and Close Files\n" +
                "\tSyntax:\n" +
                "\t\t1. FILE * fopen(char * filename, char * mode);\n" +
                "\n" +
                "\t\t2. fclose(FILE *fptr)\n" +
                "\n" +
                "\t\tNote: \n" +
                "\t\t*fptr = File Pointer\n" +
                "\t\tchar = Characters\n" +
                "\n" +
                "2. Delete Files\n" +
                "\tSyntax: \n" +
                "\t\tremove (“file_name”);\n" +
                "\n" +
                "The remove function is used to delete the file whose name is specified. This function is in the stdio header file.\n");

        content3.setText("A file can be opened in different modes. Below are some of the most commonly used modes for opening or creating a file.\n" +
                "\n" +
                "1. r : opens a text file in reading mode.\n" +
                "\n" +
                "2. w : opens or creates a text file in writing mode.\n" +
                "\n" +
                "3. a : opens a text file in append mode.\n" +
                "\n" +
                "4. r+ : opens a text file in both reading and writing mode. The file must exist.\n" +
                "\n" +
                "5. w+ : opens a text file in both reading and writing mode. If the file exists, it's truncated first before overwriting. Any old data will be lost. If the file doesn't exist, a new file will be created.\n" +
                "\n" +
                "6. a+ : opens a text file in both reading and appending mode. New data is appended at the end of the file and does not overwrite the existing content.\n");
    }
}