package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program12 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "class FactorialDemo{\n" +
                "   public static void main(String args[]){\n" +
                "      //Scanner object for capturing the user input\n" +
                "      Scanner scanner = new Scanner(System.in);\n" +
                "      System.out.println(\"Enter the number:\");\n" +
                "      //Stored the entered value in variable\n" +
                "      int num = scanner.nextInt();\n" +
                "      //Called the user defined function fact\n" +
                "      int factorial = fact(num);\n" +
                "      System.out.println(\"Factorial of entered number is: \"+factorial);\n" +
                "   }\n" +
                "   static int fact(int n)\n" +
                "   {\n" +
                "       int output;\n" +
                "       if(n==1){\n" +
                "         return 1;\n" +
                "       }\n" +
                "       //Recursion: Function calling itself!!\n" +
                "       output = fact(n-1)* n;\n" +
                "       return output;\n" +
                "   }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Enter the number:\n" +
                "5\n" +
                "Factorial of entered number is: 120");
    }
}
