package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram10 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<?php\n" +
                "// set the expiration date to one hour ago\n" +
                "setcookie(\"user\", \"\", time() - 3600);\n" +
                "?>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t<?php\n" +
                "\techo \"Cookie 'user' is deleted.\";\n" +
                "\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "Cookie 'user' is deleted.");
    }
}
