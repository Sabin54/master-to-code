package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP10 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<conio.h>\n" +
                "#include<iostream.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "   int n,i,j,c,k,place;\n" +
                "   \n" +
                "   cout<<\"Enter the no of rows: \"<<endl;\n" +
                "   cin>>n;\n" +
                "   cout<<\" \\n\\nPASCAL TRIANGLE\"<<endl;\n" +
                "   cout<<\"\\n\\n\";\n" +
                "   place=n;\n" +
                "      for(i=0;i<n;i++){\n" +
                "         c=1;\n" +
                "         for(k=place;k>=0;k--)\n" +
                "            cout<<\" \";\n" +
                "\n" +
                "            place--;\n" +
                "         for(j=0;j<=i;j++){\n" +
                "               cout<<c<<\" \";\n" +
                "               c=(c*(i-j)/(j+1));\n" +
                "            }\n" +
                "         cout<<\"\\n\";\n" +
                "\n" +
                "       }\n" +
                "getch();\n" +
                "return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter number of rows: 5\n" +
                "1\n" +
                "1 1\n" +
                "1 2 1\n" +
                "1 3 3 1\n" +
                "1 4 6 4 1");

    }
}
