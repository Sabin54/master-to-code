package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program7  extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.HashMap;\n" +
                "import java.util.Map;\n" +
                "import java.util.Set;\n" +
                " \n" +
                "public class Details {\n" +
                " \n" +
                "  public void countDupChars(String str){\n" +
                " \n" +
                "    //Create a HashMap \n" +
                "    Map<Character, Integer> map = new HashMap<Character, Integer>(); \n" +
                " \n" +
                "    //Convert the String to char array\n" +
                "    char[] chars = str.toCharArray();\n" +
                " \n" +
                "    /* logic: char are inserted as keys and their count\n" +
                "     * as values. If map contains the char already then\n" +
                "     * increase the value by 1\n" +
                "     */\n" +
                "    for(Character ch:chars){\n" +
                "      if(map.containsKey(ch)){\n" +
                "         map.put(ch, map.get(ch)+1);\n" +
                "      } else {\n" +
                "         map.put(ch, 1);\n" +
                "        }\n" +
                "    }\n" +
                " \n" +
                "    //Obtaining set of keys\n" +
                "    Set<Character> keys = map.keySet();\n" +
                " \n" +
                "    /* Display count of chars if it is\n" +
                "     * greater than 1. All duplicate chars would be \n" +
                "     * having value greater than 1.\n" +
                "     */\n" +
                "    for(Character ch:keys){\n" +
                "      if(map.get(ch) > 1){\n" +
                "        System.out.println(\"Char \"+ch+\" \"+map.get(ch));\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                " \n" +
                "  public static void main(String a[]){\n" +
                "    Details obj = new Details();\n" +
                "    System.out.println(\"String: BeginnersBook.com\");\n" +
                "    System.out.println(\"-------------------------\");\n" +
                "    obj.countDupChars(\"BeginnersBook.com\");\n" +
                "  \n" +
                "    System.out.println(\"\\nString: ChaitanyaSingh\");\n" +
                "    System.out.println(\"-------------------------\");\n" +
                "    obj.countDupChars(\"ChaitanyaSingh\");\n" +
                "\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "String: BeginnersBook.com\n" +
                "-------------------------\n" +
                "Char e 2\n" +
                "Char B 2\n" +
                "Char n 2\n" +
                "Char o 3\n" +
                "\n" +
                "String: ChaitanyaSingh\n" +
                "-------------------------\n" +
                "Char a 3\n" +
                "Char n 2\n" +
                "Char h 2\n" +
                "Char i 2");
    }
}
