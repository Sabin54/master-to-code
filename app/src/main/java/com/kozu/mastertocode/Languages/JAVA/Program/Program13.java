package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program13 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "\n" +
                "class PrimeNumberDemo\n" +
                "{\n" +
                "   public static void main(String args[])\n" +
                "   {\n" +
                "      int n;\n" +
                "      int status = 1;\n" +
                "      int num = 3;\n" +
                "      //For capturing the value of n\n" +
                "      Scanner scanner = new Scanner(System.in);\n" +
                "      System.out.println(\"Enter the value of n:\");\n" +
                "      //The entered value is stored in the var n\n" +
                "      n = scanner.nextInt();\n" +
                "      if (n >= 1)\n" +
                "      {\n" +
                "         System.out.println(\"First \"+n+\" prime numbers are:\");\n" +
                "         //2 is a known prime number\n" +
                "         System.out.println(2);\n" +
                "      }\n" +
                "\n" +
                "      for ( int i = 2 ; i <=n ;  )\n" +
                "      {\n" +
                "         for ( int j = 2 ; j <= Math.sqrt(num) ; j++ )\n" +
                "         {\n" +
                "            if ( num%j == 0 )\n" +
                "            {\n" +
                "               status = 0;\n" +
                "               break;\n" +
                "            }\n" +
                "         }\n" +
                "         if ( status != 0 )\n" +
                "         {\n" +
                "            System.out.println(num);\n" +
                "            i++;\n" +
                "         }\n" +
                "         status = 1;\n" +
                "         num++;\n" +
                "      }         \n" +
                "   }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Enter the value of n:\n" +
                "5\n" +
                "First 5 prime numbers are:\n" +
                "2\n" +
                "3\n" +
                "5\n" +
                "7\n" +
                "11");
    }
}
