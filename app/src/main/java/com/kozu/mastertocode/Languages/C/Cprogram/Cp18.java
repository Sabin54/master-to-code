package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp18 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int hcf(int n1, int n2);\n" +
                "int main() {\n" +
                "    int n1, n2;\n" +
                "    printf(\"Enter two positive integers: \");\n" +
                "    scanf(\"%d %d\", &n1, &n2);\n" +
                "    printf(\"G.C.D of %d and %d is %d.\", n1, n2, hcf(n1, n2));\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "int hcf(int n1, int n2) {\n" +
                "    if (n2 != 0)\n" +
                "        return hcf(n2, n1 % n2);\n" +
                "    else\n" +
                "        return n1;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter two positive integers: 366\n" +
                "60\n" +
                "G.C.D of 366 and 60 is 6.");

    }
}
