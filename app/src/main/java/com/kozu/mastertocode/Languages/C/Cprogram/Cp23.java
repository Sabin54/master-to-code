package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp23 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int r, c, a[100][100], b[100][100], sum[100][100], i, j;\n" +
                "    printf(\"Enter the number of rows (between 1 and 100): \");\n" +
                "    scanf(\"%d\", &r);\n" +
                "    printf(\"Enter the number of columns (between 1 and 100): \");\n" +
                "    scanf(\"%d\", &c);\n" +
                "\n" +
                "    printf(\"\\nEnter elements of 1st matrix:\\n\");\n" +
                "    for (i = 0; i < r; ++i)\n" +
                "        for (j = 0; j < c; ++j) {\n" +
                "            printf(\"Enter element a%d%d: \", i + 1, j + 1);\n" +
                "            scanf(\"%d\", &a[i][j]);\n" +
                "        }\n" +
                "\n" +
                "    printf(\"Enter elements of 2nd matrix:\\n\");\n" +
                "    for (i = 0; i < r; ++i)\n" +
                "        for (j = 0; j < c; ++j) {\n" +
                "            printf(\"Enter element a%d%d: \", i + 1, j + 1);\n" +
                "            scanf(\"%d\", &b[i][j]);\n" +
                "        }\n" +
                "\n" +
                "    // adding two matrices\n" +
                "    for (i = 0; i < r; ++i)\n" +
                "        for (j = 0; j < c; ++j) {\n" +
                "            sum[i][j] = a[i][j] + b[i][j];\n" +
                "        }\n" +
                "\n" +
                "    // printing the result\n" +
                "    printf(\"\\nSum of two matrices: \\n\");\n" +
                "    for (i = 0; i < r; ++i)\n" +
                "        for (j = 0; j < c; ++j) {\n" +
                "            printf(\"%d   \", sum[i][j]);\n" +
                "            if (j == c - 1) {\n" +
                "                printf(\"\\n\\n\");\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter the number of rows (between 1 and 100): 2\n" +
                "Enter the number of columns (between 1 and 100): 3\n" +
                "\n" +
                "Enter elements of 1st matrix:\n" +
                "Enter element a11: 2\n" +
                "Enter element a12: 3\n" +
                "Enter element a13: 4\n" +
                "Enter element a21: 5\n" +
                "Enter element a22: 2\n" +
                "Enter element a23: 3\n" +
                "Enter elements of 2nd matrix:\n" +
                "Enter element a11: -4\n" +
                "Enter element a12: 5\n" +
                "Enter element a13: 3\n" +
                "Enter element a21: 5\n" +
                "Enter element a22: 6\n" +
                "Enter element a23: 3\n" +
                "\n" +
                "Sum of two matrices: \n" +
                "-2   8   7   \n" +
                "\n" +
                "10   8   6  ");

    }
}
