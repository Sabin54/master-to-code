package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program17 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("class JavaExample {\n" +
                "   public static void main(String args[]) {\n" +
                "    int n = 100;\n" +
                "    System.out.print(\"Even Numbers from 1 to \"+n+\" are: \");\n" +
                "    for (int i = 1; i <= n; i++) {\n" +
                "       //if number%2 == 0 it means its an even number\n" +
                "       if (i % 2 == 0) {\n" +
                "        System.out.print(i + \" \");\n" +
                "       }\n" +
                "    }\n" +
                "   }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "\n" +
                "Even Numbers from 1 to 100 are: 2 4 6 8 10 12 14 16 18 20 22 24 26 28 \n" +
                "30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 \n" +
                "78 80 82 84 86 88 90 92 94 96 98 100");
    }
}
