package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp9 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    double n1, n2, n3;\n" +
                "    printf(\"Enter three different numbers: \");\n" +
                "    scanf(\"%lf %lf %lf\", &n1, &n2, &n3);\n" +
                "\n" +
                "    // if n1 is greater than both n2 and n3, n1 is the largest\n" +
                "    if (n1 >= n2 && n1 >= n3)\n" +
                "        printf(\"%.2f is the largest number.\", n1);\n" +
                "\n" +
                "    // if n2 is greater than both n1 and n3, n2 is the largest\n" +
                "    if (n2 >= n1 && n2 >= n3)\n" +
                "        printf(\"%.2f is the largest number.\", n2);\n" +
                "\n" +
                "    // if n3 is greater than both n1 and n2, n3 is the largest\n" +
                "    if (n3 >= n1 && n3 >= n2)\n" +
                "        printf(\"%.2f is the largest number.\", n3);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter three numbers: -4.5\n" +
                "3.9\n" +
                "5.6\n" +
                "5.60 is the largest number.");

    }
}
