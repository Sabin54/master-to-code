package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram4 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                "\n" +
                "<?php\n" +
                "// Check if the type of a variable is integer   \n" +
                "$x = 5985;\n" +
                "var_dump(is_int($x));\n" +
                "\n" +
                "echo \"<br>\";\n" +
                "\n" +
                "// Check again... \n" +
                "$x = 59.85;\n" +
                "var_dump(is_int($x));\n" +
                "?>  \n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "bool(true)\n" +
                "bool(false)");
    }
}
