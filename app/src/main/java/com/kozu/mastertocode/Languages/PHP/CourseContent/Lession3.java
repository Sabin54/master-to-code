package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession3 extends AppCompatActivity {
    TextView content1, content2, content3, content4,content5, content6, content7, content8, content9;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag3);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);
        content9 = findViewById(R.id.content9);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("An if statement contains a Boolean expression and block of statements enclosed within braces.\n" +
                "\n\tif(conditional expression)\n" +
                "\t{\n" +
                "\t//Statement\n" +
                "\t}\n" +
                "\nIf the Boolean expression is true then IF block is executed otherwise program directly goes to Else statement without executing IF block.\n");

        content2.setText("If statement block with else statement is known as as if...else statement. \n" +
                "Else portion is non-compulsory.\n" +
                "Example IF, ELSE IF and Else Statement: \n" +
                "\tif ( condition_one ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t } \n" +
                "\telse if ( condition_two ) \n" +
                "\t{ \n" +
                "\t//statements\n" +
                "\t }\n" +
                "\t else \n" +
                "\t{\n" +
                "\t //statements\n" +
                "\t }\n" +
                "\n" +
                "If first condition is true, then compiler will execute the if block, And if First condition is false it will check the second condition available in else if block of statements, if it is true else if block will execute and  if false then else block of statements will be executed.\n" +
                "\n");

        content3.setText("when a series of decisions are involved, we may have to use more than one if...else statement in nested form as follows:\n" +
                "if (test condition1)\n" +
                " { \n" +
                "\tIf (test condition2) \n" +
                "\t{ \n" +
                "\t//statement1; \n" +
                "\t}\n" +
                "\t Else\n" +
                "\t {\n" +
                "\t //statement2; \n" +
                "\t} \n" +
                "} \n" +
                "else \n" +
                "{ \n" +
                "\t//statement3; \n" +
                "}\n" +
                "\n");

        content4.setText("A switch statement is used instead of nested if...else statements. \n" +
                "It is multiple branch decision statement. A switch statement tests a variable with list of values for equivalence. Each value is called a case. The case value must be a constant i.\n" +
                "\n" +
                "SYNTAX \n" +
                "switch(expression) {\n" +
                "  \tcase x:\n" +
                "   \t\t// code block\n" +
                "    \t\tbreak;\n" +
                "  \tcase y:\n" +
                "   \t\t // code block\n" +
                "    \t\tbreak;\n" +
                "  \tdefault:\n" +
                "    \t\t// code block\n" +
                "}\n" +
                "\n");

        content5.setText("-Loops through a block of code as long as the specified condition is true\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\t\t$x = 1;\n" +
                "\n" +
                "\t\twhile($x <= 5) \n" +
                "\t\t{\n" +
                "\t\t  echo \"The number is: $x <br>\";\n" +
                "\t\t  $x++;\n" +
                "\t\t}\n" +
                "\t?>\t\n" +
                "\n" +
                "$x = 1; - Initialize the loop counter ($x), and set the start value to 1\n" +
                "$x <= 5 - Continue the loop as long as $x is less than or equal to 5\n" +
                "$x++; - Increase the loop counter value by 1 for each iteration");

        content6.setText("-The do...while loop will always execute the block of code once, it will then check the condition, and repeat the loop while the specified condition is true.\n" +
                "\n" +
                "Example:\n" +
                "\t\n" +
                "\t<?php\n" +
                "\t$x = 1;\n" +
                "\tdo \n" +
                "\t{\n" +
                "\t  echo \"The number is: $x <br>\";\n" +
                "\t  $x++;\n" +
                "\t} \n" +
                "\twhile ($x <= 5);\n" +
                "\t?>\n" +
                "\n");

        content7.setText("The for loop is used when you know in advance how many times the script should run.\n" +
                "\n" +
                "Parameters:\n" +
                "\n" +
                "init counter ($x = 0;) : Initialize the loop counter value\n" +
                "test counter ($x <= 10;) : Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.\n" +
                "increment counter ($x++) : Increases the loop counter value\n" +
                "\n" +
                "Example:\n" +
                "\t\n" +
                "\t<?php\n" +
                "\tfor ($x = 0; $x <= 10; $x++) \n" +
                "\t{\n" +
                "\t  echo \"The number is: $x <br>\";\n" +
                "\t}\n" +
                "\t?>");

        content8.setText("The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\t$colors = array(\"red\", \"green\", \"blue\", \"yellow\");\n" +
                "\n" +
                "\tforeach ($colors as $value) \n" +
                "\t{\n" +
                "\t  echo \"$value <br>\";\n" +
                "\t}\n" +
                "\t?>\n");


        content9.setText("PHP Break:\n" +
                "You have already seen the break statement used in an earlier chapter of this tutorial. It was used to \"jump out\" of a switch statement.\n" +
                "\n" +
                "The break statement can also be used to jump out of a loop.\n" +
                "\n" +
                "This example jumps out of the loop when x is equal to 4:\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\tfor ($x = 0; $x < 10; $x++) {\n" +
                "\t  if ($x == 4) {\n" +
                "\t    break;\n" +
                "\t  }\n" +
                "\t  echo \"The number is: $x <br>\";\n" +
                "\t}\n" +
                "\t?>\n" +
                "\n" +
                "PHP Continue:\n" +
                "The continue statement breaks one iteration (in the loop), if a specified condition occurs, and continues with the next iteration in the loop.\n" +
                "\n" +
                "This example skips the value of 4:\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\tfor ($x = 0; $x < 10; $x++) {\n" +
                "\t  if ($x == 4) {\n" +
                "\t    continue;\n" +
                "\t  }\n" +
                "\t  echo \"The number is: $x <br>\";\n" +
                "\t}\n" +
                "\t?>\n" +
                "\n");

    }
}
