package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp21 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    double num;\n" +
                "    printf(\"Enter a number: \");\n" +
                "    scanf(\"%lf\", &num);\n" +
                "\n" +
                "    if (num < 0.0)\n" +
                "        printf(\"You entered a negative number.\");\n" +
                "    else if (num > 0.0)\n" +
                "        printf(\"You entered a positive number.\");\n" +
                "    else\n" +
                "        printf(\"You entered 0.\");\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output 1:\n" +
                "\n" +
                "Enter a number: 12.3\n" +
                "You entered a positive number.\n" +
                "\n" +
                "Output 2:\n" +
                "\n" +
                "Enter a number: 0\n" +
                "You entered 0.");

    }
}
