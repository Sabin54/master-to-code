package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program19 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("public class JavaExample {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "\n" +
                "        int count = 7, num1 = 0, num2 = 1;\n" +
                "        System.out.print(\"Fibonacci Series of \"+count+\" numbers:\");\n" +
                "\n" +
                "        for (int i = 1; i <= count; ++i)\n" +
                "        {\n" +
                "            System.out.print(num1+\" \");\n" +
                "\n" +
                "            /* On each iteration, we are assigning second number\n" +
                "             * to the first number and assigning the sum of last two\n" +
                "             * numbers to the second number\n" +
                "             */\n" +
                "            int sumOfPrevTwo = num1 + num2;\n" +
                "            num1 = num2;\n" +
                "            num2 = sumOfPrevTwo;\n" +
                "        }\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "\n" +
                "Fibonacci Series of 7 numbers:0 1 1 2 3 5 8\n");
    }

}
