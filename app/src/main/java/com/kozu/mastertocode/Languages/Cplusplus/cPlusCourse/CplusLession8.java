package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession8 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    CardView cv4, cv3;

    TextView title1, title2, title3, title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        //cv3.setVisibility(View.GONE);
        cv4.setVisibility(View.GONE);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Exceptions");
        title2.setText("2. try and catch");
        title3.setText("3. Example");

        content1.setText("When executing C++ code, different errors can occur: coding errors made by the programmer, errors due to wrong input, or other unforeseeable things.\n" +
                "\n" +
                "When an error occurs, C++ will normally stop and generate an error message. The technical term for this is: C++ will throw an exception (throw an error).");

        content2.setText("Exception handling in C++ consist of three keywords: try, throw and catch:\n" +
                "\n" +
                "The try statement allows you to define a block of code to be tested for errors while it is being executed.\n" +
                "\n" +
                "The throw keyword throws an exception when a problem is detected, which lets us create a custom error.\n" +
                "\n" +
                "The catch statement allows you to define a block of code to be executed, if an error occurs in the try block.\n" +
                "\n" +
                "The try and catch keywords come in pairs");

        content3.setText("try {\n" +
                "  int age = 15;\n" +
                "  if (age > 18) \n" +
                "     {\n" +
                "       cout << \"Access granted - you are old enough.\";\n" +
                "     } \n" +
                "  else \n" +
                "     {\n" +
                "       throw (age);\n" +
                "     }\n" +
                "}\n" +
                "catch (int myNum) \n" +
                "   {\n" +
                "     cout << \"Access denied - You must be at least 18 years old.\\n\";\n" +
                "     cout << \"Age is: \" << myNum;\n" +
                "   }\n" +
                "\n" +
                "output:\n" +
                "   \n" +
                "   Access denied - You must be at least 18 years old.\n" +
                "   Age is: 15");
    }
}
