package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession6 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    CardView cv4, cv3;

    TextView title1, title2, title3, title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        //cv3.setVisibility(View.GONE);
        cv4.setVisibility(View.GONE);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Introduction");
        title2.setText("2. Purpose of Inheritance in C++");
        title3.setText("3. Example");


        content1.setText("Inheritance is the capability of one class to acquire properties and characteristics from another class. The class whose properties are inherited by other class is called the Parent or Base or Super class. And, the class which inherits properties of other class is called Child or Derived or Sub class.\n" +
                "\n" +
                "Inheritance makes the code reusable. When we inherit an existing class, all its methods and fields become available in the new class, hence code is reused.\n" +
                "\n" +
                "NOTE: All members of a class except Private, are inherited");

        content2.setText("1. Code Reusability\n" +
                "2. Method Overriding (Hence, Runtime Polymorphism.)\n" +
                "3. Use of Virtual Keyword");

        content3.setText("#include <iostream>\n" +
                "#include <string>\n" +
                "using namespace std;\n" +
                "\n" +
                "// Base class\n" +
                "class Vehicle {\n" +
                "  public: \n" +
                "    string brand = \"Ford\";\n" +
                "    void honk() {\n" +
                "      cout << \"Tuut, tuut! \\n\" ;\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "// Derived class\n" +
                "class Car: public Vehicle {\n" +
                "  public: \n" +
                "    string model = \"Mustang\";\n" +
                "};\n" +
                "\n" +
                "int main() {\n" +
                "  Car myCar;\n" +
                "  myCar.honk();\n" +
                "  cout << myCar.brand + \" \" + myCar.model;\n" +
                "  return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Tuut, tuut!\n" +
                "Ford Mustang");

    }
}