package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP15 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);
        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "void swap(int a, int b)\n" +
                "{\n" +
                "    int temp;\n" +
                "    temp = a;\n" +
                "    a = b;\n" +
                "    b = temp;\n" +
                "}\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    int a = 100, b = 200;\n" +
                "    clrscr();\n" +
                "    swap(a, b);  // passing value to function\n" +
                "    cout<<\"Value of a\"<<a;\n" +
                "    cout<<\"Value of b\"<<b;\n" +
                "    getch();\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Value of a: 100\n" +
                "Value of b: 200\n" +
                "\n");

    }
}

