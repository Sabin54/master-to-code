package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP1 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);
        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<iostream.h>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    float n1, n2, n3;\n" +
                "    cout << \"Enter three numbers: \";\n" +
                "    cin >> n1 >> n2 >> n3;\n" +
                "    if (n1 >= n2 && n1 >= n3)\n" +
                "    {\n" +
                "        cout << \"Largest number: \" << n1;\n" +
                "    }\n" +
                "    if(n2 >= n1 && n2 >= n3)\n" +
                "    {\n" +
                "        cout << \"Largest number: \" << n2;\n" +
                "    }\n" +
                "    if(n3 >= n1 && n3 >= n2)\n" +
                "    {\n" +
                "        cout << \"Largest number: \" << n3;\n" +
                "    }\n" +
                "    getch();\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter three numbers: 34 45 56\n" +
                "Largest number: 56");

    }
}