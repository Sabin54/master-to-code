package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp16 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int num, i;\n" +
                "    printf(\"Enter a positive integer: \");\n" +
                "    scanf(\"%d\", &num);\n" +
                "    printf(\"Factors of %d are: \", num);\n" +
                "    for (i = 1; i <= num; ++i) {\n" +
                "        if (num % i == 0) {\n" +
                "            printf(\"%d \", i);\n" +
                "        }\n" +
                "    }\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a positive integer: 60\n" +
                "Factors of 60 are: 1 2 3 4 5 6 10 12 15 20 30 60");

    }
}
