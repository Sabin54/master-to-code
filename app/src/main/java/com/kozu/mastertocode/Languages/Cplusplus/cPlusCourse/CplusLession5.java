package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession5 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    CardView cv4,cv3;

    TextView title1, title2, title3, title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        cv3.setVisibility(View.GONE);
        cv4.setVisibility(View.GONE);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Classes");
        title2.setText("2. Objects");

        content1.setText("The classes are the most important feature of C++ that leads to Object Oriented programming. Class is a user defined data type, which holds its own data members and member functions, which can be accessed and used by creating instance of that class.\n" +
                "\n" +
                "The variables inside class definition are called as data members and the functions are called member functions.\n" +
                "\n" +
                "Class is just a blue print, which declares and defines characteristics and behavior, namely data members and member functions respectively. And all objects of this class will share these characteristics and behavior.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "class Car {\n" +
                "  public:\n" +
                "    string brand;   \n" +
                "    string model;\n" +
                "    int year;\n" +
                "};\n" +
                "\n");

        content2.setText("Class is mere a blueprint or a template. No storage is assigned when we define a class. Objects are instances of class, which holds the data variables declared in class and the member functions work on these class objects.\n" +
                "\n" +
                "Each object has different data variables. Objects are initialised using special class functions called Constructors. We will study about constructors later.\n" +
                "\n" +
                "And whenever the object is out of its scope, another special class member function called Destructor is called, to release the memory reserved by the object. C++ doesn't have Automatic Garbage Collector like in JAVA, in C++ Destructor performs this task.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "   class Car {\n" +
                "     public:\n" +
                "       string brand;   \n" +
                "       string model;\n" +
                "       int year;\n" +
                "   };\n" +
                "\n" +
                "   int main() \n" +
                "   {\n" +
                "        // Create an object of Car\n" +
                "        Car carObj1;\n" +
                "        carObj1.brand = \"BMW\";\n" +
                "        carObj1.model = \"X5\";\n" +
                "        carObj1.year = 1999;\n" +
                "      \n" +
                "      cout << carObj1.brand << \" \" << carObj1.model << \" \" << carObj1.year << \"\\n\";\n" +
                "\n" +
                "      return 0;\n" +
                "   }\n" +
                "\n" +
                "Output: \n" +
                "\n" +
                "   BMW X5 1999\n" +
                "\n");
    }
}
