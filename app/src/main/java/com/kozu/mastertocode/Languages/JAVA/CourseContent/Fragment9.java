package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment9 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5, content6;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag9);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("Threads are implemented in the form of objects that contain a method called run().\n" +
                "The run() method is the heart and soul of any thread.\n" +
                "\n" +
                "\tpublic void run()\n" +
                "\t{\n" +
                "\t/* Your implementation of thethread here*/\n" +
                "\t}\n" +
                "\t\n" +
                "The run() method should be invoked by an object of the concerned thread.This can be achieved by creating the thread and initiating it with the help of another thread method called start().\n" +
                "\n" +
                "A new thread can be created in two ways:\n" +
                "\n" +
                "1. By creating a thread class:\n" +
                "\t\n" +
                "\tDefine a class that extends Thread class and override its run() method with the code required by the thread.\n" +
                "\n" +
                "2. By converting a class to a thread:\n" +
                "\t\n" +
                "\tDefine a class that implements Runnable interface.The runnable interface has only one method, run(), that is to be defined in the method with the code to be executed by the thread.");

        content2.setText("Stopping a thread:\n" +
                "Whenever we want to stop a thread from running further,we may do so by calling its stop() method, like:\n" +
                "\t\n" +
                "\taThread.stop();\n" +
                "\n" +
                "This statement cause the thread to move to the dead state.The stop method may used when the premature death of a thread is desired.\n" +
                "Blocking a thread:\n" +
                "\n" +
                "A thread can also be temporarily suspended or blocked from entering into the runnable and subsequently running state by using either of the following thread methods:\n" +
                "\n" +
                "sleep() \n" +
                "\t//blocked for a specified time\n" +
                "\n" +
                "suspend() \n" +
                "\t//blocked until further orders\n" +
                "\n" +
                "wait() \n" +
                "\t//blocked until certain condition occurs\n" +
                "\n" +
                "These methods cause the thread to go into the blocked(or not-runnable) state.\n");

        content3.setText("The life cycle of the thread in java is controlled by JVM. \n" +
                "A thread can be in one of the five states.\n" +
                "\n" +
                "\t\tNew\n" +
                "\t\tRunnable\n" +
                "\t\tRunning\n" +
                "\t\tNon-Runnable (Blocked)\n" +
                "\t\tTerminated\n" +
                "\n" +
                "1) New\n" +
                "\tThe thread is in new state if you create an instance of Thread class but before the invocation of start() method. \n" +
                "\n" +
                "2) Runnable\n" +
                "\tThe thread is in runnable state after invocation of start() method, but the thread scheduler has not selected it to be the running thread. \n" +
                "\n" +
                "3) Running\n" +
                "\tThe thread is in running state if the thread scheduler has selected it. \n" +
                "\n" +
                "4) Non-Runnable (Blocked)\n" +
                "\tThis is the state when the thread is still alive, but is currently waiting for other thread to finish using thread join or it can be waiting for some resources to available\n" +
                "\n" +
                "5) Terminated\n" +
                "\tA thread is in terminated or dead state when its run() method exits.");


        content4.setText("Exception:\n" +
                "\n" +
                "\tIllegalArgumentException\n" +
                "\tif the priority is not in the range MIN_PRIORITY to MAX_PRIORITY.\n" +
                "\tSecurityException \n" +
                "\tif the current thread cannot modify this thread.\n" +
                "\n" +
                "Priority of a Thread (Thread Priority):\n" +
                "\tJava permits us to set the priority of a thread using the setPriority() method as follows:\n" +
                "\tThreadName.setPriority(intNumber);\n" +
                "\tEach thread have a priority. Priorities are represented by a number between 1 and 10. In most cases, thread schedular schedules the threads according to their priority (known as preemptive scheduling). But it is not guaranteed because it depends on JVM specification that which scheduling it chooses.\n" +
                "\n" +
                "3 constants defiend in Thread class:\n" +
                "\n" +
                "\t\tpublic static int MIN_PRIORITY\n" +
                "\t\tpublic static int NORM_PRIORITY\n" +
                "\t\tpublic static int MAX_PRIORITY\n" +
                "\n" +
                "Default priority of a thread is 5 (NORM_PRIORITY). \n" +
                "\n" +
                "\tThe value of MIN_PRIORITY is 1.\n" +
                "\tThe value of MAX_PRIORITY is 10.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "class TestPriority extends Thread{ \n" +
                "\tpublic void run(){ \n" +
                "\n" +
                "\t\tSystem.out.println(\"running thread name is:\"+Thread.currentThread().getName());\n" +
                "\t\tSystem.out.println(\"running thread priority is:\"+Thread.currentThread().getPriority()); \n" +
                "\t} \n" +
                "\tpublic static void main(String args[]){ \n" +
                "\n" +
                "\t\tTestPriority1 m1=new TestPriority1(); \n" +
                "\t\tTestPriority1 m2=new TestPriority1(); \n" +
                "\t\tm1.setPriority(Thread.MIN_PRIORITY); \n" +
                "\t\tm2.setPriority(Thread.MAX_PRIORITY); \n" +
                "\t\tm1.start(); \n" +
                "\t\tm2.start(); \n" +
                "\t} \n" +
                "} \n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "\trunning thread name is:Thread-0\n" +
                "\trunning thread priority is:10\n" +
                "\trunning thread name is:Thread-1\n" +
                "\trunning thread priority is:1");

        content5.setText("Thread synchronization is the concurrent execution of two or more threads that share critical resources. \n" +
                "Threads should be synchronized to avoid critical resource use conflicts. Otherwise, conflicts may arise when parallel-running threads attempt to modify a common variable at the same time.\n" +
                "\n" +
                "if multiple threads try to write within a same file then they may corrupt the data because one of the threads can overrite data or while one thread is opening the same file at the same time another thread might be closing the same file.\n" +
                "\n" +
                "So there is a need to synchronize the action of multiple threads and make sure that only one thread can access the resource at a given point in time. This is implemented using a concept called monitors. \n" +
                "\n" +
                "Each object in Java is associated with a monitor, which a thread can lock or unlock. Only one thread at a time may hold a lock on a monitor.\n" +
                "Java programming language provides a very handy way of creating threads and synchronizing their task by using synchronized blocks. You keep shared resources within this block. \n" +
                "\n" +
                "Syntax:\n" +
                "\tsynchronized (object reference expression) \n" +
                "\t{ \n" +
                "\t//code block\n" +
                "\t}");

        content6.setText("A Thread can be created by extending Thread class also. But Java allows only one class to extend, it wont allow multiple inheritance. So it is always better to create a thread by implementing Runnable interface. Java allows you to impliment multiple interfaces at a time.\n" +
                "\n" +
                "By implementing Runnable interface, you need to provide implementation for run() method.\n" +
                "\n" +
                "To run this implementation class, create a Thread object, pass Runnable implementation class object to its constructor. Call start() method on thread class to start executing run() method.\n" +
                "\n" +
                "Implementing Runnable interface does not create a Thread object, it only defines an entry point for threads in your object. It allows you to pass the object to the Thread(Runnable implementation) constructor.\n" +
                "\n" +
                "To create a thread using Runnable, a class must implement Java Runnable interface.\n" +
                "\n" +
                "For example of Thread using runnable, reffer \"Thread Example\" program from programs section of this app");
    }
}
