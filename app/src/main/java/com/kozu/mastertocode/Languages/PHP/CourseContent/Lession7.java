package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession7 extends AppCompatActivity {
    TextView content1, content2, content3;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag7);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);


        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("There are two ways to create indexed arrays:\n" +
                "\n" +
                "The index can be assigned automatically (index always starts at 0), like this:\n" +
                "\n" +
                "    $cars = array(\"Volvo\", \"BMW\", \"Toyota\");\n" +
                "\n" +
                "or the index can be assigned manually:\n" +
                "\n" +
                "    $cars[0] = \"Volvo\";\n" +
                "    $cars[1] = \"BMW\";\n" +
                "    $cars[2] = \"Toyota\";\n" +
                "\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    <?php\n" +
                "    $cars = array(\"Volvo\", \"BMW\", \"Toyota\");\n" +
                "    echo \"I like \" . $cars[0] . \", \" . $cars[1] . \" and \" . $cars[2] . \".\";\n" +
                "    ?>");

        content2.setText("Associative arrays are arrays that use named keys that you assign to them.\n" +
                "\n" +
                "There are two ways to create an associative array: \n" +
                "\n" +
                "    $age = array(\"Peter\"=>\"35\", \"Ben\"=>\"37\", \"Joe\"=>\"43\");\n" +
                "\n" +
                "or:\n" +
                "\n" +
                "    $age['Peter'] = \"35\";\n" +
                "    $age['Ben'] = \"37\";\n" +
                "    $age['Joe'] = \"43\";\n" +
                "\n" +
                "\n" +
                "Example\n" +
                "    <?php\n" +
                "    $age = array(\"Peter\"=>\"35\", \"Ben\"=>\"37\", \"Joe\"=>\"43\");\n" +
                "    echo \"Peter is \" . $age['Peter'] . \" years old.\";\n" +
                "    ?>\n");

        content3.setText("A multidimensional array is an array containing one or more arrays.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    $cars = array (\n" +
                "      array(\"Volvo\",22,18),\n" +
                "      array(\"BMW\",15,13),\n" +
                "      array(\"Saab\",5,2),\n" +
                "      array(\"Land Rover\",17,15)\n" +
                "    );\n" +
                "\n" +
                "    <?php\n" +
                "    echo $cars[0][0].\": In stock: \".$cars[0][1].\", sold: \".$cars[0][2].\".<br>\";\n" +
                "    echo $cars[1][0].\": In stock: \".$cars[1][1].\", sold: \".$cars[1][2].\".<br>\";\n" +
                "    echo $cars[2][0].\": In stock: \".$cars[2][1].\", sold: \".$cars[2][2].\".<br>\";\n" +
                "    echo $cars[3][0].\": In stock: \".$cars[3][1].\", sold: \".$cars[3][2].\".<br>\";\n" +
                "    ?>");


    }

}
