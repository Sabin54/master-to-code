package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession7 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    CardView cv4, cv3;

    TextView title1, title2, title3, title4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        cv3.setVisibility(View.GONE);
        cv4.setVisibility(View.GONE);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("1. Introduction");
        title2.setText("2. Example");

        content1.setText("Polymorphism means \"many forms\", and it occurs when we have many classes that are related to each other by inheritance.\n" +
                "\n" +
                "Like we specified in the previous chapter; Inheritance lets us inherit attributes and methods from another class. Polymorphism uses those methods to perform different tasks. This allows us to perform a single action in different ways.");

        content2.setText("#include <iostream>\n" +
                "#include <string>\n" +
                "using namespace std;\n" +
                "\n" +
                "// Base class\n" +
                "class Animal {\n" +
                "  public:\n" +
                "    void animalSound() {\n" +
                "      cout << \"The animal makes a sound \\n\" ;\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "// Derived class\n" +
                "class Pig : public Animal {\n" +
                "  public:\n" +
                "    void animalSound() {\n" +
                "      cout << \"The pig says: wee wee \\n\" ;\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "// Derived class\n" +
                "class Dog : public Animal {\n" +
                "  public:\n" +
                "    void animalSound() {\n" +
                "      cout << \"The dog says: bow wow \\n\" ;\n" +
                "    }\n" +
                "};\n" +
                "\n" +
                "int main() {\n" +
                "  Animal myAnimal;\n" +
                "  Pig myPig;\n" +
                "  Dog myDog;\n" +
                "\n" +
                "  myAnimal.animalSound();\n" +
                "  myPig.animalSound();\n" +
                "  myDog.animalSound();\n" +
                "  return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "The animal makes a sound\n" +
                "The pig says: wee wee\n" +
                "The dog says: bow wow");
    }
}
