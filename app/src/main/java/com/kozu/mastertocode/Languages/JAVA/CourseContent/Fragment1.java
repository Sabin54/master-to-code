package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment1 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("Java is a simple and yet powerful object oriented programming language and it is in many respects similar to C++.\n" +
                "\n" +
                "Java is created by James Gosling from Sun Microsystems (Sun) in 1991. The first publicly available version of Java (Java 1.0) was released in 1995.\n" +
                "\n" +
                "Java is defined by a specification and consists of a programming language, a compiler, core libraries and a runtime machine(Java virtual machine).\n" +
                "The Java runtime allows software developers to write program code in other languages than the Java programming language which still runs on the Java virtual machine. \n" +
                "The Java platform is usually associated with the Java virtual machine and the Java core libraries.\n" +
                "\n" +
                "Java virtual machine:\n" +
                "The Java virtual machine (JVM) is a software implementation of a computer that executes programs like a real machine. \n" +
                "Java Runtime Environment vs. \n" +
                "\n" +
                "Java Development Kit:\n" +
                "A Java distribution typically comes in two flavors, the Java Runtime Environment (JRE) and the Java Development Kit (JDK).\n" +
                "The JRE consists of the JVM and the Java class libraries. Those contain the necessary functionality to start Java programs.\n" +
                "The JDK additionally contains the development tools necessary to create Java programs. The JDK therefore consists of a Java compiler, the Java virtual machine and the Java class libraries.\n" +
                "\n" +
                "Uses of JAVA:\n" +
                "Java is also used as the programming language for many different software programs, games, and add-ons. \n" +
                "Some examples of the more widely used programs written in Java or that use Java include the Android apps, Big Data Technologies, Adobe Creative suite, Eclipse, Lotus Notes, Minecraft, OpenOffice, Runescape, and Vuze.");


        content2.setText("1. Simple\n" +
                "Java is easy to learn and its syntax is quite simple and easy to understand.\n" +
                "\n" +
                "2. Object-Oriented\n" +
                "In java everything is Object which has some data and behaviour. Java can be easily extended as it is based on Object Model.\n" +
                "\n" +
                "3. Platform independent\n" +
                "Unlike other programming languages such as C, C++ etc which are compiled into platform specific machines. Java is guaranteed to be write-once, run-anywhere language.\n" +
                "On compilation Java program is compiled into bytecode. This bytecode is platform independent and can be run on any machine, plus this bytecode format also provide security. Any machine with Java Runtime Environment can run Java Programs.\n" +
                "\n" +
                "4. Secured\n" +
                "When it comes to security, Java is always the first choice. With java secure features it enable us to develop virus free, temper free system. Java program always runs in Java runtime environment with almost null interaction with system OS, hence it is more secure.\n" +
                "\n" +
                "5. Robust\n" +
                "Java makes an effort to eliminate error prone codes by emphasizing mainly on compile time error checking and runtime checking. But the main areas which Java improved were Memory Management and mishandled Exceptions by introducing automatic Garbage Collector and Exception Handling.\n" +
                "\n" +
                "6. Architecture neutral\n" +
                "Compiler generates bytecodes, which have nothing to do with a particular computer architecture, hence a Java program is easy to intrepret on any machine.\n" +
                "\n" +
                "7. Portable\n" +
                "Java Bytecode can be carried to any platform. No implementation dependent features. Everything related to storage is predefined, example: size of primitive data types \n" +
                "\n" +
                "8. High Performance\n" +
                "Java is an interpreted language, so it will never be as fast as a compiled language like C or C++. But, Java enables high performance with the use of just-in-time compiler.\n" +
                "\n" +
                "9. Multithreaded\n" +
                "Java multithreading feature makes it possible to write program that can do many tasks simultaneously. Benefit of multithreading is that it utilizes same memory and other resources to execute multiple threads at the same time, like While typing, grammatical errors are checked along.\n" +
                "\n" +
                "10. Distributed\n" +
                "We can create distributed applications in java. RMI and EJB are used for creating distributed applications. We may access files by calling the methods from any machine on the internet.\n" +
                "\n" +
                "11. Interpreted\n" +
                "An interpreter is needed in order to run Java programs. The programs are compiled into Java Virtual Machine code called bytecode. The bytecode is machine independent and is able to run on any machine that has a Java interpreter. With Java, the program need only be compiled once, and the bytecode generated by the Java compiler can run on any platform.\n" +
                "\n");



        content3.setText("Pros :\n" +
                "1. Java is Simple\n" +
                "\n" +
                "2. Java is object-oriented because programming in Java is centered on creating objects, manipulating objects, and making objects work together. This allows you to create modular programs and reusable code.\n" +
                "\n" +
                "3. One of the most significant advantages of Java is Platform indenpendence.\n" +
                "\n" +
                "4. Java is Secure: Java is one of the first programming languages to consider security as part of its design. \n" +
                "\n" +
                "5. Java is Multithreaded: Multithreaded is the capability for a program to perform several tasks simultaneously within a program.\n" +
                "\n" +
                "6. Java is Robust: Robust means reliable and no programming language can really assure reliability.\n" +
                "\n" +
                "Cons:\n" +
                "1. Java can be perceived as significantly slower and more memory-consuming than natively compiled languages such as C or C++.\n" +
                "\n" +
                "2. No local constants. In Java, variables that belong to a class can be made constant by declaring them to be final. Variables that are local to a method cannot be declared final, however.\n" +
                "\n" +
                "3. Java is predominantly a single-paradigm language. However, with the addition of static imports in Java 5.0 the procedural paradigm is better accommodated than in earlier versions of Java.\n" +
                "Aa");


        content4.setText(
                "public class HelloWorld {\n" +
                        "\t public static void main(String[] args){ \n" +
                        "\t System.out.println(\"Hello World.\"); \n" +
                        " \t } \n" +
                        "}\n");


    }

}
