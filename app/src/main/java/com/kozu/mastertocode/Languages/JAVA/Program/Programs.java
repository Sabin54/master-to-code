package com.kozu.mastertocode.Languages.JAVA.Program;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.kozu.mastertocode.R;

public class Programs extends AppCompatActivity {

    CardView t1 , t2, t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13
            ,t14 , t15, t16,t17,t18,t19,t20,t21,t22,t23;
    AdView adView, adView1;
    InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_programs);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7031830486098035/9463900861");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        t3 = findViewById(R.id.t3);
        t4 = findViewById(R.id.t4);
        t5 = findViewById(R.id.t5);
        t6 = findViewById(R.id.t6);
        t7 = findViewById(R.id.t7);
        t8 = findViewById(R.id.t8);
        t9 = findViewById(R.id.t9);
        t10 = findViewById(R.id.t10);
        t11 = findViewById(R.id.t11);
        t12 = findViewById(R.id.t12);
        t13 = findViewById(R.id.t13);
        t14 = findViewById(R.id.t14);
        t15 = findViewById(R.id.t15);
        t16 = findViewById(R.id.t16);
        t17 = findViewById(R.id.t17);
        t18 = findViewById(R.id.t18);
        t19 = findViewById(R.id.t19);
        t20 = findViewById(R.id.t20);
        t21 = findViewById(R.id.t21);
        t22 = findViewById(R.id.t22);
        t23 = findViewById(R.id.t23);


        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program1.class);
                startActivity(intent);
            }
        });

        t2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program2.class);
                startActivity(intent);
            }
        });

        t3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program3.class);
                startActivity(intent);
            }
        });

        t4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program4.class);
                startActivity(intent);
            }
        });

        t5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program5.class);
                startActivity(intent);
            }
        });

        t6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program6.class);
                startActivity(intent);
            }
        });
        t7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program7.class);
                startActivity(intent);
            }
        });
        t8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program8.class);
                startActivity(intent);
            }
        });
        t9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(), Program9.class);
                startActivity(intent);
            }
        });
        t10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program10.class);
                startActivity(intent);
            }
        });
        t11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program11.class);
                startActivity(intent);
            }
        });
        t12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program12.class);
                startActivity(intent);
            }
        });
        t13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program13.class);
                startActivity(intent);
            }
        });
        t14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program14.class);
                startActivity(intent);
            }
        });
        t15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program15.class);
                startActivity(intent);
            }
        });
        t16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program16.class);
                startActivity(intent);
            }
        });
        t17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program17.class);
                startActivity(intent);
            }
        });
        t18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program18.class);
                startActivity(intent);
            }
        });
        t19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program19.class);
                startActivity(intent);
            }
        });
        t20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program20.class);
                startActivity(intent);
            }
        });
        t21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program21.class);
                startActivity(intent);
            }
        });
        t22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program22.class);
                startActivity(intent);
            }
        });
        t23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInterstitialAd.show();
                Intent intent = new Intent(getApplicationContext(),Program23.class);
                startActivity(intent);
            }
        });


    }

}
