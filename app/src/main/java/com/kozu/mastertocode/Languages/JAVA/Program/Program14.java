package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program14 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "public class Demo {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "\n" +
                "        int num, count, total = 0;\n" +
                "\n" +
                "        \n" +
                "        System.out.println(\"Enter the value of n:\");\n" +
                "        //Scanner is used for reading user input\n" +
                "        Scanner scan = new Scanner(System.in);\n" +
                "        //nextInt() method reads integer entered by user\n" +
                "        num = scan.nextInt();\n" +
                "        //closing scanner after use\n" +
                "        scan.close();\n" +
                "        for(count = 1; count <= num; count++){\n" +
                "            total = total + count;\n" +
                "        }\n" +
                "\n" +
                "        System.out.println(\"Sum of first \"+num+\" natural numbers is: \"+total);\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "Enter the value of n:\n" +
                "20\n" +
                "Sum of first 20 natural numbers is: 210\n");
    }
}
