package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program9 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("public class Example\n" +
                "{\n" +
                "   public void reverseWordInMyString(String str)\n" +
                "   {\n" +
                "  /* The split() method of String class splits\n" +
                "   * a string in several strings based on the\n" +
                "   * delimiter passed as an argument to it\n" +
                "   */\n" +
                "  String[] words = str.split(\" \");\n" +
                "  String reversedString = \"\";\n" +
                "  for (int i = 0; i < words.length; i++)\n" +
                "        {\n" +
                "           String word = words[i]; \n" +
                "           String reverseWord = \"\";\n" +
                "           for (int j = word.length()-1; j >= 0; j--) \n" +
                "     {\n" +
                "    /* The charAt() function returns the character\n" +
                "     * at the given position in a string\n" +
                "     */\n" +
                "    reverseWord = reverseWord + word.charAt(j);\n" +
                "     }\n" +
                "     reversedString = reversedString + reverseWord + \" \";\n" +
                "  }\n" +
                "  System.out.println(str);\n" +
                "  System.out.println(reversedString);\n" +
                "   }\n" +
                "   public static void main(String[] args) \n" +
                "   {\n" +
                "  Example obj = new Example();\n" +
                "  obj.reverseWordInMyString(\"This is an easy Java Program\");\n" +
                "   }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "This is an easy Java Program\n" +
                "sihT si na ysae avaJ margorP");
    }
}
