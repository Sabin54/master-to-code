package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram8 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\t$cars = array(\"Volvo\", \"BMW\", \"Toyota\");\n" +
                "\t\tsort($cars);\n" +
                "\n" +
                "\t\t$clength = count($cars);\n" +
                "\t\tfor($x = 0; $x < $clength; $x++) {\n" +
                "\t\t  echo $cars[$x];\n" +
                "\t\t  echo \"<br>\";\n" +
                "\t\t}\n" +
                "\t\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "BMW\n" +
                "Toyota\n" +
                "Volvo");

    }
}
