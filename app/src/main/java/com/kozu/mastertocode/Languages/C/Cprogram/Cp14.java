package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp14 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int base, exp;\n" +
                "    long long result = 1;\n" +
                "    printf(\"Enter a base number: \");\n" +
                "    scanf(\"%d\", &base);\n" +
                "    printf(\"Enter an exponent: \");\n" +
                "    scanf(\"%d\", &exp);\n" +
                "\n" +
                "    while (exp != 0) {\n" +
                "        result *= base;\n" +
                "        --exp;\n" +
                "    }\n" +
                "    printf(\"Answer = %lld\", result);\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a base number: 3\n" +
                "Enter an exponent: 4\n" +
                "Answer = 81");

    }
}
