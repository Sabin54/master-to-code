package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP11 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);
        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "   int a,b,x;\n" +
                "\n" +
                "   cout<<\"Enter a and b:\\n\";\n" +
                "   cin>>a>>b;\n" +
                "   cout<<\"Before swapping\"<<endl<<\"Value of a= \"<<a<<endl<<\"Value of b= \"<<b<<endl;\n" +
                "   {\n" +
                "      x=a;\n" +
                "      a=b;\n" +
                "      b=x;\n" +
                "      cout<<\"After swapping\"<<endl<<\"Value of a= \"<<a<<endl<<\"Value of b= \"<<b<<endl;\n" +
                "   }\n" +
                "getch();\n" +
                "return 0;\n" +
                "}\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "Enter a and b:\n" +
                "23 33\n" +
                "Before swapping\n" +
                "Value of a= 23\n" +
                "Value of b= 33\n" +
                "\n" +
                "After swapping\n" +
                "Value of a= 33\n" +
                "Value of b= 23");

    }
}
