package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program8 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "public class JavaExample\n" +
                "{\n" +
                "    public static void main(String[] args) \n" +
                "    {\n" +
                "        int count;\n" +
                "        String temp;\n" +
                "        Scanner scan = new Scanner(System.in);\n" +
                "        \n" +
                "        //User will be asked to enter the count of strings \n" +
                "        System.out.print(\"Enter number of strings you would like to enter:\");\n" +
                "        count = scan.nextInt();\n" +
                "        \n" +
                "        \n" +
                "        String str[] = new String[count];\n" +
                "        Scanner scan2 = new Scanner(System.in);\n" +
                "        \n" +
                "        //User is entering the strings and they are stored in an array\n" +
                "\n" +
                "        System.out.println(\"Enter the Strings one by one:\");\n" +
                "        for(int i = 0; i < count; i++)\n" +
                "        {\n" +
                "            str[i] = scan2.nextLine();\n" +
                "        }\n" +
                "        scan.close();\n" +
                "        scan2.close();\n" +
                "        \n" +
                "        //Sorting the strings\n" +
                "        for (int i = 0; i < count; i++) \n" +
                "        {\n" +
                "            for (int j = i + 1; j < count; j++) { \n" +
                "                if (str[i].compareTo(str[j])>0) \n" +
                "                {\n" +
                "                    temp = str[i];\n" +
                "                    str[i] = str[j];\n" +
                "                    str[j] = temp;\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        //Displaying the strings after sorting them based on alphabetical order\n" +
                "\n" +
                "        System.out.print(\"Strings in Sorted Order:\");\n" +
                "        for (int i = 0; i <= count - 1; i++) \n" +
                "        {\n" +
                "            System.out.print(str[i] + \", \");\n" +
                "        }\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter number of strings you would like to enter:5\n" +
                "Enter the Strings one by one:\n" +
                "Ram\n" +
                "Shyam\n" +
                "Daimond\n" +
                "hari\n" +
                "Ace\n" +
                "platnium\n" +
                "\n" +
                "Strings in Sorted Order: Ace, Daimond, hari, platnium, Ram, Shyam \n" +
                "\n" +
                "\n");
    }
}
