package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP14 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "    int mat1[3][3], mat2[3][3];\n" +
                "    int i, j, k;\n" +
                "    cout<<\"Enter the elements of Matrix(3X3) : \";\n" +
                "    for(i=0;i<3;i++)\n" +
                "    {\n" +
                "        for(j=0;j<3;j++)\n" +
                "        {\n" +
                "            cin>>mat1[i][j];\n" +
                "        }\n" +
                "    }\n" +
                "    cout<<\"\\nMatrix is : \"<<endl;\n" +
                "    for(i=0;i<3;i++)\n" +
                "    {\n" +
                "        for(j=0;j<3;j++)\n" +
                "        {\n" +
                "            cout<<mat1[i][j]<<\" \";\n" +
                "        }\n" +
                "        cout<<endl;\n" +
                "    }\n" +
                "    //Transposing Matrices\n" +
                "    for(i=0;i<3;i++)\n" +
                "    {\n" +
                "        for(j=0;j<3;j++)\n" +
                "        {\n" +
                "            mat2[i][j]=mat1[j][i];\n" +
                "        }\n" +
                "    }\n" +
                "    cout<<\"\\nTransposed matrix is : \"<<endl;\n" +
                "    for(i=0;i<3;i++)\n" +
                "    {\n" +
                "        for(j=0;j<3;j++)\n" +
                "        {\n" +
                "            cout<<mat2[i][j]<<\" \";\n" +
                "        }\n" +
                "        cout<<endl;\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "Enter the elements of Matrix(3x3) : 1\n" +
                "2\n" +
                "3\n" +
                "4\n" +
                "5\n" +
                "6\n" +
                "7\n" +
                "8\n" +
                "9\n" +
                "Matrix is :\n" +
                "1 2 3\n" +
                "4 5 6\n" +
                "7 8 9\n" +
                "Transposed Matrix is :\n" +
                "1 4 7\n" +
                "2 5 8\n" +
                "3 6 9\n" +
                "\n");
    }
}
