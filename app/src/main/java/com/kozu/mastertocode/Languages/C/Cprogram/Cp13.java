package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp13 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    long long n;\n" +
                "    int count = 0;\n" +
                "    printf(\"Enter an integer: \");\n" +
                "    scanf(\"%lld\", &n);\n" +
                " \n" +
                "    // iterate until n becomes 0\n" +
                "    // remove last digit from n in each iteration\n" +
                "    // increase count by 1 in each iteration\n" +
                "    while (n != 0) {\n" +
                "        n /= 10;     // n = n/10\n" +
                "        ++count;\n" +
                "    }\n" +
                "\n" +
                "    printf(\"Number of digits: %d\", count);\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter an integer: 3452\n" +
                "Number of digits: 4");

    }
}
