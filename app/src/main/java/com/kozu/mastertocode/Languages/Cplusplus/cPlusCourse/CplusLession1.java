package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession1 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    TextView title1, title2, title3, title4;

    CardView cv3,cv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);

        cv3 = findViewById(R.id.cv3);
        cv4 = findViewById(R.id.cv4);

        cv4.setVisibility(View.GONE);
        cv3.setVisibility(View.GONE);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        title1.setText("Introduction to C++");
        title2.setText("Hello World");

        content1.setText("C++, as we all know is an extension to C language and was developed by Bjarne stroustrup at bell labs. C++ is an intermediate level language, as it comprises a confirmation of both high level and low level language features. C++ is a statically typed, free form, multiparadigm, compiled general-purpose language.\n" +
                "\n" +
                "C++ is an Object Oriented Programming language but is not purely Object Oriented. Its features like Friend and Virtual, violate some of the very important OOPS features, rendering this language unworthy of being called completely Object Oriented. Its a middle level language.");

        content2.setText("#include <iostream>\n" +
                "using namespace std;\n" +
                "\n" +
                "int main() {\n" +
                "    // prints the string enclosed in double quotes\n" +
                "    cout << \"Hello World\";\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Hello World");

    }
}