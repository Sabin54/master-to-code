package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program10 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "public class Example\n" +
                "{\n" +
                "   public static void main(String args[])\n" +
                "   {\n" +
                "  int counter, i=0, j=0, temp;\n" +
                "  int number[] = new int[100];\n" +
                "  Scanner scanner = new Scanner(System.in);\n" +
                "  System.out.print(\"How many elements you want to enter: \");\n" +
                "  counter = scanner.nextInt();\n" +
                "\n" +
                "    /* This loop stores all the elements that we enter in an \n" +
                "     * the array number. First element is at number[0], second at \n" +
                "     * number[1] and so on\n" +
                "     */\n" +
                "     \n" +
                "  for(i=0; i<counter; i++)\n" +
                "  {\n" +
                "      System.out.print(\"Enter Array Element\"+(i+1)+\": \");\n" +
                "      number[i] = scanner.nextInt();\n" +
                "  }\n" +
                "\n" +
                "    /* Here we are writing the logic to swap first element with\n" +
                "     * last element, second last element with second element and\n" +
                "     * so on. On the first iteration of while loop i is the index \n" +
                "     * of first element and j is the index of last. On the second\n" +
                "     * iteration i is the index of second and j is the index of \n" +
                "     * second last.\n" +
                "     */\n" +
                "\n" +
                "  j = i - 1;     \n" +
                "  i = 0;         \n" +
                "  scanner.close();\n" +
                "  while(i<j)\n" +
                "  {\n" +
                "       temp = number[i];\n" +
                "     number[i] = number[j];\n" +
                "     number[j] = temp;\n" +
                "     i++;\n" +
                "     j--;\n" +
                "  }\n" +
                "\n" +
                "  System.out.print(\"Reversed array: \");\n" +
                "  for(i=0; i<counter; i++)\n" +
                "  {\n" +
                "     System.out.print(number[i]+ \"  \");\n" +
                "  }       \n" +
                "   }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "How many elements you want to enter: 5\n" +
                "Enter Array Element1: 11\n" +
                "Enter Array Element2: 22\n" +
                "Enter Array Element3: 33\n" +
                "Enter Array Element4: 44\n" +
                "Enter Array Element5: 55\n" +
                "Reversed array: 55  44  33  22  11");
    }
}
