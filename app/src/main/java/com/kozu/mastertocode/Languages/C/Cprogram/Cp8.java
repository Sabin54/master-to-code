package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp8 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<stdio.h>\n" +
                "int main() {\n" +
                "      double first, second, temp;\n" +
                "      printf(\"Enter first number: \");\n" +
                "      scanf(\"%lf\", &first);\n" +
                "      printf(\"Enter second number: \");\n" +
                "      scanf(\"%lf\", &second);\n" +
                "\n" +
                "      // Value of first is assigned to temp\n" +
                "      temp = first;\n" +
                "\n" +
                "      // Value of second is assigned to first\n" +
                "      first = second;\n" +
                "\n" +
                "      // Value of temp (initial value of first) is assigned to second\n" +
                "      second = temp;\n" +
                "\n" +
                "      printf(\"\\nAfter swapping, firstNumber = %.2lf\\n\", first);\n" +
                "      printf(\"After swapping, secondNumber = %.2lf\", second);\n" +
                "      return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter first number: 1.20\n" +
                "Enter second number: 2.45\n" +
                "\n" +
                "After swapping, firstNumber = 2.45\n" +
                "After swapping, secondNumber = 1.20");

    }
}
