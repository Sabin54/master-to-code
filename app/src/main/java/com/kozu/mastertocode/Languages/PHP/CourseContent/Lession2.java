package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession2 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag2);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("The PHP superglobals $_GET and $_POST and $_REQUEST are used to collect formdata.");

        content2.setText("- Information sent from a form with the GET method is visible to everyone (all\n" +
                "variable names and values are displayed in the URL).\n" +
                "- GET also has limits on the amount of information to send. The limitation is\n" +
                "about 2000 characters.\n" +
                "- GET may be used for sending non-sensitive data.\n" +
                "- GET should NEVER be used for sending passwords or other sensitive\n" +
                "information!\n" +
                "- $_GET superglobal Array is used to access data.");

        content3.setText("Information sent from a form with the POST method is invisible to others.\n" +
                "- has no limits on the amount of information to send.\n" +
                "- POST method is secure than GET Method.\n" +
                "- POST is used for sensitive data.\n" +
                "- $_POST superglobal Array is used to access data.");

        content4.setText("- $_REQUEST is a super global variable which is widely used to collect data after\n" +
                "submitting html forms.\n" +
                "- It is used to access data sent through get and post method.");

    }
}
