package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program23 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Date;\n" +
                "import java.text.DateFormat;\n" +
                "import java.text.SimpleDateFormat;\n" +
                "import java.util.Calendar;\n" +
                "\n" +
                "public class GettingCurrentDate {\n" +
                "   public static void main(String[] args) {\n" +
                "       //getting current date and time using Date class\n" +
                "       DateFormat df = new SimpleDateFormat(\"dd/MM/yy HH:mm:ss\");\n" +
                "       Date dateobj = new Date();\n" +
                "       System.out.println(df.format(dateobj));\n" +
                "\n" +
                "       /*getting current date time using calendar class \n" +
                "        * An Alternative of above*/\n" +
                "       Calendar calobj = Calendar.getInstance();\n" +
                "       System.out.println(df.format(calobj.getTime()));\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "11/09/20 22:13:06\n" +
                "11/09/20 22:13:06");
    }
}
