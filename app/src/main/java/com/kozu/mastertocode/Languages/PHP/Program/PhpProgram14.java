package com.kozu.mastertocode.Languages.PHP.Program;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram14 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>PHP Get Current Date</title>\n" +
                "</head>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\t// Return current date from the remote server\n" +
                "\t\t$today = date(\"d/m/Y\");\n" +
                "\t\techo $today;\n" +
                "\t\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "13/09/2020\n");

    }
}
