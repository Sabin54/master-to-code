package com.kozu.mastertocode.Languages.C.CprogrammingCourse;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Clession3 extends AppCompatActivity {

    TextView content1, content2, content3, content4,content5;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.c_frag3);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("#include <stdio.h>\n" +
                "int main()\n" +
                "{\n" +
                "    int testInteger;\n" +
                "    printf(\"Enter an integer: \");\n" +
                "    scanf(\"%d\", &testInteger);  \n" +
                "    printf(\"Number = %d\",testInteger);\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output\n" +
                "\n" +
                "Enter an integer: 4\n" +
                "Number = 4\n" +
                "\n" +
                "\n" +
                "Here, we have used %d format specifier inside the scanf() function to take int input from the user. When the user enters an integer, it is stored in the testInteger variable.\n" +
                "\n" +
                "Notice, that we have used &testInteger inside scanf(). It is because &testInteger gets the address of testInteger, and the value entered by the user is stored in that address.");

        content2.setText("#include <stdio.h>\n" +
                "int main()\n" +
                "{\n" +
                "    float num1;\n" +
                "    double num2;\n" +
                "\n" +
                "    printf(\"Enter a number: \");\n" +
                "    scanf(\"%f\", &num1);\n" +
                "    printf(\"Enter another number: \");\n" +
                "    scanf(\"%lf\", &num2);\n" +
                "\n" +
                "    printf(\"num1 = %f\\n\", num1);\n" +
                "    printf(\"num2 = %lf\", num2);\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output\n" +
                "\n" +
                "Enter a number: 12.523\n" +
                "Enter another number: 10.2\n" +
                "num1 = 12.523000\n" +
                "num2 = 10.200000\n" +
                "\n" +
                "\n" +
                "We use %f and %lf format specifier for float and double respectively.");

        content3.setText("#include <stdio.h>\n" +
                "int main()\n" +
                "{\n" +
                "    char chr;\n" +
                "    printf(\"Enter a character: \");\n" +
                "    scanf(\"%c\",&chr);     \n" +
                "    printf(\"You entered %c.\", chr);  \n" +
                "    \n" +
                "}\n" +
                "\n" +
                "Output\n" +
                "\n" +
                "Enter a character: g\n" +
                "You entered g\n" +
                "\n" +
                "\n" +
                "When a character is entered by the user in the above program, the character itself is not stored. Instead, an integer value (ASCII value) is stored.\n" +
                "\n" +
                "And when we display that value using %c text format, the entered character is displayed. If we use %d to display the character, it's ASCII value is printed.");

        content4.setText("#include <stdio.h>\n" +
                "int main()\n" +
                "{\n" +
                "    char chr;\n" +
                "    printf(\"Enter a character: \");\n" +
                "    scanf(\"%c\", &chr);     \n" +
                "\n" +
                "    // When %c is used, a character is displayed\n" +
                "    printf(\"You entered %c.\\n\",chr);  \n" +
                "\n" +
                "    // When %d is used, ASCII value is displayed\n" +
                "    printf(\"ASCII value is %d.\", chr);  \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output\n" +
                "\n" +
                "Enter a character: g\n" +
                "You entered g.\n" +
                "ASCII value is 103.\n" +
                "\n");

        content5.setText("As you can see from the above examples, we use\n" +
                "\n" +
                "%d for int\n" +
                "\n" +
                "%f for float\n" +
                "\n" +
                "%lf for double\n" +
                "\n" +
                "%c for char");
    }
}
