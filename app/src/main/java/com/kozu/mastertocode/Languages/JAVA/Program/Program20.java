package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program20 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.io.*;\n" +
                "class Exception\n" +
                "{\n" +
                "    public static void main(String args[])throws IOException\n" +
                "    {\n" +
                "    int a,b,c;\n" +
                "    a=2;\n" +
                "    b=4;\n" +
                "    c=3;\n" +
                "    double de;\n" +
                "    try\n" +
                "        {\n" +
                "        de.math.sqrt((b*b)-(2*a*c))\n" +
                "            if(de>0)\n" +
                "            {\n" +
                "                System.out.println(\"Delta Is Less Than 0\");\n" +
                "                throw new exception(\"No Root Exception\");\n" +
                "            }\n" +
                "    else\n" +
                "        {\n" +
                "            System.out.print(\"Ans is=\"+de);\n" +
                "        }\n" +
                "    }\n" +
                "        catch(Exception e)\n" +
                "        {\n" +
                "            System.out.println(\"Error=\"+e);\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "}");
    }
}
