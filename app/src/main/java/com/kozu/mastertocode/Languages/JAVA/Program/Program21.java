package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program21 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("class MyThread implements Runnable {\n" +
                "String name;\n" +
                "Thread t;\n" +
                "    MyThread String thread){\n" +
                "    name = threadname; \n" +
                "    t = new Thread(this, name);\n" +
                "System.out.println(\"New thread: \" + t);\n" +
                "t.start();\n" +
                "}\n" +
                "public void run() {\n" +
                " try {\n" +
                "     for(int i = 5; i > 0; i--) {\n" +
                "     System.out.println(name + \": \" + i);\n" +
                "      Thread.sleep(1000);\n" +
                "}\n" +
                "}catch (InterruptedException e) {\n" +
                "     System.out.println(name + \"Interrupted\");\n" +
                "}\n" +
                "     System.out.println(name + \" exiting.\");\n" +
                "}\n" +
                "}\n" +
                "class MultiThread {\n" +
                "public static void main(String args[]) {\n" +
                "     new MyThread(\"One\");\n" +
                "     new MyThread(\"Two\");\n" +
                "     new NewThread(\"Three\");\n" +
                "try {\n" +
                "     Thread.sleep(10000);\n" +
                "} catch (InterruptedException e) {\n" +
                "      System.out.println(\"Main thread Interrupted\");\n" +
                "}\n" +
                "      System.out.println(\"Main thread exiting.\");\n" +
                "      }\n" +
                "}\n" +
                "\n" +
                "The output from this program is shown here:\n" +
                "\n" +
                "New thread: Thread[One,5,main]\n" +
                "New thread: Thread[Two,5,main]\n" +
                "New thread: Thread[Three,5,main]\n" +
                "One: 5\n" +
                "Two: 5\n" +
                "Three: 5\n" +
                "One: 4\n" +
                "Two: 4\n" +
                "Three: 4\n" +
                "One: 3\n" +
                "Three: 3\n" +
                "Two: 3\n" +
                "One: 2\n" +
                "Three: 2\n" +
                "Two: 2\n" +
                "One: 1\n" +
                "Three: 1\n" +
                "Two: 1\n" +
                "One exiting.\n" +
                "Two exiting.\n" +
                "Three exiting.\n" +
                "Main thread exiting.");
    }
}
