package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp17 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <math.h>\n" +
                "#include <stdio.h>\n" +
                "\n" +
                "int checkPrimeNumber(int n);\n" +
                "int checkArmstrongNumber(int n);\n" +
                "\n" +
                "int main() {\n" +
                "   int n, flag;\n" +
                "   printf(\"Enter a positive integer: \");\n" +
                "   scanf(\"%d\", &n);\n" +
                "\n" +
                "   // check prime number\n" +
                "   flag = checkPrimeNumber(n);\n" +
                "   if (flag == 1)\n" +
                "      printf(\"%d is a prime number.\\n\", n);\n" +
                "   else\n" +
                "      printf(\"%d is not a prime number.\\n\", n);\n" +
                "\n" +
                "   // check Armstrong number\n" +
                "   flag = checkArmstrongNumber(n);\n" +
                "   if (flag == 1)\n" +
                "      printf(\"%d is an Armstrong number.\", n);\n" +
                "   else\n" +
                "      printf(\"%d is not an Armstrong number.\", n);\n" +
                "   return 0;\n" +
                "}\n" +
                "\n" +
                "// function to check prime number\n" +
                "int checkPrimeNumber(int n) {\n" +
                "   int i, flag = 1, squareRoot;\n" +
                "\n" +
                "   // computing the square root\n" +
                "   squareRoot = sqrt(n);\n" +
                "   for (i = 2; i <= squareRoot; ++i) {\n" +
                "      // condition for non-prime number\n" +
                "      if (n % i == 0) {\n" +
                "         flag = 0;\n" +
                "         break;\n" +
                "      }\n" +
                "   }\n" +
                "   return flag;\n" +
                "}\n" +
                "\n" +
                "// function to check Armstrong number\n" +
                "int checkArmstrongNumber(int num) {\n" +
                "   int originalNum, remainder, n = 0, flag;\n" +
                "   double result = 0.0;\n" +
                "\n" +
                "   // store the number of digits of num in n\n" +
                "   for (originalNum = num; originalNum != 0; ++n) {\n" +
                "      originalNum /= 10;\n" +
                "   }\n" +
                "\n" +
                "   for (originalNum = num; originalNum != 0; originalNum /= 10) {\n" +
                "      remainder = originalNum % 10;\n" +
                "\n" +
                "      // store the sum of the power of individual digits in result\n" +
                "      result += pow(remainder, n);\n" +
                "   }\n" +
                "\n" +
                "   // condition for Armstrong number\n" +
                "   if (round(result) == num)\n" +
                "      flag = 1;\n" +
                "   else\n" +
                "      flag = 0;\n" +
                "   return flag;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a positive integer: 407\n" +
                "407 is not a prime number.\n" +
                "407 is an Armstrong number.");

    }
}
