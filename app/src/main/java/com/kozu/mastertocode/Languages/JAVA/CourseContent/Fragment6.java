package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment6 extends AppCompatActivity {

    TextView content1, content2, content3, content4;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag6);


        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("An interface is a reference type in Java, it is similar to class, it is a collection of abstract methods.\n" +
                "\n" +
                "A class implements an interface, thereby inheriting the abstract methods of the interface. Along with abstract methods an interface may also contain constants, default methods, static methods, and nested types. Method bodies exist only for default methods and static methods.\n" +
                "\n" +
                "Writing an interface is similar to writing a class. But a class describes the attributes and behaviours of an object. And an interface contains behaviours that a class implements.\n" +
                "\n" +
                "Unless the class that implements the interface is abstract, all the methods of the interface need to be defined in the class.\n" +
                "The General form of an interface definition is:\n" +
                "\n" +
                "[visibility] interface InterfaceName [extends other interfaces]\n" +
                "{\n" +
                "variables declarations;\n" +
                "abstract method declarations;\n" +
                "}\n" +
                "\n" +
                "here, interface is the keyword and InterfaceName is any valid java variable(just like class name).\n" +
                "variables are declared as follows:\n" +
                "static final tyoe VariableName=Value; \n" +
                "\n" +
                "example:\n" +
                "interface printable{\n" +
                "void print(); \n" +
                "} \n" +
                "class A6 implements printable{ \n" +
                "public void print(){System.out.println(\"Hello\");} \n" +
                "\tpublic static void main(String args[]){ \n" +
                "\t\tA6 obj = new A6(); \n" +
                "\t\tobj.print(); \n" +
                "\t} \n" +
                "}\n" +
                "\n" +
                "Output: Hello");


        content2.setText("An interface can extend another interface, similarly to the way that a class can extend another class. \n" +
                "\n" +
                "The extends keyword is used to extend an interface, and the child interface inherits the methods of the parent interface.\n" +
                "this is achieved using the keyword extends as shown below:\n" +
                "\n" +
                "public class C extends A implements B {\n" +
                "//trying to override doSomthing...\n" +
                "\tpublic int myMethod(int x)\n" +
                "\t{\n" +
                "\treturn doSomthingElse(x);\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "interface ItemConstants\n" +
                "\t{\n" +
                "\tint code=2015;\n" +
                "\tstring name=\"Akshay\";\n" +
                "\t}\n" +
                "interface Item extends ItemConstants\n" +
                "\t{\n" +
                "\tvoid display();\n" +
                "\t}");

        content3.setText("A class uses the implements keyword to implement an interface. The implements keyword appears in the class declaration following the extends portion of the declaration.\n" +
                "\n" +
                "class classname implements interfacename\n" +
                "\t{\n" +
                "\tbody of classname\n" +
                "\t}\n" +
                "\there the classname \"implements\" the interface interfacename. A more general form of implementation may look like this:\n" +
                "\tclass classname extends superclass implements interface1,interface2,...\n" +
                "\t{\n" +
                "\tbody of classname\n" +
                "\t}\n" +
                "\t\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "interface MyInterface\n" +
                "\t{\n" +
                "\tpublic void method1();\n" +
                "\tpublic void method2();\n" +
                "\t\t}\n" +
                "\t\tclass XYZ implements MyInterface\n" +
                "\t\t{\n" +
                "\tpublic void method1()\n" +
                "\t\t{\n" +
                "\t\tSystem.out.println(\"implementation of method1\");\n" +
                "\t\t}\n" +
                "\tpublic void method2()\n" +
                "\t\t{\n" +
                "\t\tSystem.out.println(\"implementation of method2\");\n" +
                "\t\t}\n" +
                "\tpublic static void main(String arg[])\n" +
                "\t\t{\n" +
                "\t\tMyInterface obj = new XYZ();\n" +
                "\t\tobj. method1();\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "Output:\n" +
                "implementation of method1");


        content4.setText("Interfaces can be used to declare a set of constants that can be used in different classes. \n" +
                "This is similar to creating header files in c++ to contain large number of constants.\n" +
                "Since such interfaces do not contain methods,there is no need to worry about implementing any methods. \n" +
                "The constant value will be available to any class that implements the interface.\n" +
                "The values can be used in any method, as part of any variable declaration, or anywhere where we can use a final value.\n" +
                "\n" +
                "Example:\n" +
                "interface A \n" +
                "\t{\n" +
                "\tint m=10;\n" +
                "\tint n=50;\n" +
                "\t}\n" +
                "class B implements A\n" +
                "\t{ \n" +
                "\tint x=m;\n" +
                "\tvoid methodB(int size)\n" +
                "\t\t{\n" +
                "\t\t...\n" +
                "\t\t...\n" +
                "\t\tif(size<n)\n" +
                "\t\t}\n" +
                "\t}");

    }
}
