package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP5 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<iostream.h>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    int i, n1, factorial=1;\n" +
                "    clrscr() ;\n" +
                "    cout<<\"Enter the number to find it's factorial : \" ;\n" +
                "    cin>>n1 ;\n" +
                "    for(a=1;a<=n1;a++)\n" +
                "    {\n" +
                "        factorial*=a;\n" +
                "    }\n" +
                "    if(n1==0)\n" +
                "    {\n" +
                "        cout<<\"\\nThe factorial is : 1\";\n" +
                "    }\n" +
                "    else\n" +
                "    {\n" +
                "        cout<<\"\\nThe factorial is : \"<<factorial;\n" +
                "    }\n" +
                "    getch() ;\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter the number to find it's factorial : 3\n" +
                "The factorial is : 6");

    }
}
