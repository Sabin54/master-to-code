package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram13 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\t$email = \"kozu@example.com\";\n" +
                "\n" +
                "\t\t// Remove all illegal characters from email\n" +
                "\t\t$email = filter_var($email, FILTER_SANITIZE_EMAIL);\n" +
                "\n" +
                "\t\t// Validate e-mail\n" +
                "\t\tif (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) \n" +
                "\t\t\t{\n" +
                "\t\t\t  echo(\"$email is a valid email address\");\n" +
                "\t\t\t} \n" +
                "\t\telse \n" +
                "\t\t\t{\n" +
                "\t\t\t  echo(\"$email is not a valid email address\");\n" +
                "\t\t\t}\n" +
                "\t\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "kozu@example.com is a valid email address\n");

    }
}
