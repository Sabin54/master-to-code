package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession1 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag1);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("PHP was introduced by Rasmus Lerdorf in 1994. It was developed to track visitor of\n" +
                "his resume.\n" +
                "When PHP was developed Full form of PHP was “Personal Home Page”");

        content2.setText("A PHP script can be placed anywhere in the document.\n" +
                "A PHP script starts with <?php and ends with ?>\n" +
                "\n" +
                "    <?php\n" +
                "    // PHP code goes here\n" +
                "    ?>");

        content3.setText("A variable can have a short name (like x and y) or a more descriptive name (age,\n" +
                "carname, total_volume).\n" +
                "\n" +
                "Rules for PHP variables:\n" +
                "    - A variable starts with the $ sign, followed by the name of the variable\n" +
                "    - A variable name must start with a letter or the underscore character\n" +
                "    - A variable name cannot start with a number\n" +
                "    - A variable name can only contain alpha-numeric characters and underscores\n" +
                "    (A-z, 0-9, and _ )\n" +
                "    - Variable names are case-sensitive ($age and $AGE are two different variables)\n" +
                "\n" +
                "Example: \n" +
                "\n" +
                "    <?php\n" +
                "    $x = 5;\n" +
                "    $y = 4;\n" +
                "    echo $x + $y;\n" +
                "    ?>\n");

        content4.setText("PHP supports the following data types:\n" +
                "\n" +
                "1. String - A string can be any text inside quotes.\n" +
                "    Ex. $fname = „piyush‟;\n" +
                "     $lname = „Gupta‟;\n" +
                "\n" +
                "2. Integer – negative and positive whole number\n" +
                "\n" +
                "3. Float - Value with decimal point ex. – 0.5, 1.0\n" +
                "\n" +
                "4. Boolean - A Boolean represents two possible states: TRUE or FALSE.\n" +
                "\n" +
                "5. Array - An array stores multiple values in one single variable.\n" +
                "    Ex. $number = array(45, 60, 55, 25);\n" +
                "\n" +
                "6. Object – Instance of class\n" +
                "    Ex. $obj = new mysqli();\n" +
                "\n" +
                "7. NULL – A variable of data type NULL is a variable that has no value assigned to it.\n" +
                "\n" +
                "8. Resource - It is the storing of a reference to functions and resources external to\n" +
                "PHP.\n" +
                "    Ex. – connection to database, mail server");

        content5.setText("A constant is an identifier (name) for a simple value. The value cannot be changed\n" +
                "during the script.\n" +
                "A valid constant name starts with a letter or underscore (no $ sign before the\n" +
                "constant name).\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\tdefine(\"GREETING\", \"Welcome to PHP coding!\");\n" +
                "\techo GREETING;\n" +
                "\t?>");
    }

}