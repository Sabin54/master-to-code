package com.kozu.mastertocode.Languages.PHP.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram12 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<?php\n" +
                "session_start();\n" +
                "?>\n" +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\t// remove all session variables\n" +
                "\t\tsession_unset();\n" +
                "\n" +
                "\t\t// destroy the session\n" +
                "\t\tsession_destroy();\n" +
                "\n" +
                "\t\techo \"All session variables are now removed, and the session is destroyed.\"\n" +
                "\t\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "All session variables are now removed, and the session is destroyed.\n");

    }
}
