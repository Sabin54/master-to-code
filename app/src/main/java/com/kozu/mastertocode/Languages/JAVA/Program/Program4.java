package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program4 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "\n" +
                "class CheckEvenOdd\n" +
                "{\n" +
                "  public static void main(String args[])\n" +
                "  {\n" +
                "    int num;\n" +
                "    System.out.println(\"Enter an Integer number:\");\n" +
                "\n" +
                "    //The input provided by user is stored in num\n" +
                "    Scanner input = new Scanner(System.in);\n" +
                "    num = input.nextInt();\n" +
                "\n" +
                "    /* If number is divisible by 2 then it's an even number\n" +
                "     * else odd number*/\n" +
                "    if ( num % 2 == 0 )\n" +
                "        System.out.println(\"Entered number is even\");\n" +
                "     else\n" +
                "        System.out.println(\"Entered number is odd\");\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "Output 1:\n" +
                "\n" +
                "Enter an Integer number:\n" +
                "78\n" +
                "Entered number is even\n" +
                "\n" +
                "Output 2:\n" +
                "\n" +
                "Enter an Integer number:\n" +
                "77\n" +
                "Entered number is odd");
    }
}
