package com.kozu.mastertocode.Languages.JAVA.Program;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Program16 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("import java.util.Scanner;\n" +
                "public class JavaExample \n" +
                "{\n" +
                "    public static void main(String args[])\n" +
                "    {\n" +
                "        int num1, num2;\n" +
                "        Scanner scanner = new Scanner(System.in);\n" +
                "        System.out.print(\"Enter first number:\");\n" +
                "        num1 = scanner.nextInt();\n" +
                "        System.out.print(\"Enter second number:\");\n" +
                "        num2 = scanner.nextInt();\n" +
                "        /* To make you understand, lets assume I am going\n" +
                "         * to enter value of first number as 10 and second \n" +
                "         * as 5. Binary equivalent of 10 is 1010 and 5 is\n" +
                "         * 0101\n" +
                "         */\n" +
                "        \n" +
                "        //num1 becomes 1111 = 15\n" +
                "        num1 = num1 ^ num2;\n" +
                "        //num2 becomes 1010 = 10\n" +
                "        num2 = num1 ^ num2;\n" +
                "        //num1 becomes 0101 = 5\n" +
                "        num1 = num1 ^ num2;\n" +
                "        scanner.close();\n" +
                "        System.out.println(\"The First number after swapping:\"+num1);\n" +
                "        System.out.println(\"The Second number after swapping:\"+num2);\n" +
                "    }\n" +
                "}\n" +
                "Output:\n" +
                "\n" +
                "\n" +
                "Enter first number:10\n" +
                "Enter second number:5\n" +
                "The First number after swapping:5\n" +
                "The Second number after swapping:10");
    }
}
