package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession5 extends AppCompatActivity {
    TextView content1, content2, content3;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag5);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);


        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);


        content1.setText("A user defined function declaration starts with theword \"function\".\n" +
                "A function name can start with a letter or underscore (not a number).\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    <?php\n" +
                "    function writeMsg() {\n" +
                "      echo \"Hello world!\";\n" +
                "    }\n" +
                "\n" +
                "    writeMsg(); // call the function\n" +
                "    ?>");

        content2.setText("Information can be passed to functions through arguments. Arguments are specified after the function name, inside the parentheses.\n" +
                "You can add as many arguments as you want, just separate them with a comma.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "<?php\n" +
                "function familyName($fname) {\n" +
                "  echo \"$fname Refsnes.<br>\";\n" +
                "}\n" +
                "\n" +
                "familyName(\"Kozu\");\n" +
                "familyName(\"Newar\");\n" +
                "\n" +
                "?>\n");

        content3.setText("To let a function return a value, use the return statement.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    <?php \n" +
                "    function msg() {\n" +
                "      return \"Welcome To Nepal\";\n" +
                "    }\n" +
                "    echo msg;\n" +
                "    ?>");
    }
}
