package com.kozu.mastertocode.Languages.Cplusplus.CplusQuiz;

public class CplusQuestions {
    private String mQuestions[] = {
            "Which of the following is the correct syntax of including a user defined header files in C++? ",
            "Which of the following is a correct identifier in C++? ",
            " Wrapping data and its related functionality into a single entity is known as _____________",
            "Which of the following type is provided by C++ but not C?",
            "The value 132.54 can be represented using which data type?",
            "Which of the following correctly declares an array?",
            " What will be the output of the following C++ code?\n" +
                    "\n" +
                    "    #include <stdio.h>\n" +
                    "    #include<iostream>\n" +
                    "    using namespace std;\n" +
                    "    int array1[] = {1200, 200, 2300, 1230, 1543};\n" +
                    "    int array2[] = {12, 14, 16, 18, 20};\n" +
                    "    int temp, result = 0;\n" +
                    "    int main()\n" +
                    "    {\n" +
                    "        for (temp = 0; temp < 5; temp++) \n" +
                    "        {\n" +
                    "            result += array1[temp];\n" +
                    "        }\n" +
                    "        for (temp = 0; temp < 4; temp++)\n" +
                    "        {\n" +
                    "            result += array2[temp];\n" +
                    "        }\n" +
                    "        cout << result;\n" +
                    "        return 0;\n" +
                    "    }",
            " What will be the output of the following C++ code?\n" +
                    "\n" +
                    "    #include <stdio.h>\n" +
                    "    #include <iostream>\n" +
                    "    using namespace std;\n" +
                    "    int main()\n" +
                    "    {\n" +
                    "        char str[5] = \"ABC\";\n" +
                    "        cout << str[3];\n" +
                    "        cout << str;\n" +
                    "        return 0;\n" +
                    "    }",
            "How many characters are specified in the ASCII scheme?",
            "Which of the following belongs to the set of character types?",
            "What will be the output of the following C++ code?\n" +
                    "\n" +
                    "    #include <stdio.h>\n" +
                    "    int main()\n" +
                    "    {\n" +
                    "        char a = '\\012';\n" +
                    " \n" +
                    "        printf(\"%d\", a);\n" +
                    "        return 0;\n" +
                    "    }",
            "The concept of two functions with same name is know as?"

    };


    private String mChoices[][] = {
            {"#include <userdefined.h>", " #include <userdefined>", " #include “userdefined”", "#include [userdefined]"},
            {"7var_name", "7VARNAME", "VAR_1234", "$var_name"},
            {"Abstraction", "Encapsulation", "Polymorphism", "Modularity"},
            {"int", "bool", "double", "float"},
            {"int", "bool", "double", "void"},
            {"int array[10];", "int array;", "array{10};", "array array[10];"},
            {"6553","6533","6522","12200"},
            {"ABCD","ABC","AB","AC"},
            {"64","128","256","24"},
            {"char","wchar_t","only a","both wchar_t and char"},
            {" compile error","10"," 12","empty"},
            {"Function Overloading","Operator Overloading","Function Overriding","Function Renaming"}
    };

    private String mCorrectAnswers[] = {"#include “userdefined”", "VAR_1234", "Encapsulation", "bool", "double",
            "int array[10];", "6533","ABC","128","both wchar_t and char","10","Function Overloading"};


    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getAns1(int a) {
        String Ans1 = mChoices[a][0];
        return Ans1;
    }

    public String getAns2(int a) {
        String Ans2 = mChoices[a][1];
        return Ans2;
    }

    public String getAns3(int a) {
        String Ans3 = mChoices[a][2];
        return Ans3;
    }

    public String getAns4(int a) {
        String Ans4 = mChoices[a][3];
        return Ans4;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }
}


