package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession4 extends AppCompatActivity {

    TextView content1, content2, content3, content4, content5, content6, content7, content8;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag4);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("The PHP arithmetic operators are used with numeric values to perform common arithmetical operations, such as addition, subtraction, multiplication etc.\n" +
                "\n" +
                "Operators:\n" +
                "+\tAddition\n" +
                "-\tSubtraction\n" +
                "*\tMultiplication\n" +
                "/\tDivision\n" +
                "%\tModulus\n" +
                "**\tExponentiation");

        content2.setText("The PHP assignment operators are used with numeric values to write a value to a variable.\n" +
                "\n" +
                "The basic assignment operator in PHP is \"=\". It means that the left operand gets set to the value of the assignment expression on the right.\n" +
                "\n" +
                "Assignment:\t\t\tSame as...\t\n" +
                "\n" +
                "x = y\t\t\t\t\tx = y\t\t\n" +
                "x += y\t\t\t\t\tx = x + y\t\n" +
                "x -= y\t\t\t\t\tx = x - y\t\n" +
                "x *= y\t\t\t\t\tx = x * y\t\n" +
                "x /= y\t\t\t\t\tx = x / y   \n" +
                "x %= y\t\t\t\t\tx = x % y\t\n");

        content3.setText("The PHP comparison operators are used to compare two values (number or string):\n" +
                "\n" +
                "Operators:\n" +
                "\n" +
                "==\t\tEqual\n" +
                "===\t\tIdentical\n" +
                "!=\t\tNot equal\n" +
                "<>\t\tNot equal\n" +
                "!==\t\tNot identical\n" +
                ">\t\tGreater than\n" +
                "<\t\tLess than\n" +
                ">=\t\tGreater than or equal to\n" +
                "<=\t\tGreater than or equal to\n" +
                "<=>\t\tSpaceship\n" +
                "\n" +
                "\tSpaceship:\n" +
                "\n" +
                "\tReturns an integer less than, equal to, or greater than zero, depending on if $x is less than, equal to, or greater than $y. Introduced in PHP 7.");

        content4.setText("Increment and decrement operators are used to add or subtract 1 from the current value of oprand. \n" +
                "Increment and Decrement operators can be preIncrement or postIncrement and preDecrement or postDecrement. \n" +
                "\n" +
                "Types: \n" +
                "\n" +
                "++$x    pre-increment\n" +
                "$x++    post-increment  \n" +
                "--$x    pre-decrement\n" +
                "$x--    post-decrement \n");

        content5.setText("The following table lists the logical operators:\n" +
                "    \n" +
                "    &&\t\tConditional-AND \n" +
                "    ||\t\tConditional-OR\n" +
                "    ! \t\tNot\n" +
                "    xor\t\tXor\n" +
                "    and\t\tConditional-AND\n" +
                "    or \t\tConditional-OR");

        content6.setText("PHP has two operators that are specially designed for strings.\n" +
                "\n" +
                "    .  \t\tConcatenation\n" +
                "    .= \t\tConcatenation assignment");

        content7.setText("The PHP array operators are used to compare arrays.\n" +
                "\n" +
                "+\t\tUnion\n" +
                "==\t\tEquality\n" +
                "===\t\tIdentity\n" +
                "!=\t\tInequality\n" +
                "<>\t\tInequality\n" +
                "!==\t\tNon-identity");

        content8.setText("The PHP conditional assignment operators are used to set a value depending on conditions:\n" +
                "\n" +
                "?: Ternary Operator\n" +
                "??  Null coalescing Operator\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    Ternary Operator: $x = expr1 ? expr2 : expr3\n" +
                "    Returns the value of $x.\n" +
                "    The value of $x is expr2 if expr1 = TRUE.\n" +
                "    The value of $x is expr3 if expr1 = FALSE\n" +
                "\n" +
                "\n" +
                "    Null coalescing Operator: $x = expr1 ?? expr2\n" +
                "    Returns the value of $x.\n" +
                "    The value of $x is expr1 if expr1 exists, and is not NULL.\n" +
                "    If expr1 does not exist, or is NULL, the value of $x is expr2.\n" +
                "    Introduced in PHP 7");
    }
}


