package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp19 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "void reverseSentence();\n" +
                "int main() {\n" +
                "    printf(\"Enter a sentence: \");\n" +
                "    reverseSentence();\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "void reverseSentence() {\n" +
                "    char c;\n" +
                "    scanf(\"%c\", &c);\n" +
                "    if (c != '\\n') {\n" +
                "        reverseSentence();\n" +
                "        printf(\"%c\", c);\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a sentence: awesome program\n" +
                "margorp emosewa");

    }
}
