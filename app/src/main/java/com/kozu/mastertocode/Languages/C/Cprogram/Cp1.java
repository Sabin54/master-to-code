package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp1 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {   \n" +
                "    int number;\n" +
                "   \n" +
                "    printf(\"Enter an integer: \");  \n" +
                "    \n" +
                "    // reads and stores input\n" +
                "    scanf(\"%d\", &number);\n" +
                "\n" +
                "    // displays output\n" +
                "    printf(\"You entered: %d\", number);\n" +
                "    \n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output\n" +
                "\n" +
                "Enter an integer: 25\n" +
                "You entered: 25");

    }
}
