package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession8 extends AppCompatActivity {
    TextView content1, content2, content3, content4;
    AdView adView, adView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag8);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("Fopen() function is used to open file with different modes.\n" +
                "\n" +
                "    r ->     Read Only\n" +
                "    w ->     Write Only\n" +
                "    a ->     Writr Only\n" +
                "\n" +
                "fread() function reads data from an open file.\n" +
                "fclose() function is used to close an opened file.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    <?php\n" +
                "    $myfile = fopen(\"webdictionary.txt\", \"r\") or die(\"Unable to open file!\");\n" +
                "    echo fread($myfile,filesize(\"webdictionary.txt\"));\n" +
                "    fclose($myfile);\n" +
                "    ?>");

        content2.setText("The copy() function copies a file. This function returns TRUE on success and FALSE on failure.\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "    <?php\n" +
                "    echo copy(\"source.txt\",\"target.txt\");\n" +
                "    ?>\n");

        content3.setText("The rename() function renames a file or directory. This function returns TRUE on success, or FALSE on failure.\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "    <?php\n" +
                "    rename(\"/test/file1.txt\",\"/home/docs/my_file.txt\");\n" +
                "    ?>\n");

        content4.setText("The unlink() function deletes a file. Same work can be done through delete() function also.\n" +
                "\n" +
                "Syntax:\n" +
                "\n" +
                "    <?php\n" +
                "    $file = fopen(\"test.txt\",\"w\");\n" +
                "    echo fwrite($file,\"Hello World. Testing!\");\n" +
                "    fclose($file);\n" +
                "\n" +
                "    unlink(\"test.txt\");\n" +
                "    ?>\n");


    }
}