package com.kozu.mastertocode.Languages.C.Cquiz;

public class Cquestions {
    private String mQuestions[] = {
            " #include<stdio.h>\n" +
                    "\n" +
                    "main() \n" +
                    "{ \n" +
                    "   const int a = 5; \n" +
                    "   \n" +
                    "   a++; \n" +
                    "   printf(\"%d\", a); \n" +
                    "}",
            " The type name/reserved word ‘short’ is __________",
            " Identify the incorrect file opening mode from the following.",
            "#include<stdio.h>\n" +
                    "\n" +
                    "main()\n" +
                    "{\t\n" +
                    "   fprintf(stdout,\"Hello, World!\");\n" +
                    "}",
            "A Variable name in C includes which special symbols?",
            "   Which of the following is a logical OR operator?",
            "To print a float value which format specifier can be used?",
            "The library function strrchr() finds the first occurrence of a substring in another string.",
            "int incr (int i) \n" +
                    "{ \n" +
                    "   static int count = 0; \n" +
                    "   count = count + i; \n" +
                    "   return (count); \n" +
                    "} \n" +
                    "main () \n" +
                    "{ \n" +
                    "   int i,j; \n" +
                    "   for (i = 0; i <=4; i++) \n" +
                    "      j = incr(i); \n" +
                    "} \n" +
                    "\n" +
                    "Value of j = ?",
            "#include<stdio.h>\n" +
                    "\n" +
                    "main() \n" +
                    "{\n" +
                    "   int x = 5;\n" +
                    "   \n" +
                    "   if(x==5)\n" +
                    "   {\t\n" +
                    "       if(x==5) break;\n" +
                    "       printf(\"Hello\");\n" +
                    "   } \n" +
                    "   printf(\"Hi\");\n" +
                    "}",
            "How many loops are there in C",
            "The concept of two functions with same name is know as?",
            "What is the following is invalid header file in C?",
            "Libray function getch() belongs to which header file?",
            "Can we declare function inside structure of C Programming?",
            "How many main() function we can have in our project?",
            "int x = 10;\n" +
                    "\n" +
                    "int main()\n" +
                    "{\n" +
                    "    int x = 0;\n" +
                    "    printf(\"%d\",x);\n" +
                    "    return 0;\n" +
                    "}",

            "What should be the output:\n" +
                    "int main()\n" +
                    "{ \n" +
                    "    int a = 10/3;\n" +
                    "    printf(\"%d\",a); \n" +
                    "    \n" +
                    "    return 0;\n" +
                    "}",
            "How many times Nepal is printed?\n" +
                    "int main()\n" +
                    "{\n" +
                    "int a = 0;\n" +
                    "while(a++ < 5-++a)\n" +
                    "printf(\"Nepal\");\n" +
                    "return 0;\n" +
                    "}",
            "What is the extension of output file produced by Preprocessor?"

    };


    private String mChoices[][] = {
            {"5", "6", "Runtime error", "Compile error"},
            {"short long", " short char", "short float", "short int"},
            {"r", "w", "x", "a"},
            {"Hello, World!", "No Output", "Compile error", "Runtime error"},
            {"*", "#", "+", "_"},
            {"&", "&&", "||", "None of Above"},
            {"%lf","%f","%Lf","None of above"},
            {"strchr()","yes","Strstr()","strnset()"},
            {"10","4","6","7"},
            {"Hi","Hello","HelloHi","Compile Error"},
            {" 3"," 2"," 4","1"},
            {"Function Overloading","Operator Overloading","Function Overriding","Function Renaming"},
            {"math.h","mathio.h","string.h","ctype.h"},
            {"stdio.h","conio.h","stdlib.h","stdlibio.h"},
            {"Yes","No","Depends upon Compiler","Yes but run time error"},
            {"1","2","No limit","depends Upon Compiler"},
            {"10", "0", "Undefined", "Error"},
            {"3.33","3.0","3","0"},
            {"1","2","5","4"},
            {".h",".exe",".i",".asm"}
    };

    private String mCorrectAnswers[] = {"Compile error", "short int", "x", "Hello, World!", "_",
            "||", "%f","Strstr()","10","Hi","3","Function Overloading","mathio.h","conio.h","No","1","0","3","1",".i"};


    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getAns1(int a) {
        String Ans1 = mChoices[a][0];
        return Ans1;
    }

    public String getAns2(int a) {
        String Ans2 = mChoices[a][1];
        return Ans2;
    }

    public String getAns3(int a) {
        String Ans3 = mChoices[a][2];
        return Ans3;
    }

    public String getAns4(int a) {
        String Ans4 = mChoices[a][3];
        return Ans4;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }
}
