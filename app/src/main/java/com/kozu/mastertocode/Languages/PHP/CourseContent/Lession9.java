package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession9 extends AppCompatActivity {
    TextView content1, content2;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag9);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("A cookie is a small file that the server embeds on the user's computer. Each time the samecomputer requests a page with a browser, it will send the cookie too. \n" +
                "With PHP, you can both create and retrieve cookie values.\n" +
                "A cookie is often used to identify a user.\n" +
                "A cookie is created with the setcookie() function.\n" +
                "The setcookie() function must appear BEFORE the <html> tag.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "    <?php\n" +
                "    $cookie_name = \"user\";\n" +
                "    $cookie_value = \"John Doe\";\n" +
                "    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), \"/\"); // 86400 = 1 day\n" +
                "    ?>\n" +
                "    <html>\n" +
                "    <body>\n" +
                "\n" +
                "    <?php\n" +
                "    if(!isset($_COOKIE[$cookie_name])) {\n" +
                "      echo \"Cookie named '\" . $cookie_name . \"' is not set!\";\n" +
                "    } else {\n" +
                "      echo \"Cookie '\" . $cookie_name . \"' is set!<br>\";\n" +
                "      echo \"Value is: \" . $_COOKIE[$cookie_name];\n" +
                "    }\n" +
                "    ?>\n" +
                "\n" +
                "    </body>\n" +
                "    </html>");

        content2.setText("A session is a way to store information (in variables) to be used across multiple pages.\n" +
                "By default, session variables last until the user closes the browser.\n" +
                "Session variables hold information about one single user, and are available to all pages in one application.\n" +
                "Session data is stored on web server.\n" +
                "A session is started with the session_start() function.\n" +
                "\n" +
                "Example:\n" +
                "\n" +
                "\t<?php\n" +
                "\t// Start the session\n" +
                "\tsession_start();\n" +
                "\t?>\n" +
                "\t<!DOCTYPE html>\n" +
                "\t<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t<?php\n" +
                "\t// Set session variables\n" +
                "\t$_SESSION[\"favcolor\"] = \"green\";\n" +
                "\t$_SESSION[\"favanimal\"] = \"cat\";\n" +
                "\techo \"Session variables are set.\";\n" +
                "\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "\t</html>");
    }
}
