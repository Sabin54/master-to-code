package com.kozu.mastertocode.Languages.PHP.PHPQuiz;

public class PHPQuestions {
    private String mQuestions[] = {
            " PHP files have a default file extension of?",
            " What should be the correct syntax to write a PHP code?",
            " <?php\n" +
                    "    $num  = 1;\n" +
                    "    $num1 = 2;\n" +
                    "    print $num . \"+\". $num1;\n" +
                    "    ?>",
            " <?php\n" +
                    "    $num  = \"1\";\n" +
                    "    $num1 = \"2\";\n" +
                    "    print $num+$num1;\n" +
                    "    ?>",
            "Which is the right way of declaring a variable in PHP?",
            "    <?php\n" +
                    "    $total = \"25 students\";\n" +
                    "    $more = 10;\n" +
                    "    $total = $total + $more;\n" +
                    "    echo \"$total\";\n" + "    ?>",
            "PHP is a _________________",
            "_____________ is Concatenation operator in PHP",
            "Which of following is used to add multiple line comments in PHP ?",
            "Which sign is used to access variable of variable in PHP?",
            "Which of following is not a Superglobals in PHP?",
            "Which function is used to get ASCII value of a character in PHP ?",
            "______ is used to unset a variable in PHP ?",
            "Which function in PHP is used to get the length of string variable?",
            "PHP’s numerically indexed array begin with position ___________ .",
            "Which of the following PHP functions accepts any number of parameters?",
            "If $a = 12 what will be returned when ($a == 12) ? 5 : 1 is executed?"

    };


    private String mChoices[][] = {
            {".html", " .xml", ".php", ".ph"},
            {"< php >", " < ? php ?>", "<? ?>", "<?php ?>"},
            {"3", "1+2", "1.+.2", "Error"},
            {"3", "1+2", "Error", "12"},
            {"ii)", "iii)", "ii), iii) and iv)", " ii) and iv)"},
            {"Error", "35 students", "35", "25 students"},
            {" Tightly typed language","Loosely typed language","Server typed language","Client typed language"},
            {" + (plus)",". (dot)","^","-"},
            {"//","/* */","{{}}","{/}"},
            {"$$","$","@","%"},
            {" $_SERVER"," $_ENV"," $_FILES","$_PUT"},
            {"asc()","chr()","ascii()","val()"},
            {"delete()","unset()","unlink()","remove()"},
            {"count()","streln","strcount","len"},
            {"-1","0","1","2"},
            {"func_get_args()","func_get_argv()","get_argv()","get_argc()"},
            {"12", "1", "5", "Error"}
    };

    private String mCorrectAnswers[] = {".php", "<?php ?>", "1+2", "3", " ii) and iv)",
            "35", "Loosely typed language",". (dot)","/* */","$$","$_PUT","chr()","unset()","streln","0","func_get_args()","5"};


    public String getQuestion(int a) {
        String question = mQuestions[a];
        return question;
    }

    public String getAns1(int a) {
        String Ans1 = mChoices[a][0];
        return Ans1;
    }

    public String getAns2(int a) {
        String Ans2 = mChoices[a][1];
        return Ans2;
    }

    public String getAns3(int a) {
        String Ans3 = mChoices[a][2];
        return Ans3;
    }

    public String getAns4(int a) {
        String Ans4 = mChoices[a][3];
        return Ans4;
    }

    public String getCorrectAnswer(int a) {
        String answer = mCorrectAnswers[a];
        return answer;
    }

}


