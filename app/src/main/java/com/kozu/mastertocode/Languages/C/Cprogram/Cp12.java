package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp12 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int i, n, t1 = 0, t2 = 1, nextTerm;\n" +
                "    printf(\"Enter the number of terms: \");\n" +
                "    scanf(\"%d\", &n);\n" +
                "    printf(\"Fibonacci Series: \");\n" +
                "\n" +
                "    for (i = 1; i <= n; ++i) {\n" +
                "        printf(\"%d, \", t1);\n" +
                "        nextTerm = t1 + t2;\n" +
                "        t1 = t2;\n" +
                "        t2 = nextTerm;\n" +
                "    }\n" +
                "\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter the number of terms: 10\n" +
                "Fibonacci Series: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ");

    }
}
