package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP12 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);
        tv1 = findViewById(R.id.tv1);

        tv1.setText("#include<iostream.h>\n" +
                "#include<string.h>\n" +
                "\n" +
                "void main()\n" +
                "{\n" +
                "    char string[50];\n" +
                "    int len, i, x, flag=0;\n" +
                "    cout<<\"Enter a String(max 49 characters): \";\n" +
                "    cin.getline(string,50);\n" +
                "    len = strlen(string);\n" +
                "    for(i=0, x=len-1;i<=len/2;i++, x--)\n" +
                "    {\n" +
                "        if(string[i]!=string[x])\n" +
                "        {\n" +
                "            flag=1;\n" +
                "            break;\n" +
                "        }\n" +
                "    }\n" +
                "    if(flag==0)\n" +
                "    {\n" +
                "        cout<<\"\\n\\nString is palindrome\";\n" +
                "    }\n" +
                "    else\n" +
                "    {\n" +
                "        cout<<\"\\n\\nString is not a palindrome\";\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "\n" +
                "Output:\n" +
                "Enter a String(max 49 characters) : Studytonight\n" +
                "String is palindrome");

    }
}
