package com.kozu.mastertocode.Languages.Cplusplus.CplusProgram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class CPP2 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include<iostream.h>\n" +
                "#include<conio.h>\n" +
                "\n" +
                "int main()\n" +
                "{\n" +
                "    int i, n1;\n" +
                "    clrscr() ;\n" +
                "    cout<<\"Enter the number to find it's divisors : \" ;\n" +
                "    cin>>n1 ;\n" +
                "    cout<<\"\\nThe divisors are :\\n\") ;\n" +
                "    for(i = 1 ; i <= n1 ; i++)\n" +
                "        if(n1 % i == 0)\n" +
                "            cout<<\"\\t\"<<i ;\n" +
                "    getch() ;\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "Enter the number to find it's divisors : 21\n" +
                "The divisors are :\n" +
                "1 3 7 21");

    }
}
