package com.kozu.mastertocode.Languages.Cplusplus.cPlusCourse;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class CplusLession2 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5, content6, content7, content8, content9, content10;
    AdView adView, adView1;
    TextView title1, title2, title3, title4,title5, title6, title7, title8,title9, title10;

    CardView cv3, cv4, cv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag4);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);
        content6 = findViewById(R.id.content6);
        content7 = findViewById(R.id.content7);
        content8 = findViewById(R.id.content8);
        content9 = findViewById(R.id.content9);
        content10 = findViewById(R.id.content10);

        title1 = findViewById(R.id.title1);
        title2 = findViewById(R.id.title2);
        title3 = findViewById(R.id.title3);
        title4 = findViewById(R.id.title4);
        title5 = findViewById(R.id.title5);
        title6 = findViewById(R.id.title6);
        title7 = findViewById(R.id.title7);
        title8 = findViewById(R.id.title8);
        title9 = findViewById(R.id.title9);
        title10 = findViewById(R.id.title10);


        title1.setText("1. OOP (Object-Oriented Programming)");
        title2.setText("2. Platform or Machine Independent/ Portable");
        title3.setText("3. Simple");
        title4.setText("4. High-level programming language");
        title5.setText("5. Popular");
        title6.setText("6. Case sensitive");
        title7.setText("7. Compiler-Based");
        title8.setText("8. DMA (Dynamic Memory Allocation)");
        title9.setText("9. Existence of Libraries");
        title10.setText("10. Speed");

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("C++ is an object-oriented language, unlike C which is a procedural language. This is one of the most important features of C++. It employs the use of objects while programming. These objects help you implement real-time problems based on data abstraction, data encapsulation, data hiding, and polymorphism.");

        content2.setText("In simple terms, portability refers to using the same piece of code in varied environments.\n" +
                "\n" +
                "Let us understand this C++ feature with the help of an example. Suppose you write a piece of code to find the name, age, and salary of an employee in Microsoft Windows and for some apparent reason you want to switch your operating system to LINUX. This code will work in a similar fashion as it did in Windows.");

        content3.setText("When we start off with a new language, we expect to understand in depth. The simple context of C++ gives an appeal to programmers, who are eager to learn a new programming language.\n" +
                "\n" +
                "If you are already familiar with C, then you don’t need to worry about facing any trouble while working in C++. The syntax of C++ is almost similar to that of C. Afterall C++ is referred to as “C with classes”.");

        content4.setText("It is important to note that C++ is a high-level programming language, unlike C which is a mid-level programming language. It makes it easier for the user to work in C++ as a high-level language as we can closely associate it with the human-comprehensible language, that is, English.");

        content5.setText("After learning C, it is the base language for many other popular programming languages which supports the feature of object-oriented programming. Bjarne Stroustrup found Simula 67, the first object-oriented language ever, lacking simulations and decided to develop C++.");

        content6.setText("Just like C, it is pretty clear that the C++ programming language treats the uppercase and lowercase characters in a different manner. For instance, the meaning of the keyword ‘cout’ changes if we write it as ‘Cout’ or “COUT”. Other programming languages like HTML and MySQL are not case sensitive.");

        content7.setText("Unlike Java and Python that are interpreter-based, C++ is a compiler based language and hence it a relatively much faster than Python and Java.");

        content8.setText("Since C++ supports the use of pointers, it allows us to allocate memory dynamically. We may even use constructors and destructors while working with classes and objects in C++.");

        content9.setText("The C++ programming language offers a library full of in-built functions that make things easy for the programmer. These functions can be accessed by including suitable header files.");

        content10.setText("As discussed earlier, C++ is compiler-based hence it is much faster than other programming languages like Python and Java that are interpreter-based.");

    }
}