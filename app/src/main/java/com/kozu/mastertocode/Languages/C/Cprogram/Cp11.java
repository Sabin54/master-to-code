package com.kozu.mastertocode.Languages.C.Cprogram;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class Cp11 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("#include <stdio.h>\n" +
                "int main() {\n" +
                "    int n, i, sum = 0;\n" +
                "\n" +
                "    printf(\"Enter a positive integer: \");\n" +
                "    scanf(\"%d\", &n);\n" +
                "\n" +
                "    for (i = 1; i <= n; ++i) {\n" +
                "        sum += i;\n" +
                "    }\n" +
                "\n" +
                "    printf(\"Sum = %d\", sum);\n" +
                "    return 0;\n" +
                "}\n" +
                "\n" +
                "Output:\n" +
                "\n" +
                "Enter a positive integer: 100\n" +
                "Sum = 5050");

    }
}
