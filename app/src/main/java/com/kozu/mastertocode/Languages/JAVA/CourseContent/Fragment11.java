package com.kozu.mastertocode.Languages.JAVA.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Fragment11 extends AppCompatActivity {
    TextView content1, content2, content3, content4, content5;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_frag11);

        content1 = findViewById(R.id.content1);
        content2 = findViewById(R.id.content2);
        content3 = findViewById(R.id.content3);
        content4 = findViewById(R.id.content4);
        content5 = findViewById(R.id.content5);

        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("An applet is a small Java program that is embedded and ran in some other Java interpreter program such as a Java technology-enabled browser Or Sun's applet viewer program called appletviewer.\n" +
                "\n" +
                "An applet can be a fully functional Java application because it has the entire Java API at its disposal.\n" +
                "Key Points\n" +
                "\n" +
                "An applet program is a written as a inheritance of the java.Appletclass\n" +
                "There is no main() method in an Applet.\n" +
                "Applets are designed to be embedded within an HTML page.\n" +
                "\n" +
                "An applet uses AWT for graphics\n" +
                "A JVM is required to view an applet. The JVM can be either a plug-in of the Web browser or a separate runtime environment.\n" +
                "Other classes that the applet needs can be downloaded in a single Java Archive (JAR) file.\n" +
                "\n" +
                "Advantages\n" +
                "\t\n" +
                "\t1. Automatically integrated with HTML; hence, resolved virtually all installation issues.\n" +
                "\t\n" +
                "\t2. Can be accessed from various platforms and various java-enabled web browsers.\n" +
                "\t\n" +
                "\t3. Can provide dynamic, graphics capabilities and visualizations\n" +
                "\t\n" +
                "\t4. Implemented in Java, an easy-to-learn OO programming language\n" +
                "\t\n" +
                "\t5. Alternative to HTML GUI design \n" +
                "\t\n" +
                "\t6. Safe! Because of the security built into the core Java language and the applet structure, you don't have to worry about bad code causing damage to someone's system\n" +
                "\t\n" +
                "\t7. Can be launched as a standalone web application independent of the host web server\n" +
                "\n" +
                "Disadvantages\n" +
                "\t\n" +
                "\t1. Applets can't run any local executable programs\n" +
                "\t\n" +
                "\t2. Applets can't with any host other than the originating server\n" +
                "\t\n" +
                "\t3. Applets can't read/write to local computer's file system\n" +
                "\t\n" +
                "\t4. Applets can't find any information about the local computer\n" +
                "\t\n" +
                "\t5. All java-created pop-up windows carry a warning message\n" +
                "\t\n" +
                "\t6. Stability depends on stability of the client's web server\n" +
                "\t\n" +
                "\t7. Performance directly depend on client's machine");


        content2.setText("init:\n" +
                "This method is intended for whatever initialization is needed for your applet. It is called after the param tags inside the applet tag have been processed.\n" +
                "\n" +
                "start:\n" +
                "This method is automatically called after the browser calls the init method. It is also called whenever the user returns to the page containing the applet after having gone off to other pages.\n" +
                "\n" +
                "stop:\n" +
                "This method is automatically called when the user moves off the page on which the applet sits. It can, therefore, be called repeatedly in the same applet.\n" +
                "\n" +
                "destroy: \n" +
                "This method is only called when the browser shuts down normally. Because applets are meant to live on an HTML page, you should not normally leave resources behind after a user leaves the page that contains the applet.\n" +
                "\n" +
                "paint:\n" +
                "Invoked immediately after the start() method, and also any time the applet needs to repaint itself in the browser. The paint() method is actually inherited from the java.awt.\n");


        content3.setText("To create an executable jar file there should be a main class (a class with a main method) inside the jar to run the software. \n" +
                "An applet has no main method since it is usually designed to work inside a web browser.\n" +
                "\n" +
                "Executable applet is nothing but the .class file of applet, which is obtained by compiling the source code of the applet.\n" +
                "Compiling the applet is exactly the same as compiling an application using following command.\n" +
                "\n" +
                "\tjavac appletname.java\n" +
                "\n" +
                "The compiled output file called appletname.class should be placed in the same directory as the source file.");

        content4.setText("Java applet are programs that reside on web page. A web page is basically made up of text and HTML tags that can be interpreted by a web browser or an applet viewer.\n" +
                "HTML files should be stored in the same directory as the compiled code of the applets.\n" +
                "A web page is marked by an opening HTML tag <HTML> and closing HTML tag </HTML> and is divided into the following three major parts:\n" +
                "\n" +
                "1. Comment Section\n" +
                "2. Head Section\n" +
                "3. Body Section\n" +
                "\n" +
                "Comment Section\n" +
                "This is very first section of any web page containing the comments about the web page functionality. A comment line begins with <! And ends with a > and the web browsers will ignore the text enclosed between them. The comments are optional and can be included anywhere in the web page. \n" +
                "\n" +
                "Head Section\n" +
                "This section contains title, heading and sub heading of the web page. The head section is defined with a starting <Head> tag and closing </Head> tag.\n" +
                "\t\t\n" +
                "\t\t<Head>\n" +
                "\t\t<Title>Hello World Applet</Title>\n" +
                "\t\t</Head> \n" +
                "\n" +
                "Body Section\n" +
                "The entire information and behaviour of the web page is contained in it, It defines what the data placed and where on to the screen. It describes the color, location, sound etc. of the data or information that is going to be placed in the web page.\n" +
                "\t\n" +
                "\t\t<body>\n" +
                "\t\t<h1>Hi, This is My First Java Applet on the Web!</h1>\n" +
                "\t\t</body> \n" +
                "\n" +
                "Applet Tag\n" +
                "The <Applet...> tag supplies the name of the applet to be loaded and tells the browser how much space the applet requires.\n" +
                "The <Applet> tag given below specifies the minimum requirements to place the Hellojava applet on a web page.\n" +
                "\t\t\n" +
                "\t\t<Applet code = \"Hellojava.class\" \n" +
                "\t\twidth = 400\n" +
                "\t\tHeight = 200 >\n" +
                "\t\t</Applet> \n" +
                "\n" +
                "This HTML code tells the browser to load the compiled java applet Hellojava.class, which is in the same directory as this HTML file.\n" +
                "It also specifies the display area for the applet output as 400 pixels width and 200 pixels height.\n" +
                "After you create this file, you can execute the HTML file called RunApp.html (say) on the command line.\n" +
                "\n" +
                "\tc:\\ appletviewer RunApp.html");

        content5.setText("To execute an applet with an applet viewer, you may also execute the HTML file in which it is enclosed, eg.\n" +
                "\t\n" +
                "\tc:\\>appletviewer RunApp.html\n" +
                "\t\n" +
                "Execute the applet the applet viewer, specifying the name of your applet's source file.\n" +
                "The applet viewer will encounter the applet tage within the comment and execute your applet.");

    }
}
