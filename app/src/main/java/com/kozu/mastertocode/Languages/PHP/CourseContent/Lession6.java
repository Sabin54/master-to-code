package com.kozu.mastertocode.Languages.PHP.CourseContent;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kozu.mastertocode.R;

public class Lession6 extends AppCompatActivity {
    TextView content1;
    AdView adView, adView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_frag6);

        content1 = findViewById(R.id.content1);


        adView = findViewById(R.id.adView);
        adView1 = findViewById(R.id.adView1);

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        adView1.loadAd(adRequest1);

        content1.setText("PHP String are enclosed within single quotes or double quotes. String is the collection of alphabets, digits, symbols.\n" +
                "\n" +
                "    1. strlen() – this function is used to get the length of String\n" +
                "\n" +
                "    2. str_word_count() – counts the number of words in a string\n" +
                "\n" +
                "    3. strrev() – reverses a string\n" +
                "\n" +
                "    4. strpos() – strpos() function searches for a specific text within a string. If a match is" +
                "    found, the function returns the character position of the first match. If no match is" +
                "    found, it will return FALSE.\n" +
                "\n" +
                "    5. str_replace() – str_replace() function replaces some characters with some other characters in a string.\n" +
                "\n" +
                "    6. uc_words() – Convert the first character of each word to uppercase\n" +
                "\n" +
                "    7. trim() – Remove characters from both sides of a string\n" +
                "\n" +
                "    8. strtoupper() – Convert all characters to uppercase\n" +
                "\n" +
                "    9. strtolower() - Convert all characters to lowercase\n");
    }
}
