package com.kozu.mastertocode.Languages.PHP.Program;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kozu.mastertocode.R;

public class PhpProgram11 extends AppCompatActivity {
    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.j_program1);

        tv1 = findViewById(R.id.tv1);
        tv1.setText("<?php\n" +
                "// Start the session\n" +
                "session_start();\n" +
                "?>\n" +
                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<body>\n" +
                "\n" +
                "\t\t<?php\n" +
                "\t\t// Set session variables\n" +
                "\t\t$_SESSION[\"favcolor\"] = \"green\";\n" +
                "\t\t$_SESSION[\"favanimal\"] = \"cat\";\n" +
                "\t\techo \"Session variables are set.\";\n" +
                "\t\t?>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>\n" +
                "\n" +
                "Output:\n" +
                "Session variables are set.");

    }
}
