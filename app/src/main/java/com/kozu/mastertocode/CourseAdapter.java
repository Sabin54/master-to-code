package com.kozu.mastertocode;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kozu.mastertocode.Languages.C.CprogramCourseContent;
import com.kozu.mastertocode.Languages.Cplusplus.CplusCourseContent;
import com.kozu.mastertocode.Languages.JAVA.JavaCourseContent;
import com.kozu.mastertocode.Languages.PHP.PhpCourseContent;

import java.util.List;

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyViewHolder> {

    private Context mContext;
    private List<CourseListDataModel> CourseList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
         TextView title;
         ImageView thumbnail;
         CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            card_view = view.findViewById(R.id.card_view);
        }
    }
    public CourseAdapter(Context mContext, List<CourseListDataModel> CourseList) {
        this.mContext = mContext;
        this.CourseList = CourseList;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_page, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        CourseListDataModel s = CourseList.get(i);
        myViewHolder.title.setText(s.getCourseName());
        Glide.with(mContext).load(s.getPhoto()).into(myViewHolder.thumbnail);

        myViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = i;
                Log.d("TITLE", "onClick: "+myViewHolder.title.getText().toString());
                if(myViewHolder.title.getText().equals("JAVA"))
                {
                    Intent intent = new Intent(mContext, JavaCourseContent.class);
                    mContext.startActivity(intent);
                }
                if(myViewHolder.title.getText().equals("C++"))
                {
                    Intent intent = new Intent(mContext, CplusCourseContent.class);
                    mContext.startActivity(intent);
                }
                if(myViewHolder.title.getText().equals("Kotlin"))
                {
                    Toast.makeText(mContext,"Coming Soon", Toast.LENGTH_LONG).show();
                }
                if(myViewHolder.title.getText().equals("Python"))
                {
                    Toast.makeText(mContext,"Coming Soon", Toast.LENGTH_LONG).show();
                }
                if(myViewHolder.title.getText().equals("C#"))
                {
                    Toast.makeText(mContext,"Coming Soon", Toast.LENGTH_LONG).show();
                }
                if(myViewHolder.title.getText().equals("C Programming"))
                {
                    Intent intent = new Intent(mContext, CprogramCourseContent.class);
                    mContext.startActivity(intent);
                }
                if(myViewHolder.title.getText().equals("PHP"))
                {
                    Intent intent = new Intent(mContext, PhpCourseContent.class);
                    mContext.startActivity(intent);
                }
                if(myViewHolder.title.getText().equals("Java Script"))
                {
                    Toast.makeText(mContext,"Coming Soon", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return CourseList.size();
    }

}
